<?php
abstract class Extension {
	public abstract function hasMethods();
	public abstract function getPackage();
}