<?php
/**
 * @file
 * Contains web objects which are useful in various situations.
 */
 
/**
 * WebResponse Class - Web Response
 *
 * This class handles sending responses to the user. This includes redirection,
 * caching, cookies and types.
 */
class WebResponse {
	protected $kernel;
	
	public function __construct($kernel) {
		if (!($kernel instanceof Kernel)) {
			throw new KInvalidArgumentException('Invalid Kernel was passed.');
		}
		
		$this->kernel = $kernel;
	}
	
	/**
	 * Redirects the user to a new page.
	 *
	 * @param $url
	 *	 The URL to redirect the user to. In most cases, relative URLs will not
	 *	 work with Kakera.
	 */
	public function redirect($url) {
		// Clear all output buffers.
		$ob_levels = ob_get_level();
		for($i = 0; $i < $ob_levels; $i++)
			ob_end_clean();
		
		// Send header and a small message.
		header("Location: {$url}");
		die("Now redirecting you to <a href=\"{$url}\">{$url}</a>.");
	}
	
	/**
	 * Redirects the user to a internal page.
	 *
	 * This method automatically includes the URL for this application and
	 * so will redirect the user's browser to another URL within the same
	 * application.
	 *
	 * This will send a 301 header to the client and they will automatically
	 * request that page. This will not work for POST requests, the POST
	 * data will not be resent. 
	 *
	 * @param $path
	 *   The path to redirect the user to.
	 */
	public function redirectInternal($path) {
		// Clear all output buffers.
		$ob_levels = ob_get_level();
		for($i = 0; $i < $ob_levels; $i++)
			ob_end_clean();
		
		// Get URL Root
		$weburi = new WebUri($this->kernel);
		$url = $weburi->getRoot($path);
		
		// Send header and a small message.
		header("Location: {$url}");
		die("Now redirecting you to <a href=\"{$url}\">{$url}</a>.");
	}
	
	/**
	 * Sends cache specific HTTP headers to control caching.
	 *
	 * This controls caching on the client side, you can either specify to 
	 * disable caching all together with FALSE, or you can send an amount of
	 * seconds to cache from now.
	 *
	 * This function does not gurantee the client will honor these headers as
	 * they are subject to whatever settings the user has set. However, 
	 * reverse proxies are much more compliant and will honor these if set 
	 * correctly.
	 *
	 * Times under 5 minutes are prone to be unreliable since client's clocks
	 * may not correctly match the ones with the server. To avoid this, avoid
	 * setting times under 5 minutes.
	 *
	 * @param $time
	 *	 If set to FALSE, this will disable caching for this page, otherwise
	 *	 it will assume this to be the amount of seconds to cache this page
	 *	 for.
	 */
	public function setCache($time) {
		$realtime = $time * 1;
		if ($time === FALSE || $realtime <= 0) {
			header("Cache-Control: no-cache, must-revalidate");
			header("Expires: Wed, 1 Jan 1997 05:00:00 GMT");
		}
		else {
			header('Expires: '. gmdate('D, j M Y H:i:s', time()+$realtime) . ' GMT');
		}
	}
	
	/**
	 * Sends headers to the client if headers have not already been sent.
	 *
	 * This method is similar to the php function header() but differs in that
	 * if headers have already been sent to the client, this will not send 
	 * any more (and a log event will occur). 
	 *
	 * By using this, you can avoid having the PHP error message related to
	 * headers from appearing and allows you to at least be notified via. the 
	 * log.
	 *
	 * @param $header
	 *   The header to send.
	 */
	public function setHeader($header) {
		if (headers_sent()) {
			$this->kernel->log()->add('Setting header (%s) failed! Headers have already been sent.', Log::LOG_WARN);
			return FALSE;
		}
		else {
			header($header);
			return TRUE;
		}
	}
	
	/**
	 * Sets a HTTP Status Code.
	 *
	 * This method allows you to set a HTTP Status Code for your response. 
	 * This can be any currently be:
	 *   200, 400, 401, 403, 404, 500, 501, 502, 503. 
	 *
	 * Any other HTTP Status Codes will be ignored.
	 *
	 * @param $code
	 *   The code to send.
	 */
	public function setHttpStatusCode($code) {
		$codes = array(
			200 => 'OK', 
			400 => 'Bad Request', 
			401 => 'Unauthorized', 
			403 => 'Forbidden', 
			404 => 'Not Found', 
			500 => 'Internal Server Error', 
			501 => 'Not Implemented', 
			502 => 'Bad Gateway', 
			503 => 'Service Unavailable',
			);
		
		if (!in_array($code, array_keys($codes))) {
			return FALSE;
		}
		
		$this->setHeader(sprintf('HTTP/1.1 %d %s', $code, $codes[$code]));
		return TRUE;
	}
}