<?php
/**
 * Config - Configuration Object Class
 *
 * This is the configuration object used in Bittersweet. It allows you to load and
 * access various configuration files and settings.
 */
class Config implements ArrayAccess {
	protected $settings = array();
	
	public function load($file, $overwrite = TRUE) {
		if (file_exists($file)) {
			$config = include $file;
			return $this->add($config, $overwrite);
		}
		
		return FALSE;
	}
	
	public function add($config, $overwrite = TRUE) {
		if (is_array($config)) {
			if ($overwrite) {
				$this->settings = array_merge($this->settings, $config);
			}
			else {
				$this->settings = array_merge($config, $this->settings);
			}
			return TRUE;
		}
		
		return FALSE;
	}
	
	public function set($name, $value) {
		$this->settings[$name] = $value;
	}
	
	public function get($name, $default = NULL) {
		if (isset($this->settings[$name])) {
			return $this->settings[$name];
		}
		
		return $default;
	}
	
	public function __get($name) {
		return $this->get($name);
	}
	
	public function __isset($name) {
		return isset($this->settings[$name]);
	}
	
	public function __unset($name) {
		unset($this->settings[$name]);
	}
	
	public function __set($name, $value) {
		$this->set($name, $value);
	}
	
	public function has($name) {
		return isset($this->settings[$name]);
	}
	
	public function getAll() {
		return $this->settings;
	}
	
	public function getKeys() {
		return array_keys($this->settings);
	}
	
	public function offsetExists($name) {
		return $this->has($name);
	}
	
	public function offsetGet($name) {
		return $this->get($name);
	}
	
	public function offsetSet($name, $value) {
		return $this->set($name, $value);
	}
	
	public function offsetUnset($name) {
		unset($this->settings[$name]);
	}
	
	public function __toString() {
		return "Config Object (". count($this->settings) . " Keys)";
	}
}