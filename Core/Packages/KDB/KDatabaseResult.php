<?php
/**
 * KDatabaseResult - Database Result Object
 *
 * This is a Database Result object which uses PHP's PDO as a backend. 
 */
class KDatabaseResult {
	/**
	 * Contains the result set handle
	 */
	protected $handle;
	
	/**
	 * Determines whether this query was successful or not.
	 */
	protected $valid = FALSE;

	/**
	 * Constructor
	 */
	public function __construct($result, $success) {
		$this->handle = $result;
		$this->valid = $success;
	}

	/**
	 * Returns whether this result is valid or not.
	 *
	 * @return
	 *	 Returns TRUE if there are results available, or the query did not 
	 *	 have any errors during it's execution. Returns FALSE otherwise.
	 */
	public function isValid() {
		return $this->valid;
	}
	
	/**
	 * Returns a row as an associative array.
	 *
	 * @return
	 *	 Returns the next available row as an associative array with the keys
	 *	 being the field names, and the values being the values.
	 */
	public function fetchAssoc() {
		if (!$this->isValid()) {
			return false;
		}
		
		return $this->handle->fetch(PDO::FETCH_ASSOC);
	}
	
	/**
	 * Returns a row as an indexed array.
	 *
	 * @return
	 *	 Returns the next available row as an indexed array containing the 
	 *	 containing the values in the same order as the fields were defiend.
	 */
	public function fetchRow() {
		if (!$this->isValid()) {
			return false;
		}
		
		return $this->handle->fetch(PDO::FETCH_NUM);
	}
	
	/**
	 * Returns all the rows as an array.
	 *
	 * @return
	 *	 Returns all the rows in this resultset as a indexed array of 
	 *	 associative arrays.
	 */
	public function fetchAll() {
		if (!$this->isValid()) {
			return false;
		}
		
		return $this->handle->fetchAll(PDO::FETCH_ASSOC);
	}
	
	/**
	 * Frees memory used by this resultset.
	 */
	public function free() {
		$this->handle->closeCursor();
	}
	
	/**
	 * Used when this object is unreachable.
	 */
	public function __destruct() {
		$this->free();
	}
}