<?php
/**
 * KDatabaseException - Exception when an error occurs from a database query.
 *
 * This exception occurs when an exception is returned from the database 
 * backend. 
 */
class KDatabaseException extends KException {}