<?php
/**
 * KDatabaseConnection - Database Connection Object
 *
 * This is a Database Connection object which uses PHP's PDO as a backend. It
 * supports prepared statements and caching of previously used statements.
 */
abstract class KDatabaseConnection {
	/**
	 * Contains the Database connection handle.
	 */
	protected $handle;

	/**
	 * Keeps the status of the Database Connection.
	 */
	protected $connected = false;

	/**
	 * A cache of previously executed queries.
	 */
	protected $queries = array();

	/**
	 * Num Executions
	 */
	protected $queryExecCount = array();

	/**
	 * A Tag for this DB Connection.
	 */
	protected $tag;

	/**
	 * Connects to a database server.
	 *
	 * This connects to a database server using the host, username, and
	 * password provided. If one cannot be given, the defaults will be used.
	 *
	 * @param $host
	 *	 The host to connect to. This can be a DNS, IP Address or Socket.
	 * @param $username
	 *	 The username to use when connecting.
	 * @param $password
	 *	 The password to use when connecting.
	 * @param $database
	 *	 The database to connect to.
	 *
	 * @return
	 *	 Returns TRUE on success, FALSE on failure.
	 */
	public function connect($host, $username, $password, $database, $tag = NULL) {
		$this->tag = $tag;
	}

	/**
	 * Retrieves the tag associated with this database connection.
	 *
	 * This is a tag used by applications to identify which database connection
	 * it is.
	 *
	 * @return
	 *   Returns NULL if not set, otherwise a String containing the tag.
	 */
	public function getTag() {
		return $this->tag;
	}

	/**
	 * Closes the current connection.
	 *
	 * This closes the current connection to the database server if one has
	 * been established. This method fails silently if there was no
	 * connection to close.
	 */
	public function close() {
		if ($this->connected) {
			$this->connected = FALSE;
			unset($this->handle);
		}
	}

	/**
	 * Returns the last error message from the database.
	 *
	 * @return
	 *	 Returns the message if one exists, otherwise returns FALSE if one
	 *	 does not exist.
	 */
	public function errorMessage() {
		$msg = $this->handle->errorInfo();
		if (!isset($msg[2]) || strlen($msg[2]) <= 0) {
			return FALSE;
		}

		return $msg[2];
	}

	/**
	 * Executes a query on the database.
	 *
	 * This executes a raw query on the database.
	 *
	 * @param $query
	 *		The query to execute.
	 *
	 * @return KDatabaseResult
	 *	 Returns a result which can be parsed.
	 */
	public function query($query) {
		try {
			$q = $this->getPreparedQuery($query);
		}
		catch (KDatabaseException $e) {
			throw $e;
		}
		$this->incrementQueryCount($query);

		$success = $q->execute();
		return new KDatabaseResult($q, $success);
	}

	/**
	 * Inserts some data into a table
	 *
	 * Inserts some data into a table within the database. This does a simple
	 * INSERT query and automatically escapeString()'s any arguments given.
	 *
	 * @param $table
	 *	 The name of the table you wish to insert into.
	 * @param $data
	 *	 The data which you wish you insert data into. This should be an
	 *	 indexed array with the data in the same order as the fields you'll
	 *	 insert into.
	 * @param $fields
	 *	 This contains the fields which you will be inserting into. If you
	 *	 do not provide this parameter, then it assumes you will insert into
	 *	 all fields. This is NOT filtered, so please only put hardcored
	 *	 variables in this field. If any user data gets into this, then it
	 *	 WILL become a SQL injection vector.
	 *
	 * @return
	 *		Returns the ID of the inserted data, or FALSE on failure.
	 */
	public function insert($table, $data) {
		$num_fields = count($data);

		// Create Fields
		$fields = array_keys($data);
		$f = '';
		$v = "";
		for($i = 0; $i < $num_fields; $i++) {
			$f .= $fields[$i];
			$v .= '?';
			if (($i + 1) < $num_fields) {
				$f .= ", ";
				$v .= ", ";
			}
		}

		$f = "($f)";
		$v = "($v)";

		// Execute the query.
		$query = "INSERT INTO {$table}{$f} VALUES {$v}";
		try {
			$q = $this->getPreparedQuery($query);
		}
		catch (KDatabaseException $e) {
			throw $e;
		}
		$this->incrementQueryCount($query);

		// Bind Params
		for ($i = 0; $i < $num_fields; $i++) {
			switch (true) {
				case is_bool($data[$fields[$i]]):
					$type = PDO::PARAM_BOOL;
					break;
				case is_int($data[$fields[$i]]):
					$type = PDO::PARAM_INT;
					break;
				case is_null($data[$fields[$i]]):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
					break;
			}
			$q->bindValue($i+1, $data[$fields[$i]], $type);
		}

		$success = $q->execute();

		if ($success) {
			return $q->rowCount();
		}
		else {
			return FALSE;
		}
	}

	/**
	 * Updates data within a table.
	 *
	 * This updates data within a table. This automatically escapes arguments
	 * given and creates the query.
	 *
	 * @param $table
	 *	 The table you wish to update.
	 * @param $data
	 *	 An associative array with the field names as the keys, and the values
	 *	 as the values.
	 * @param $where
	 *	 This is an associative array containing which data you wish to update.
	 *	 This is identical to a WHERE...AND... statement, and must match all
	 *	 values. The field names are NOT filtered, so please only put hardcored
	 *	 field names in this field. If any user data gets into this, then it
	 *	 WILL become a SQL injection vector.
	 *
	 * @return
	 *	 Returns the number of affected rows if successful, or FALSE on
	 *	 failure.
	 */
	public function update($table, $data, $where) {
		// Create SET area.
		$s = '';
		$set_fields = array_keys($data);
		$num_fields = count($set_fields);
		for ($i = 0; $i < $num_fields; $i++) {
			$s .= sprintf(
				"%s = ?",
				$set_fields[$i]
				);

			if (($i+1) < $num_fields) {
				$s .= ', ';
			}
		}

		// Create WHERE area.
		$w = '';
		$where_fields = array_keys($where);
		$num_where = count($where);
		for ($i = 0; $i < $num_where; $i++) {
			if (is_null($where[$where_fields[$i]])) {
				$w .= sprintf(
					"%s IS NULL",
					$where_fields[$i]
					);
			}
			else {
				$w .= sprintf(
					"%s = ?",
					$where_fields[$i]
					);
			}

			if (($i+1) < $num_where) {
				$w .= ' AND ';
			}
		}

		// Create Query and execute.
		$query = "UPDATE {$table} SET {$s} WHERE {$w}";
		try {
			$q = $this->getPreparedQuery($query);
		}
		catch (KDatabaseException $e) {
			throw $e;
		}
		$this->incrementQueryCount($query);

		// Bind Params
		for ($i = 0; $i < $num_fields; $i++) {
			switch (true) {
				case is_bool($data[$set_fields[$i]]):
					$type = PDO::PARAM_BOOL;
					break;
				case is_int($data[$set_fields[$i]]):
					$type = PDO::PARAM_INT;
					break;
				case is_null($data[$set_fields[$i]]):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
					break;
			}
			$q->bindValue($i+1, $data[$set_fields[$i]], $type);
		}

		for ($i = 0; $i < $num_where; $i++) {
			if (is_null($where[$where_fields[$i]])) {
				continue;
			}

			switch (true) {
				case is_bool($where[$where_fields[$i]]):
					$type = PDO::PARAM_BOOL;
					break;
				case is_int($where[$where_fields[$i]]):
					$type = PDO::PARAM_INT;
					break;
				default:
					$type = PDO::PARAM_STR;
					break;
			}
			$q->bindValue($num_fields + $i +1, $where[$where_fields[$i]], $type);
		}

		$success = $q->execute();

		if ($success) {
			return $q->rowCount();
		}
		else {
			return FALSE;
		}
	}

	/**
	 * Deletes data from a table.
	 *
	 * This deletes data from a table. This is equivilent to a DELETE FROM
	 * query. It will automatically escape arguments given.
	 *
	 * @param $table
	 *	 The table you wish to delete from.
	 * @param $where
	 *	 This is an associative array containing which data you wish to delete.
	 *	 This is identical to a WHERE...AND... statement, and must match all
	 *	 values.
	 *
	 * @return
	 *	 Returns the number of affected rows if successful, or FALSE on
	 *	 failure.
	 */
	public function delete($table, $where) {
		// Create WHERE area.
		$w = '';
		$fields = array_keys($where);
		$num_where = count($where);
		for ($i = 0; $i < $num_where; $i++) {
			if (is_null($where[$fields[$i]])) {
				$w .= sprintf(
					"%s IS NULL",
					$fields[$i]
					);
			}
			else {
				$w .= sprintf(
					"%s = ?",
					$fields[$i]
					);
			}

			if (($i+1) < $num_where) {
				$w .= ' AND ';
			}
		}

		// Create Query and execute.
		$query = "DELETE FROM {$table} WHERE {$w}";
		try {
			$q = $this->getPreparedQuery($query);
		}
		catch (KDatabaseException $e) {
			throw $e;
		}
		$this->incrementQueryCount($query);

		// Bind Params.
		for ($i = 0; $i < $num_where; $i++) {
			if (is_null($where[$fields[$i]])) {
				continue;
			}

			switch (true) {
				case is_bool($where[$fields[$i]]):
					$type = PDO::PARAM_BOOL;
					break;
				case is_int($where[$fields[$i]]):
					$type = PDO::PARAM_INT;
					break;
				default:
					$type = PDO::PARAM_STR;
					break;
			}
			$q->bindValue($i+1, $where[$fields[$i]], $type);
		}

		$success = $q->execute();

		if ($success) {
			return $q->rowCount();
		}
		else {
			return FALSE;
		}
	}

	/**
	 * Selects data from a table.
	 *
	 * This selects data from a table. It is equivilent to a SELECT .. FROM
	 * query. It will automatically escape arguments given.
	 *
	 * @param $table
	 *	 The table you wish to select data from.
	 * @param $where
	 *	 An associative array containing the data which you would like to
	 *	 match.
	 * @param $fields
	 *	 An array of fields, stating which specific fields you would like to
	 *	 retrieve.
	 *
	 * @return KDatabaseResult
	 *	 Returns a KDatabaseResult containing the results.
	 */
	 abstract public function select($table, $where = array(), $fields = null, $sort = null, $sortOrder = 'asc', $limit = 1000, $offset = 0);

	/**
	 * Returns the prepared query of $query.
	 *
	 * This can be useful if your application requires accessing the underlying
	 * PDOStatement object for functionality that isn't supported by KDB. Note
	 * that you will be responsible for catching PDOExceptions if you do
	 * choose this route.
	 *
	 * Also note that the query counter will not be updated if you execute the
	 * PDOStatement outside of KDB.
	 *
	 * @param $query
	 *	 The query to prepare.
	 *
	 * @return
	 *	 The PDOStatement Object.
	 */
	public function getPreparedQuery($query) {
		if (isset($this->queries[$query])) {
			$q = $this->queries[$query];
			$q->closeCursor();
		}
		else {
			try {
				$q = $this->handle->prepare($query);
			}
			catch (PDOException $e) {
				throw new KException(t('@query failed to execute.', array('@query' => $query)));
			}
			$this->queries[$query] = $q;
		}

		return $q;
	}

	/**
	 * Increments the execution count of $query.
	 *
	 * @param $query
	 *	 The query to increment the execution count for.
	 */
	protected function incrementQueryCount($query) {
		if (isset($this->queryExecCount[$query])) {
			$this->queryExecCount[$query]++;
		}
		else {
			$this->queryExecCount[$query] = 1;
		}
	}

	/**
	 * Begins a transaction. Causes an exception on failure.
	 *
	 * @return
	 *   Returns TRUE. Throws exception if fails.
	 */
	public function beginTransaction() {
		$result = $this->handle->beginTransaction();
		if (!$result) {
			throw new KDatabaseException('Could not begin transaction');
		}

		return TRUE;
	}

	/**
	 * Commits a transaction.
	 *
	 * @return
	 *   Returns TRUE on success, FALSE on failure.
	 */
	public function commitTransaction() {
		return $this->handle->commit();
	}

	/**
	 * Rolls back a transaction
	 *
	 * @return
	 *   Returns TRUE on success, FALSE on failure.
	 */
	public function rollbackTransaction() {
		return $this->handle->rollBack();
	}

	/**
	 * Returns the queries which have been executed.
	 *
	 * @return
	 *	 Returns the queries which have been executed via this DBI in an array.
	 */
	public function getQueries() {
		return array_keys($this->queries);
	}

	/**
	 * Get Query Execution Count
	 *
	 * @return
	 *	 Returns an array with queries as the keys, and the number of times
	 *	 the query has been executed as the value.
	 */
	public function getQueryExecutionCount() {
		return $this->queryExecCount;
	}

	/**
	 * Returns a boolean indicating if this connection object is connected.
	 *
	 * @return
	 *   Returns TRUE if currently connected, otherwise returns FALSE.
	 */
	public function isConnected() {
		return $this->connected;
	}
}