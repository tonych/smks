<?php
//-----------------------------------------------------------------------------
// Cryptography for KakeraSNS
//	 This package provides various cryptography functions to assist with 
//   securely keep data.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// General Metadata
//-----------------------------------------------------------------------------
$package['author']      = "Tony Chau";
$package['version']     = "1.0";
$package['name']	    = "Cryptography";
$package['description'] = 
	"This package provides various cryptography functions to assist with "
   ."securely keep data.";

//-----------------------------------------------------------------------------
// Dependencies
//-----------------------------------------------------------------------------
$package['dependencies'] = array();

//-----------------------------------------------------------------------------
// Normal Files which need to be included once a class has been loaded.
//-----------------------------------------------------------------------------
$package['files'] = array(
	'opensslWrapper.php',
	'uuid.php',
	);

//-----------------------------------------------------------------------------
// Autoload Table
//-----------------------------------------------------------------------------
$package['autoload'] = array(
	);

//------------------------------------------------------------------------------
// Misc.
//------------------------------------------------------------------------------
$package['load_immediately'] = TRUE;

return $package;
