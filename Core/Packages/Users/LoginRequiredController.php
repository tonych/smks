<?php
/**
 * K Login Required Controller
 *
 * This login required controller will redirect any users to a login form.
 */
class LoginRequiredController extends SessionController {
	public function __construct() {
		parent::__construct();
		
		if (!$this->isLoggedIn()) {
			kernel()->response()->redirectInternal($this->kernel->config()->get("app/defaultLogin", 'login/'));;
		}
	}
}

