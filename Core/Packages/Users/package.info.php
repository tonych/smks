<?php
//-----------------------------------------------------------------------------
// Users for KakeraSNS
//	 This package gets the users for KakeraSNS.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// General Metadata
//-----------------------------------------------------------------------------
$package['author']  = "Tony Chau";
$package['version'] = "1.0";
$package['name']	= "Users";
$package['description'] = 
	"This package overrides the default page dispatcher in KakeraSNS and "
   ."allows the use of friendly URLs.";

//-----------------------------------------------------------------------------
// Dependencies
//-----------------------------------------------------------------------------
$package['dependencies'] = array('KDB');

//-----------------------------------------------------------------------------
// Normal Files which need to be included once a class has been loaded.
//-----------------------------------------------------------------------------
$package['files'] = array(
	);

//-----------------------------------------------------------------------------
// Autoload Table
//-----------------------------------------------------------------------------
$package['autoload'] = array(
	'UsersObject'              => 'UsersObject.php',
	'UsersManager'             => 'UsersManager.php',
	'Group'                    => 'Group.php',
	'Session'                  => 'Session.php',
	'State'                    => 'State.php',
	'User'                     => 'User.php',
	'GroupNotFoundException'   => 'GroupNotFoundException.php',
	'GroupModificationException' => 'GroupModificationException.php',
	'SessionNotFoundException'   => 'SessionNotFoundException.php',
	'UserNotFoundException'      => 'UserNotFoundException.php',
	'UserModificationException'  => 'UserModificationException.php',
	'StateNotFoundException'     => 'StateNotFoundException.php',
	'LoginRequiredController'    => 'LoginRequiredController.php',
	'SessionController'          => 'SessionController.php',
	);

//------------------------------------------------------------------------------
// Misc.
//------------------------------------------------------------------------------
$package['load_immediately'] = FALSE;

return $package;
