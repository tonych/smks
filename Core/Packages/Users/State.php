<?php
class State extends UsersObject {
	protected $stateInfo;
	protected $stateName;
	
	public function __construct($dbconn, $stateName, $lazyLoad = TRUE) {
		parent::__construct($dbconn);
	
		$this->stateName = $stateName;
		$this->loadStateInfo();
	
		if (!$lazyLoad) {
			$this->doLazyLoad();
		}
	}
	
	public function isGroupState() {
		return (bool) $this->stateInfo['isGroupState'];
	}
	
	public function getGroups() {
		$groups = array();
		if ($this->isGroupState()) {
			//TODO Group States, DB Missing Table.
			//$rs = $this->db->select('groups')
		}
		
		return $groups;
	}
	
	public function getUsers() {
		$users = array();
		if (!$this->isGroupState()) {
			$rs = $this->db->select('users', array('stateName' => $this->stateName), array('userId'));
			while ($row = $rs->fetchAssoc()) {
				$users[] = $this->usersManager->getUser($row['userId']);
			}
		}
		
		return $users;
	}
	
	public function getStateName() {
		return $this->stateName;
	}
	
	public function getDescription() {
		return $this->stateInfo['description'];
	}
	
	public function setDescription($description) {
		$description = Filter::removeInvalidCharacters($description);
		
		// Update
		return $this->db->update('states', array('description' => $description), array('stateName' => $this->stateName));
	}
	
	protected function loadStateInfo() {
		// Get User Information
		$rs = $this->db->select('states',
			array(
				'stateName' => $this->stateName,
			),
			array('stateName', 'isGroupState', 'description')
			);
			
		$result = $rs->fetchAssoc();
		if ($result !== FALSE) {
			$this->stateInfo = $result;
			$this->stateName = $result['stateName'];
			return;
		}
				
		throw new StateNotFoundException($this->__toString());
	}
	
	protected function doLazyLoad() {
	}
	
	public function __toString() {
		return sprintf("State {%s}", $this->stateName);
	}
	
	public function __sleep() {
		// Return array of members to return.
		return array('dbname', 'stateName');
	}
	
	public function __wakeup() {
		$this->restoreConnection();
		$this->loadSessionInfo();
	}
}