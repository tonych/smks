<?php
/**
 * K Session Controller
 *
 * This default controller loads and imports a session from cookies if they
 * were sent with a client's request. If no cookie was sent with the request,
 * the user will be assumed to be in a guest state.
 *
 * Notes about Cookie Encoding:
 * The cookie is encoded in Base64. The internal representation is:
 *
 *    [1-byte length (int)][data][1-byte length (int)][data]
 *
 * The 1-byte integer represents the length of the data, then the data follows
 * in binary form.
 *
 * It is then encoded in Base64 to ensure that the binary data makes it over
 * HTTP headers fine.
 */
class SessionController extends Controller {
	protected $session = NULL;
	protected $user = NULL;

	/**
	 * @var UsersManager
	 */
	protected $usersManager;

	/**
	 * @var KDatabaseConnection
	 */
	protected $dbc;

	protected function isLoggedIn() {
		return ($this->session !== NULL || $this->user !== NULL);
	}

	/**
	 * @return User
	 */
	protected function getCurrentUser() {
		return $this->user;
	}

	/**
	 * @return Session
	 */
	protected function getCurrentSession() {
		return $this->session;
	}

	public function __construct() {
		// Run Controller Logic.
		parent::__construct();

		// Some references.
		$dbc = $this->kernel->getDatabaseConnection();
		$this->dbc = $dbc;
		$this->usersManager = new UsersManager($dbc->getTag());

		// Load User's Session
		$cookieName = $this->kernel->config()->get('app/sessionCookie', 'KSession');
		$this->session = Session::loadCookie($cookieName);
		if ($this->session !== NULL) {
			$this->user = $this->session->getUser();
			$this->session->setIpAddress($this->kernel->request()->getClientIp());
			$this->session->setUserAgent($this->kernel->request()->getClientAgent());
		}

		// Do User Settings
		if ($this->isLoggedIn()) {
			//$this->session->setExpiry(NULL);

			// Get User Settings.
			$settings = $this->user->getSettings();
			foreach ($settings as $key => $value) {
				if (strpos(trim($key), 'user/') === 0) {
					$this->kernel->config()->set(trim($key), $value);
				}
			}

			// Set Timezone.
			$success = date_default_timezone_set(
				$this->kernel->config()->get(
						'user/timezone',
						$this->kernel->config()->get('system/defaultTimezone', 'UTC')
					)
				);
		}
		
		// do not cache.
		$this->kernel->response()->setCache(FALSE);
	}
}
