<?php
class UsersManager {
	protected $dbname;
	protected $db;


	public function __construct($dbconn) {
		$this->dbname = $dbconn;
		try {
			$this->db = kernel()->getDatabaseConnection($this->dbname);
			
			if (!($this->db instanceof KDatabaseConnection)) {
				throw new KDatabaseException();
			}
		}
		catch (KDatabaseException $e) {
			throw new KInvalidArgumentException('The database given was invalid.');
		}
	}
	
	/**
	 * Gets a user.
	 * 
	 * @param $userId
	 *   The User ID you wish to get.
	 * 
	 * @return User 
	 */
	public function getUser($userId) {
		return new User($this->dbname, $userId);
	}
	
	public function lookupEmail($email) {
		// Get User Information
		$rs = $this->db->select('users',
			array(
				'email' => $email,
				),
			array('userId')
			);
	
		$result = $rs->fetchAssoc();
		if ($result !== FALSE) {
			return $result['userId'] * 1;
		}
		
		throw new UserNotFoundException("User with email $email does not exist.");
	}
	
	public function getGroup($groupId) {
		return new Group($this->dbname, $groupId);
	}
	
	public function getSession($sessionId) {
		return new Session($this->dbname, $sessionId);
	}
	
	public function getState($stateName) {
		return new State($this->dbname, $stateName);
	}
}