<?php
class Session extends UsersObject {
	protected $sessionInfo;
	protected $sessionId;
	protected $user;

	public static function loadCookie($cookieName, $dbconn = NULL) {
		if (isset($_COOKIE[$cookieName])) {
			$raw = base64_decode($_COOKIE[$cookieName]);

			if ($raw !== false) {
				/*
				* $i = starting Index.
					* $len = decoded length.
				* $data[] = contents of each field.
				*/
				$i = 0;
				$data = array();

				while($i < (strlen($raw) - 1)) {
					$len = ord(substr($raw, $i, 1));
					$data[] = substr($raw, $i + 1, $len);
					$i = $i + $len + 1;
				}

				if (count($data) == 2) {
					// Get SessionID and SessionKey.
					try {
						$session = new Session($dbconn, $data[0]);
						if ($data[1] == $session->getSessionKey() && !$session->hasExpired()) {
							return $session;
						}
					}
					catch (SessionNotFoundException $e) {
						// Clear their cookie
						setcookie($cookieName, '', time()-3600, '/');
					}
				}
			}
		}

		return NULL;
	}

	public function __construct($dbconn, $sessionId, $lazyLoad = TRUE) {
		parent::__construct($dbconn);

		$this->sessionId = $sessionId;
		$this->loadSessionInfo();

		if (!$lazyLoad) {
			$this->doLazyLoad();
		}
	}

	public function getStartTime() {
		return $this->sessionInfo['sessionStart'];
	}

	public function getSessionId() {
		return $this->sessionId;
	}

	public function getExpiry() {
		return $this->sessionInfo['expiry'];
	}

	public function hasExpired() {
		return kernel()->request()->getRequestTime() > $this->getExpiry();
	}

	public function setExpiry($time) {
		return $this->db->update('users_sessions', array('expiry' => $time), array('sessionId' => $this->sessionId));
	}

	public function getSessionKey() {
		return $this->sessionInfo['sessionKey'];
	}

	public function getUserAgent() {
		return $this->sessionInfo['userAgent'];
	}

	public function setUserAgent($ua) {
		return $this->db->update('users_sessions', array('userAgent' => $ua), array('sessionId' => $this->sessionId));
	}

	public function getIpAddress() {
		return $this->sessionInfo['ipAddress'];
	}

	public function setIpAddress($ip) {
		return $this->db->update('users_sessions', array('ipAddress' => $ip), array('sessionId' => $this->sessionId));
	}

	public function getUser() {
		if ($this->user === NULL) {
			$this->loadUser();
		}

		return $this->user;
	}

	public function saveCookie($cookieName) {
		// valid, create session
		$data = array(
			$this->getSessionId(),
			$this->getSessionKey(),
			);

		$output = '';
		foreach($data as $d) {
			$output .= chr(strlen($d));
			$output .= $d;
		}

		setcookie($cookieName, base64_encode($output), $this->getExpiry(), kernel()->config()->get('app/sessionPath', '/'));
	}

	protected function loadSessionInfo() {
		// Get User Information
		$rs = $this->db->select('users_sessions',
			array(
				'sessionId' => $this->sessionId,
			),
			array('sessionId', 'userId', 'sessionKey', 'sessionStart', 'expiry', 'userAgent', 'ipAddress')
			);

		$result = $rs->fetchAssoc();
		if ($result !== FALSE) {
			$this->sessionInfo = $result;
			$this->sessionId = $result['sessionId'];
			return;
		}

		throw new SessionNotFoundException($this->__toString());
	}

	protected function loadUser() {
		$this->user = $this->usersManager->getUser($this->sessionInfo['userId']);
	}

	protected function doLazyLoad() {
		$this->loadUser();
	}

	public function __toString() {
		return sprintf("Session ID {%s}", $this->sessionId);
	}

	public function __sleep() {
		// Return array of members to return.
		return array('dbname', 'sessionId');
	}

	public function __wakeup() {
		$this->restoreConnection();
		$this->loadSessionInfo();
	}
}