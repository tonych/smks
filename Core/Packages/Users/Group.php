<?php
class Group extends UsersObject {
	protected $groupInfo;
	protected $groupId;
	protected $states;
	protected $members;
	protected $owner;
	
	public function __construct($dbconn, $groupId, $lazyLoad = TRUE) {
		parent::__construct($dbconn);
	
		$this->groupId = $groupId * 1;
		$this->loadGroupInfo();
	
		if (!$lazyLoad) {
			$this->doLazyLoad();
		}
	}
	
	public function getName() {
		return $this->groupInfo['groupName'];
	}
	
	public function setName($name) {
		$name = Filter::removeInvalidCharacters($name);
		
		// Check Collisions
		$rs = $this->db->select(
				'groups',
				array(
						'ownerId' => $this->owner->getUserId(),
						'groupName' => $name
					)
				);
		if (!$rs->isValid() || $rs->fetchAssoc() === FALSE) {
			throw new GroupModificationException("Another group with that name exists.");
		}
		
		// Update
		return $this->db->update('groups', array('groupName' => $name), array('groupId' => $this->groupId));
	}
	
	public function listStates() {
		if ($this->states === NULL) {
			loadStates();
		}
		
		return $this->states;
	}
	
	public function listMembers() {
		if ($this->members === NULL) {
			loadMembers();
		}
		
		return $this->members;
	}
	
	public function getGroupId() {
		return $this->groupId * 1;
	}
	
	public function getOwner() {
		if ($this->owner === NULL) {
			loadOwner();
		}
		
		return $this->owner;
	}
	
	protected function loadGroupInfo() {
		// Get User Information
		$rs = $this->db->select('groups',
			array(
				'groupId' => $this->groupId * 1,
			),
			array('groupId', 'groupName', 'ownerId')
			);
			
		$result = $rs->fetchAssoc();
		if ($result !== FALSE) {
			$this->groupInfo = $result;
			$this->groupId = $result['groupId'] * 1;
			return;
		}
				
		throw new GroupNotFoundException($this->__toString());
	}
	
	protected function loadStates() {
		// Get State Information
		$rs = $this->db->select('groups_states',
			array(
				'groupId' => $this->groupId * 1,
			),
			array('stateName')
			);
		
		$this->states = array();
		while ($result = $rs->fetchAssoc()) {
			$this->states[] = $this->usersManager->getState($result['stateName']);
		}
	}
	
	protected function loadMembers() {
		// Get Group Information
		$rs = $this->db->select('users_groups',
			array(
				'groupId' => $this->groupId * 1,
				),
			array('memberId')
			);
		
		$this->members = array();
		while ($result = $rs->fetchAssoc()) {
			$this->members[] = $this->usersManager->getUser($result['memberId']);
		}
	}
	
	protected function loadOwner() {
		$rs = $this->db->select('users',
			array(
				'userId' => $this->groupInfo['ownerId'] * 1,
				),
			array('userId')
			);
		
		$result = $rs->fetchAssoc();
		if ($result !== FALSE) {
			$this->owner = $this->usersManager->getUser($result['userId']);
		}
		
		$this->owner = NULL;
	}
	
	protected function doLazyLoad() {
		$this->loadStates();
		$this->loadMembers();
	}
	
	public function __toString() {
		return sprintf("Group ID #%d", $this->groupId);
	}
	
	public function __sleep() {
		// Return array of members to return.
		return array('dbname', 'groupId');
	}
	
	public function __wakeup() {
		$this->restoreConnection();
		$this->loadGroupInfo();
	}
}