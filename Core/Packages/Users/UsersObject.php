<?php
class UsersObject {
	/**
	 * @var KDatabaseConnection
	 */
	protected $db;
	protected $dbname;
	
	/**
	 * @var UsersManager
	 */
	protected $usersManager;
	
	public function __construct($dbconn) {
		$this->dbname = $dbconn;
		$this->restoreConnection();
	}
	
	protected function restoreConnection() {
		try {
			$this->db = kernel()->getDatabaseConnection($this->dbname);
			
			if (!($this->db instanceof KDatabaseConnection)) {
				throw new KDatabaseException();
			}
		}
		catch (KDatabaseException $e) {
			throw new KInvalidArgumentException('The database given was invalid.');
		}
		
		$this->usersManager = new UsersManager($this->dbname);
	}
}