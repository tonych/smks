<?php
class User extends UsersObject {
	protected $userInfo;
	protected $userId;
	protected $membersOf;
	protected $groups;
	protected $settings;
	protected $states;
	protected $sessions;

	public function __construct($dbconn, $userId, $lazyLoad = TRUE) {
		parent::__construct($dbconn);

		$this->userId = $userId * 1;
		$this->loadUserInfo();

		if (!$lazyLoad) {
			$this->doLazyLoad();
		}
	}

	public function isMemberOf() {
		// List all groups Member of.
		if ($this->membersOf === NULL) {
			$this->loadMembersOf();
		}

		return $this->membersOf;
	}

	public function listGroups() {
		// List all owned Groups.
		if ($this->groups === NULL) {
			$this->loadGroups();
		}

		return $this->groups;
	}

	public function listStates() {
		// List all States.
		if ($this->states === NULL) {
			$this->loadStates();
		}

		return $this->states;
	}

	public function listSessions() {
		// List all Sessions.
		if ($this->sessions === NULL) {
			$this->loadSessions();
		}

		return $this->sessions;
	}

	public function createSession($sessionTime = 3600) {
		// Create new session.
		$sessionStart = kernel()->request()->getRequestTime();
		$userAgent = kernel()->request()->getClientAgent();
		$ipAddress = kernel()->request()->getClientIp();
		$sessionKey = sha1($this->userId . get_random_bytes(20, $hex = FALSE));
		$expiry = $sessionStart + $sessionTime;

		// Generate Session ID and make sure it is unique.
		$sessionNotFound = TRUE;
		do {
			$sessionId = get_uuid();

			try {
				$this->usersManager->getSession($sessionId);
			}
			catch (SessionNotFoundException $e) {
				$sessionNotFound = FALSE;
			}
		} while ($sessionNotFound);

		// Insert it into the database.
		$rs = $this->db->insert('users_sessions', array(
			'sessionId' => $sessionId,
			'userId' => $this->getUserId(),
			'sessionKey' => $sessionKey,
			'sessionStart' => $sessionStart,
			'expiry' => $expiry,
			'userAgent' => $userAgent,
			'ipAddress' => $ipAddress,
		));

		if ($rs !== FALSE) {
			return $this->usersManager->getSession($sessionId);
		}

		throw new SessionNotFoundException("Did not newly created session ID ({$sessionId}).");
	}

	public function hasState($state) {
		if ($this->states === NULL) {
			$this->loadStates();
		}

		if ($state instanceof State) {
			return in_array($state, $this->states);
		}
		elseif (is_string($state)) {
			foreach ($this->states as $s) {
				if ($s->getStateName() == $state) {
					return TRUE;
				}
			}
		}

		return FALSE;
	}

	// @TODO: Add State to User->addState($state)
	public function addState($state) {
		// Add State
		if ($state instanceof State && $state->isUserState()) {
			// Add State Here

			$this->states[] = $state;
		}
		else {
			throw new KInvalidArgumentException("The state given is not valid for a user.");
		}
	}

	// @TODO: Remove State to User->removeState($state)
	public function removeState($state) {
		// Remove State
	}

	public function getUserId() {
		return $this->userId * 1;
	}

	public function getEmail() {
		return $this->userInfo['email'];
	}

	// @TODO: Write Setter for User->setEmail($email)
	public function setEmail($email) {

	}

	public function getPassword() {
		return $this->userInfo['password'];
	}

	public function getSalt() {
		return $this->userInfo['salt'];
	}

	public function setPassword($password) {
		// Do Set Password and Set Salt.
		$salt = get_random_bytes(20, TRUE);
		$passwordhex = hash(
			"sha512",
			$salt . $password . kernel()->config()->get('app/pepper', 'abcdef')
			);

		// Update
		return $this->db->update('users', array('password' => $passwordhex, 'salt' => $salt), array('userId' => $this->userId));
	}

	public function getRealName() {
		return $this->userInfo['realName'];
	}

	public function setRealName($name) {
		$name = Filter::removeInvalidCharacters($name);

		// Check Collisions
		$rs = $this->db->select(
				'users',
				array(
						'realName' => $name,
					)
				);
		if (!$rs->isValid() || $rs->fetchAssoc() === FALSE) {
			throw new UserModificationException("Another user with that name exists.");
		}

		// Update
		return $this->db->update('users', array('realName' => $name), array('userId' => $this->userId));
	}

	// @TODO: Do Lazy Loading on User->getSettings()
	public function getSettings() {
		if ($this->settings === NULL) {
			$this->loadSettings();
		}

		return $this->settings;
	}

	protected function doLazyLoad() {
		// Do Lazy Loading.
		$this->loadMembersOf();
		$this->loadGroups();
		$this->loadStates();
		$this->loadSessions();
		$this->loadSettings();
	}

	protected function loadSettings() {
		// Get User Information
		$rs = $this->db->select('users_settings',
			array(
				'userId' => $this->userId * 1,
				),
			array('settingName', 'settingValue')
			);

		$this->settings = array();
		while ($row = $rs->fetchAssoc()) {
			$this->settings[$row['settingName']] = $row['settingValue'];
		}
	}

	protected function loadUserInfo() {
		// Get User Information
		$rs = $this->db->select('users',
			array(
				'userId' => $this->userId * 1,
				),
			array('userId', 'email', 'password', 'salt', 'realName')
			);

		$result = $rs->fetchAssoc();
		if ($result !== FALSE) {
			$this->userInfo = $result;
			$this->userId = $result['userId'] * 1;
			return;
		}

		throw new UserNotFoundException($this->__toString());
	}

	protected function loadMembersOf() {
		// Get Group Information
		$rs = $this->db->select('users_groups',
			array(
				'memberId' => $this->userId * 1,
				),
			array('groupId')
			);

		$this->membersOf = array();
		while ($result = $rs->fetchAssoc()) {
			$this->membersOf[] = $this->usersManager->getGroup($result['groupId']);
		}
	}

	protected function loadGroups() {
		// Get Group Information
		$rs = $this->db->select('groups',
			array(
				'ownerId' => $this->userId * 1,
				),
			array('groupId')
			);

		$this->groups = array();
		while ($result = $rs->fetchAssoc()) {
			$this->groups[] = $this->usersManager->getGroup($result['groupId']);
		}
	}

	protected function loadStates() {
		// Get State Information
		$rs = $this->db->select('users_states',
			array(
				'userId' => $this->userId * 1,
			),
			array('stateName')
			);

		$this->states = array();
		while ($result = $rs->fetchAssoc()) {
			$this->states[] = $this->usersManager->getState($result['stateName']);
		}
	}

	protected function loadSessions() {
		// Get Sessions Information
		$rs = $this->db->select('users_sessions',
			array(
				'userId' => $this->userId * 1,
			),
			array('sessionId'),
			'expiry',
			'desc'
			);

		$this->sessions = array();
		while ($result = $rs->fetchAssoc()) {
			$this->sessions[] = $this->usersManager->getSession($result['sessionId']);
		}
	}

	public function __toString() {
		return sprintf("User ID #%d", $this->userId);
	}

	public function __sleep() {
		// Return array of members to return.
		return array('dbname', 'userId');
	}

	public function __wakeup() {
		$this->restoreConnection();
		$this->loadUserInfo();
	}
}