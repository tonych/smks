<?php
function web_onReady($args) {
	// Start Application Sequence.
	try {
		$controller = kernel()->loadController(kernel()->getDispatchController());
		$action = $controller->loadAction(kernel()->getDispatchAction());
	}
	catch (KException $e) {
		$status = kernel()->callHook('onControllerException', array('exception' => $e), TRUE);
		if ($status === NULL || $status === FALSE)
			throw $e;
	}
}

kernel()->addHook('onReady', 'web_onReady');