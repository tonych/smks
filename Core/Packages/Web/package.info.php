<?php
//-----------------------------------------------------------------------------
// Web for KakeraSNS
//	 This package contains various extensions to the kernel to handle web
//   dispatching and execution.
//-----------------------------------------------------------------------------
return array(
	//-------------------------------------------------------------------------
	// General Metadata
	//   This describes your package and who made it and what it does. It is
	//   generally used for updating and informational purposes.
	//-------------------------------------------------------------------------
	'author'      => "Tony Chau",
	'version'     => "1.0",
	'name'        => "Web",
	'description' => 
		"This package contains various extensions to the kernel to handle web "
	   ."dispatching and execution.",

	//-------------------------------------------------------------------------
	// Dependencies
	//   This is a list of all the other packages which must be loaded before
	//   this package can load. If a dependency package does not exist, an
	//   exception will occur.
	//-------------------------------------------------------------------------
	'dependencies' => array(),
	
	//-------------------------------------------------------------------------
	// Files
	//   This is a list of files which will be included once this package loads.
	//   By default, packages are loaded when a class containing in the Autoload
	//   Table is called. However you can change that by altering the 
	//   loadImmediately variable.
	//-------------------------------------------------------------------------
	'files' => 
		array(
			'WebExtension.php',
			'onReady.php',
			),
	
	//-------------------------------------------------------------------------
	// Autoload Table
	//   This is a list of classes and the files they are contained in. When a 
	//   class within this list is called, it will automatically load this 
	//   package.
	//-------------------------------------------------------------------------
	'autoload' => array(
		'Controller'                      => 'Controller.php',
		'KViewNotFoundException'          => 'KViewNotFoundException.php',
		'KControllerNotFoundException'    => 'KControllerNotFoundException.php',
		),
	
	//--------------------------------------------------------------------------
	// Misc.
	//   This section contains a list of new options that have yet to be 
	//   categorized.
	//--------------------------------------------------------------------------
	'load_immediately' => TRUE,
);
