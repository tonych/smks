<?php
class WebExtension extends Extension {
	public function hasMethods() {
		return array('getDispatchController', 'getDispatchAction', 'getDispatchUrl', 'loadController');
	}

	public function getPackage() {
		return 'Web';
	}

	/**
	* This calls a hook to handle dispatching of the controller.
	*/
	public function getDispatchController() {
		$controller = kernel()->callHook('onGetDispatchController', array(), true);
		if ($controller !== NULL) {
			return $controller;
		}

		// Default if Hooks cannot do it.
		if (isset($_GET['pg'])) {
			$controller = trim($_GET['pg']);
			$valid = !preg_match('#([^a-zA-Z0-9\_\-])#', $_GET['pg']);
			if ($valid) {
				return $controller;
			}
		}
		else {
			return kernel()->config()->get('app/defaultController', 'default');
		}
	}

	/**
	* This calls a hook to handle dispatching of the action
	*/
	public function getDispatchAction() {
		$action = kernel()->callHook('onGetDispatchAction', array(), true);
		if ($action !== NULL) {
			return $action;
		}

		// Default if Hooks cannot do it.
		if (isset($_GET['action'])) {
			$action = trim($_GET['action']);
			$valid = !preg_match('#([^a-zA-Z0-9\_])#', $_GET['action']);
			if ($valid) {
				return $action;
			}
		}
		else {
			return kernel()->config()->get('app/defaultAction', 'defaultPage');
		}
	}

	/**
	 * This gets the URL to call the specified Controller and Action.
	 *
	 * @param $controller
	 * @param $action
	 */
	public function getDispatchUrl($controller, $action) {
		$url = kernel()->callHook('onGetDispatchUrl', array("controller" => $controller, "action" => $action), true);
		if ($url !== FALSE || $url !== NULL) {
			return $url;
		}

		return kernel()->request()->getUri()->getRoot(sprintf('?pg=%s&amp;action=%s', urlencode($controller), urlencode($action)));
	}

	/**
	 * Loads a controller and executes it's code.
	 * @param $controller
	 */
	public function loadController($controller) {
		$controllerPath = CONTROLLER_PATH;

		if (file_exists($controllerPath . ucfirst($controller) . '.php'))
			$controller = ucfirst($controller);

		kernel()->callHook('onControllerLoad', array("controller" => &$controller, "path" => &$controllerPath));

		if (kernel()->checkPath($controllerPath . $controller . '.php', $controllerPath)) {
			include $controllerPath . $controller . '.php';
			$controller_class = ucfirst($controller) . 'Controller';
			if (class_exists($controller_class) && is_subclass_of($controller_class, 'Controller')) {
				return new $controller_class();
			}
		}

		throw new KControllerNotFoundException(
			t('The controller @controller does not exist.', array('@controller' => $controller))
			);
	}
}

// Register Extension.
kernel()->attachExtension(new WebExtension());