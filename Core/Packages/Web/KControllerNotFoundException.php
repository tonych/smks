<?php
/**
 * KControllerNotFoundException - Controller Not Found Exception
 *
 * This occurs when a controller was requested but could not be found.
 */
class KControllerNotFoundException extends KNotFoundException {}