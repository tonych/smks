<?php
/**
 * KViewNotFoundException - View Not Found Exception
 *
 * This occurs when a view was requested but could not be found.
 */
class KViewNotFoundException extends KNotFoundException {}