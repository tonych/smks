<?php
/**
 * @file
 * Contains a class for running web applications.
 */

/**
 * Controller - Web Controller
 *
 * This class provides a base class for controllers with default functionality.
 */
class Controller extends Extensible {
	protected $controller;
	protected $action;
	protected $layout;
	protected $view;

	/**
	 * @var Kernel
	 */
	protected $kernel;

	protected $styles = array();
	protected $title = "";
	protected $meta = array();
	protected $js = array();

	protected $returnView = FALSE;

	protected $enableLayout = TRUE;

	public function __construct() {
		$this->kernel = kernel();

		$this->controller = get_class($this);
		$this->layout = $this->kernel->config()->get('app/defaultLayout', 'layouts/main');
	}

	public function loadAction($action) {
		$this->action = trim($action);
		if (empty($this->action)) {
			$this->action = 'index';
		}

		if (method_exists($this, $this->action)) {
			$callback = array($this, $this->action);
			$method = new ReflectionMethod($this, $this->action);
		}
		elseif (method_exists($this, 'defaultPage')) {
			$callback = array($this, 'defaultPage');
			$method = new ReflectionMethod($this, 'defaultPage');
		}

		if (isset($method) && $method->isPublic()) {
			ob_start();
			$return_code = call_user_func($callback);

			if ($this->enableLayout) {
				$content = ob_get_clean();
				$this->loadStyle($this->layout);

				$styles = "";
				foreach ($this->styles as $s) {
					$styles .= $s;
				}

				$meta = "";
				foreach ($this->meta as $name => $value) {
					$meta .= sprintf(
						'%s<meta name="%s" content="%s" />%s',
						"\t\t",
						f($name),
						f($value),
						"\n"
						);
				}

				$js = "";
				foreach ($this->js as $s) {
					$js .= sprintf(
						'%s<script type="text/javascript" src="%s"></script>%s',
						"\t\t",
						$s,
						"\n"
						);
				}

				$status = $this->loadView(
					$this->layout,
					array(
						'content' => $content,
						'styles' => $styles,
						'title' => $this->title,
						'meta' => $meta,
						'js' => $js,
						)
					);

				if (!$status) {
					throw new KViewNotFoundException(t('Layout @layout could not be found.', array('@layout' => $this->layout)));
				}
			}

			return $return_code;
		}
		else {
			throw new KAccessDeniedException(t(
				'@method access level does not allow calling via. URL in @controller',
				array('@method' => $this->action, '@controller' => get_class($this))
				));
		}

		throw new KControllerNotFoundException(t(
			'@method could not be found in @controller',
			array('@method' => $this->action, '@controller' => get_class($this))
			));
	}

	protected function loadView($view, $data = array(), $return = FALSE) {
		// Check View Path.
		$view_path = VIEW_PATH . $view . '.php';
		if (!$this->kernel->checkPath($view_path, VIEW_PATH)) {
			throw new KViewNotFoundException(t(
				'@view was not found.',
				array('@view' => $view)
				));
		}

		// Load Styles
		$this->loadStyle($view);

		// Save State.
		$this->view = $view_path;
		$this->returnView = $return;

		// Clear (in case they get replaced by the variables below).
		unset($return);
		unset($view);
		unset($view_path);

		// Set Variables
		//foreach ($data as $k => $v) {
		//	$$k = $v;
		//}

		// Clear array.
		//unset($data);
		//unset($k);
		//unset($v);
		$urlroot = $this->kernel->request()->getUri()->getRoot();
		extract($data);
		unset($data);

		// Check if we are returning.
		ob_start();
		include $this->view;

		$content = ob_get_clean();

		// Call Hooks to process Content.
		$this->kernel->callHook('onLoadView', array('content' => &$content));

		if ($this->returnView) {
			return $content;
		}

		echo $content;
		return TRUE;
	}

	protected function loadStyle($style) {
		// Load CSS Styles if they exist.
		$css_path = VIEW_PATH . $style . '.css';
		if ($this->kernel->checkPath($css_path, VIEW_PATH) && !array_key_exists($style, $this->styles)) {
			$this->styles[$style] = str_replace('/*URLROOT*/', $this->kernel->request()->getUri()->getRoot(), file_get_contents($css_path));
		}
	}

	protected function setEnableLayout($status) {
		$this->enableLayout = $status;
	}

	protected function setLayout($template) {
		// Check View Path.
		$view_path = VIEW_PATH . $template . '.php';
		if (!$this->kernel->checkPath($view_path, VIEW_PATH)) {
			throw new KViewNotFoundException(t(
				'@view was not found when attemping to set layout.',
				array('@view' => $template)
				));
		}

		$this->layout = $template;
		return TRUE;
	}

	/**
	 * Adds Meta Tag
	 *
	 * Adds a Meta Tag to the HTML. This only allows name="" type meta tags.
	 * For http-equiv tags, please send HTTP headers instead via header().
	 *
	 * @param $name
	 *   This is the name of the meta tag you wish to add.
	 * @param $content
	 *   This contains the content of the meta tag.
	 */
	protected function setMeta($name, $content) {
		$this->meta[$name] = $content;
	}

	/**
	 * Loads an external JS file.
	 *
	 * This loads a javascript file into the website. This must be a URL.
	 *
	 * @param $src
	 *   This is the URL of the javascript file you wish to add.
	 */
	protected function loadJS($src) {
		if (!in_array($src, $this->js)) {
			$this->js[] = $src;
		}
	}

	protected function setTitle($title) {
		$this->title = $title;
	}

	protected function getTitle() {
		return $this->title;
	}
}