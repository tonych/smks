<?php
/**
 * KNotSupportedException - Not Supported Exception
 *
 * This occurs when the user attempts to execute an action which is not 
 * supported. This could be due to missing dependencies or invalid information.
 */
class KNotSupportedException extends KException {}