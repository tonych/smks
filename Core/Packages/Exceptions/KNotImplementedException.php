<?php
/**
 * KNotImplementedException - Not Implemented Exception
 *
 * This occurs when an application tries to access functionality which has not 
 * yet been implemented yet.
 */
class KNotImplementedException extends KException {}