<?php
/**
 * KNotFoundException - Exception when a resource could not be found.
 *
 * This exception occurs when a file, resource or anything could not be found.
 */
class KNotFoundException extends KException {}