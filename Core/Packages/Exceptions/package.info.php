<?php
//-----------------------------------------------------------------------------
// Exceptions for KakeraSNS
//	 This package contains exceptions for use within KakeraSNS.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// General Metadata
//-----------------------------------------------------------------------------
$package['author']  = "Tony Chau";
$package['version'] = "1.0";
$package['name']	   = "Exceptions";
$package['description'] = 
	"This package contains exceptions which can be used within KakeraSNS. "
   ."This is a core package in KakeraSNS, if removed will render KakeraSNS "
   ."inoperable.";

//-----------------------------------------------------------------------------
// Dependencies
//-----------------------------------------------------------------------------
$package['dependencies'] = array();

//-----------------------------------------------------------------------------
// Normal Files which need to be included once a class has been loaded.
//-----------------------------------------------------------------------------
$package['files'] = array();

//-----------------------------------------------------------------------------
// Autoload Table
//-----------------------------------------------------------------------------
$package['autoload'] = array(
	'KException' => 'KException.php',
	'KAccessDeniedException'    => 'KAccessDeniedException.php',
	'KInvalidArgumentException' => 'KInvalidArgumentException.php',
	'KNotFoundException'        => 'KNotFoundException.php',
	'KNotImplementedException'  => 'KNotImplementedException.php',
	'KNotSupportedException'    => 'KNotSupportedException.php',
	'KModificationException'	=> 'KModificationException.php',
	'KUnexpectedErrorException' => 'KUnexpectedErrorException.php',
	);

//------------------------------------------------------------------------------
// Misc.
//------------------------------------------------------------------------------
$package['load_immediately'] = TRUE;

return $package;
