<?php
/**
 * KInvalidArgumentException - Invalid Argument Exception
 *
 * This occurs when the user passes invalid arguments to a method or function.
 */
class KInvalidArgumentException extends KException {}