<?php
/**
 * KException Class - Generic Kakera Exception
 *
 * This occurs when a generic Kakera exception has occured.
 */
class KException extends Exception {}