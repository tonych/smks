<?php
//-----------------------------------------------------------------------------
// FriendlyDispatcher for KakeraSNS
//	 This package overrides the default page dispatcher in KakeraSNS and 
//   allows the use of friendly URLs.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// General Metadata
//-----------------------------------------------------------------------------
$package['author']      = "Tony Chau";
$package['version']     = "1.0";
$package['name']	    = "Friendly Dispatcher";
$package['description'] = 
	"This package overrides the default page dispatcher in KakeraSNS and "
   ."allows the use of friendly URLs.";

//-----------------------------------------------------------------------------
// Dependencies
//-----------------------------------------------------------------------------
$package['dependencies'] = array();

//-----------------------------------------------------------------------------
// Normal Files which need to be included once a class has been loaded.
//-----------------------------------------------------------------------------
$package['files'] = array(
	'onGetDispatchURL.php',
	'onGetDispatchController.php',
	'onGetDispatchAction.php',
	);

//-----------------------------------------------------------------------------
// Autoload Table
//-----------------------------------------------------------------------------
$package['autoload'] = array(
	);

//------------------------------------------------------------------------------
// Misc.
//------------------------------------------------------------------------------
$package['load_immediately'] = TRUE;

return $package;
