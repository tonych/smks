<?php
function fd_onGetDispatchAction($args) {
	extract($args);
	
	$action = $kernel->request()->getUri()->getPart(1);
	if (strlen($action) <= 0)
		$action = NULL;
	
	return $action;
}

// Add to Kernel
$this->kernel->addHook('onGetDispatchAction', 'fd_onGetDispatchAction');