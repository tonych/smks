<?php
function fd_onGetDispatchUrl($args) {
	extract($args);
	return $kernel->request()->getUri()->getRoot(sprintf('%s/%s', urlencode($controller), urlencode($action)));
}

// Add to Kernel
$this->kernel->addHook('onGetDispatchUrl', 'fd_onGetDispatchUrl');