<?php
function fd_onGetDispatchController($args) {
	extract($args);
	
	$controller = $kernel->request()->getUri()->getPart(0);
	if ($controller === NULL) {
		$controller = $kernel->config()->get('app/default_controller');
	}
	
	return $controller;
}

// Add to Kernel
$this->kernel->addHook('onGetDispatchController', 'fd_onGetDispatchController');