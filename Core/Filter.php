<?php
/**
 * Filter - Filtering Class
 *
 * This filters data to prevent XSS attacks in output data. In general, data 
 * should be escaped on output, validated on input. 
 */
class Filter {
	/**
	 * Filters text and removes changes for XSS to get through.
	 *
	 * This function uses iconv to check for well-formed multi-byte UTF-8 and so,
	 * the iconv extension has to be enabled for this to work.
	 *
	 * @param $text
	 *   The text to filter.
	 *
	 * @return
	 *   The filtered text.
	 */
	public static function filterText($text) {
		return htmlspecialchars(self::removeInvalidCharacters($text), ENT_QUOTES, 'UTF-8');
	}
	
	/**
	 * Removes invalid UTF8 characters from strings.
	 *
	 * This function uses iconv to check for well-formed multi-byte UTF-8 and
	 * so the iconv extension must be installed for this to work.
	 *
	 * @param $text
	 *   The text to remove invalid UTF-8 Characters.
	 *
	 * @return
	 *   A well-formed UTF-8 string.
	 */
	public static function removeInvalidCharacters($text) {
		ini_set('mbstring.substitute_character', "none");
		return mb_convert_encoding($text, 'UTF-8', 'UTF-8'); 
		//return iconv('UTF-8', 'UTF-8//IGNORE', $text);
	}
}