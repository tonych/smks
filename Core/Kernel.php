<?php
/**
 * Kernel - KakeraSNS Kernel
 *
 * This is the core of the kernel, hooks, packages and dispatching. It
 * also has autoloading and templating.
 *
 * @author Tony Chau
 */
class Kernel extends Extensible {
	const MIN_PHP_VER = '5.3.0';
	const VERSION     = '1.0';
	private $hooks = array();

	private $config;
	private $packages;
	private $log;

	private $request;
	private $response;

	/**
	 * Constructor
	 *
	 * This sets up the kernel and prepares it for processing. Before
	 * calling or initializing this method, the constant BASE_PATH and
	 * CORE_PATH must exist.
	 *
	 * If they do not exist, this will stop execution of the script,
	 * and print out a fatal error message.
	 *
	 * There are no external modules or OS dependancies.
	 */
	public function __construct() {
		// Check Paths.
		if ((!defined('BASE_PATH') && file_exists(BASE_PATH) && is_dir(BASE_PATH)) ||
		    (!defined('CORE_PATH')  && file_exists(CORE_PATH) && is_dir(CORE_PATH))) {
			die('Fatal Error: Could not find paths to kernel.');
		}

		// Handlers
		set_exception_handler(array($this, 'kExceptionHandler'));
		spl_autoload_register(array($this, 'kAutoloadHandler'));

		// Magic Quote Cleanup.
		// Due to using Closures, this requires PHP 5.3.0+. As Magic Quotes no
		// longer exist PHP 6, this will be strictly PHP <5.3 and >6
		if((function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) ||
                (ini_get('magic_quotes_sybase') && (strtolower(ini_get('magic_quotes_sybase')) != "off")))
		{
			$array_unquote = function (&$data, $key) {
					$data = stripslashes($data);
				};

			array_walk_recursive($_GET, $array_unquote);
			array_walk_recursive($_POST, $array_unquote);
			array_walk_recursive($_COOKIE, $array_unquote);
		}

		// Clean Global Space.
		$keys = array_keys($GLOBALS);
		$allowed = array('GLOBALS', '_POST', '_GET', '_REQUEST', '_ENV', '_SERVER', '_COOKIE', '_FILES');

		foreach($keys as $k) {
			if (!in_array($k, $allowed)) {
				unset($GLOBALS[$k]);
			}
		}

		// Set MBString Encoding.
		mb_internal_encoding("UTF-8");

		// Start Logger
		$this->log = new Log();
		$this->log()->add('Logging Started.');

		// Load Configuration
		$this->config = new Config();

		// Find and load all configuration files.
		$config_files = scandir(CONFIG_PATH);

		foreach ($config_files as $f) {
			if (!is_dir(CONFIG_PATH . $f) && pathinfo(CONFIG_PATH . $f, PATHINFO_EXTENSION) == 'php') {
				$this->config->load(CONFIG_PATH . $f);
			}
		}

		// Set Default Timezone
		date_default_timezone_set($this->config->get("system/defaultTimezone", "UTC"));

		// Start Package Manager
		$this->log()->add('Package Manager Starting...');
		$this->packages = new PackageManager($this);
		$this->log()->add('Package Manager Started.');

		// Load Request and Response
		$this->request = new WebRequest($this);
		$this->response = new WebResponse($this);
	}

	/**
	 * Accessor for the Log.
	 *
	 * @return Log
	 */
	public function log() {
		return $this->log;
	}

	/**
	 * Accessor for the configuration.
	 *
	 * @return Config
	 */
	public function config() {
		return $this->config;
	}

	/**
	 * Accessor for the Packages.
	 *
	 * @return PackageManager
	 */
	public function packages() {
		return $this->packages;
	}

	/**
	 * Accessor for the WebRequest Object
	 *
	 * @return WebRequest
	 */
	public function request() {
		return $this->request;
	}

	/**
	 * Accessor for the WebResponse Object
	 *
	 * @return WebResponse
	 */
	public function response() {
		return $this->response;
	}

	/**
	 * Shutdowns the Server
	 *
	 * Shutdowns the server with the specified reason. Although in this
	 * version it is identical to die(), in future, a more advanced
	 * version of this method may be implemented.
	 *
	 * @param $reason
	 */
	public function shutdown($reason) {
		$this->callHook("onKernelShutdown", array($reason));
	}

	/**
	 * Checks that $path is contained within $base.
	 * @param $path
	 * @param $base
	 */
	public function checkPath($path, $base = NULL) {
		if ($base === NULL) {
			$base = BASE_PATH;
		}

		return (file_exists($path) && strpos(realpath($path), realpath($base)) === 0);
	}

	/**
	 * Adds a callback to be executed on $hook.
	 * @param $hook
	 * @param $callback
	 */
	public function addHook($hook, $callback) {
		if (is_callable($callback)) {
			if (isset($this->hooks[$hook])) {
				$this->hooks[$hook] = array($callback);
			}
			else {
				$this->hooks[$hook][] = $callback;
			}
		}
	}

	/**
	 * Calls a hook's callback functions.
	 * @param $hook
	 * @param $arguments
	 */
	public function callHook($hook, $arguments = array(), $stopWhenTrue = false) {
		$arguments['kernel'] = $this;
		if (isset($this->hooks[$hook])) {
			foreach ($this->hooks[$hook] as $c) {
				if (is_callable($c)) {
					$result = call_user_func($c, $arguments);
					if ($result !== NULL && $stopWhenTrue)
						return $result;
				}
			}
		}

		return NULL;
	}

	//-----------------------------------------------------------------------//
	//                         KERNEL HANDLERS                               //
	//                   NOT MEANT TO BE CALLED DIRECTLY                     //
	//-----------------------------------------------------------------------//
	public function kExceptionHandler($exception) {
		$this->callHook("onUnhandledException", array("exception" => $exception));
		if ($_SERVER["SERVER_NAME"] == "localhost" && (
			$_SERVER["REMOTE_ADDR"] == "::1" || $_SERVER["REMOTE_ADDR"] == "127.0.0.7" || $_SERVER["REMOTE_ADDR"] == "localhost"
		)) {
			echo "<pre>";
			die(print_r($exception, true));
		}
	}

	public function kAutoloadHandler($name) {
		$path = sprintf('%s%s.php', CORE_PATH, $name);
		if ($this->checkPath($path, CORE_PATH)) {
			include_once $path;
			return TRUE;
		}

		return FALSE;
	}

}


/**
 * Translates, and processes strings.
 *
 * This function is a mix of sprintf() and filtering. It filters
 * for invalid characters, and performs filtering on strings.
 *
 * You should never pass variables as the text string.
 *
 * There are 2 types of placeholders which you can place.
 *
 * @var: Where var is the name of the content you want to put, it filters the
 *			 content before subsituting.
 * !var: Where var is the name of the content you want to put in, it places the
 *			 content in as is.
 */
function t($text, $array = array()) {
	foreach ($array as $k => $v) {
		switch($k[0]) {
			case '@':
				$v = Filter::filterText($v);
				break;
		}

		$text = str_replace($k, $v, $text);
	}

	return $text;
}

/**
 * Returns the kernel being used this instance.
 * @return Kernel
 */
function kernel() {
	if (isset($GLOBALS['kernel']))
		return $GLOBALS['kernel'];
	else
		return $GLOBALS['kernel'] = new Kernel();
}
