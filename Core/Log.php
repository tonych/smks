<?php
/**
 * Kakera Logging Class
 *
 * This is a class which handles logging of messages and exceptions. It allows
 * you to attach LogStreams which let you log messages to a location defined 
 * by the stream. 
 *
 * There are 4 levels of log severity. LOG_DEBUG, LOG_INFO, LOG_WARN, and 
 * LOG_ERROR. When setting the log level, you can set which types are logged by
 * using bitwise operators. For example, LOG_ERROR | LOG_WARN would log errors
 * and warnings. 
 *
 * Alternatively, you could use LOG_ALL ~ LOG_DEBUG. Which logs all except for
 * debug messages.
 */
class Log {
	const LOG_DEBUG = 0x1;	// 1
	const LOG_INFO  = 0x10;	// 2
	const LOG_WARN  = 0x100;	// 4
	const LOG_ERROR = 0x1000; // 8
	const LOG_ALL   = 0x1111; // 15
	
	protected $logStartTime;
	protected $logSize;
	protected $messages;
	protected $index = 0;
	
	protected $logLevel;
	protected $streams = array();
	
	public function __construct($logLevel = Log::LOG_ALL) {
		$this->logStartTime = microtime(true);
		$this->logLevel = $logLevel;
		$this->messages = array();
	}
	
	public function getLogSize() {
		return $this->logSize;
	}
	
	public function getMessages() {
		return $this->messages;
	}
	
	public function getLogLevel() {
		return $this->logLevel;
	}
	
	public function getLogStartTime() {
		return $this->logStartTime;
	}
	
	public function add($message, $level = Log::LOG_INFO) {
		$validLevels = array(Log::LOG_DEBUG, Log::LOG_INFO, Log::LOG_WARN, Log::LOG_ERROR);
		if (in_array($level, $validLevels) && ($this->logLevel & $level)) {
			$time = microtime(true) - $this->logStartTime;
			$this->messages[] = array("message" => rtrim($message), "level" => $level, "time" => $time);
			foreach ($this->streams as $s) {
				$s->onLog($message, $level, $time);
			}
			
			return TRUE;
		}
		
		throw new KInvalidArgumentException("$level is not a valid Log level.");
	}
	
	public function attachStream($stream) {
		if ($stream instanceof LogStream) {
			$stream->onAttach($this->messages);
			$this->streams[] = $stream;
			return TRUE;
		}
		
		throw new KInvalidArgumentException(sprintf("Expected LogStream, found %s", is_object($stream) ? get_class($stream) : gettype($stream)));
	}
}