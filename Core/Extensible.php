<?php
/**
 * This class allows other classes to be attached and dynamically add new 
 * functionality to an existing class without modification.
 * 
 * @author Tony Chau
 */
class Extensible {
	private $extensions = array();
	
	public function __call($name, $arguments) {
		foreach ($this->extensions as $ext) {
			foreach ($ext->hasMethods() as $m) {
				if ($m == $name) {
					return call_user_func_array(array($ext, $m), $arguments);
				}
			}
		}
		
		throw new KNotImplementedException("The method called does not exist.");
	}
	
	public function attachExtension($ext) {
		if (!($ext instanceof Extension)) {
			throw new KInvalidArgumentException('Given Extension is invalid!');
		}
		
		$this->extensions[] = $ext;
	}
}