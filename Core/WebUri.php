<?php
/**
 * @file
 * Contains web objects which are useful in various situations.
 */
 
/**
 * WebUri Class
 *
 * This class contains a collection of loosely related methods which ease 
 * accessing URL information such as the root path of this installation, 
 * the current URL, etc.
 */
class WebUri {
	protected $kernel;
	
	public function __construct($kernel) {
		if (!($kernel instanceof Kernel)) {
			throw new KInvalidArgumentException('Invalid Kernel was passed.');
		}
		
		$this->kernel = $kernel;
	}
	
	/**
	 * Splits up the stem of the URI into parts.
	 *
	 * This method is usually used when splitting up the URL stem into parts
	 * which can then be used to determine the controller, method, and various
	 * information which can be passed along in the /controller/method/var/var.
	 *
	 * @return
	 *	 Returns an array containing the parts.
	 */
	public function getParts() {
	 	$stem = $this->getPath(FALSE);
		
		// Split by / and pass to whoever needs it.
		$stem_parts = explode("/", $stem);
		
		// Trim Empty Elements off the end
		for ($i = (count($stem_parts) - 1); $i >= 0; $i--) {
			if (strlen(trim($stem_parts[$i])) != 0) {
				break;
			}
		}
		
		array_splice($stem_parts, $i + 1);
		return $stem_parts;
	}
	
	/**
	 * Gets a certain part of the URI.
	 *
	 * This method can be used to get a certain part of the URI Path. This starts
	 * at 0, so if the path was /foo/bar/view/1, getPart(1) would give you bar.
	 *
	 * @param $index
	 *	 The index which represents the section that you want returned.
	 *
	 * @return
	 *	 Returns a string indicating the section, otherwise NULL.
	 */
	public function getPart($index) {
		$parts = $this->getParts();
		if (isset($parts[$index])) {
			return $parts[$index];
		}
		
		return NULL;
	}
	
	/**
	 * Gets the URL root which this Kakera App is installed under.
	 *
	 * This attempts to determine the URL which this installation is located 
	 * at. It will then cache this inside a configuration variable. If this
	 * configuration variable is already set (default: app/urlroot) then it
	 * will use that value instead.
	 *
	 * This will not work under some configurations (aliases, reverse proxies are
	 * known to cause problems related to port detection and path). If this is 
	 * the case, please set the configuration variable app/urlroot to the path
	 * of your website.
	 *
	 * @param $append
	 *	 A string/url to append onto the URL Root.
	 *
	 * @return
	 *	 A string containing the URL.
	 */
	public function getRoot($append = "") {
		if ($this->kernel->config()->get('system/urlroot') !== NULL) {
			return $this->kernel->config()->get('system/urlroot') . $append;
		}
		
		// Get the Hostname
		$name = $_SERVER['SERVER_NAME'];
		$port = '';
		
		// Determine the port via. protocol
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
			$proto = 'https';
			if ($_SERVER['SERVER_PORT'] != 443) {
				$port = sprintf(':%d', $_SERVER['SERVER_PORT']);
			}
		}
		else {
			$proto = 'http';
			if ($_SERVER['SERVER_PORT'] != 80) {
				$port = sprintf(':%d', $_SERVER['SERVER_PORT']);
			}
		}
		
		// Find the Dir which Kakera is running under.
		$dir = str_replace(basename($_SERVER['SCRIPT_FILENAME']), "", $_SERVER['SCRIPT_NAME']);
		
		// Cache Result
		$this->kernel->config()->set('system/urlroot', sprintf("%s://%s%s%s", $proto, $name, $port, $dir));
		
		// Return the result.
		return $this->kernel->config()->get('system/urlroot') . $append;
	}
	
	/**
	 * Gets the current URL of this page.
	 *
	 * Attempts to determine the current page's URL. If the URL Root is 
	 * incorrect, this also will be incorrect.
	 *
	 * In some cases, if your server configuration is using aliases, it may cause
	 * problems with detecting the path.
	 *
	 * @return
	 *	 Returns a string containing the current URL.
	 */
	public function getCurrentUrl() {
		// Find the Dir which Kakera is running under.
		$dir = str_replace(basename($_SERVER['SCRIPT_FILENAME']), "", $_SERVER['SCRIPT_NAME']);
		
		// Remove the Dir from the the web_urlroot and append the REQUEST_URI 
		// which contains the Dir. 
		return substr_replace($this->getRoot(), "", strrpos($this->getRoot(), $dir)) . $_SERVER['REQUEST_URI'];
	}
	
	/**
	 * Gets the path section of the URL
	 *
	 * Gets the path of the URL. This is the part after the URL Root of the
	 * current URL. This part is usually used for SEO/Path purposes.
	 *
	 * @param $include_query_string
	 *   If set to TRUE, this will also include the Query String in the returned
	 *   string.
	 * @return
	 *	 Returns a string containing the stem.
	 */
	public function getPath($include_query_string = TRUE) {
		$stem = str_replace($this->getRoot(), "", $this->getCurrentUrl());
		if ($stem == $this->getCurrentUrl()) {
			$stem = str_replace(substr($this->getRoot(), 0, strlen($this->getRoot()) - 1), "", $this->getCurrentUrl());
		}
		
		if (!$include_query_string) {
			// Remove the Query String
			if (strpos($stem, "?") !== FALSE) {
				$stem = substr($stem, 0, strpos($stem, "?"));
			}
		}
		
		return trim($stem);
	}
	
	/**
	 * Organize URL Parts
	 *
	 * Gets an input associative array, and places URL Parts into that array
	 * with the specified data type. At the moment the valid types are: bool,
	 * int, and string with the default being a string. 
	 *
	 * No filtering is performed on the parts and applications should still
	 * validate the data before using it.
	 *
	 * @param $array
	 *	 Associative array containing keys to assign the parts to.
	 * @return array
	 *	 Returns an array containing only the keys which existed in the array.
	 */
	public function getNameParts($array) {
		$keys = array_keys($array);
		$output = array();
		
		$url_parts = $this->getParts();
		
		// Loop through the associative array given.
		for ($i = 0; $i < count($keys); $i++) {
			// Check that the target URL part exists.
			if (isset($url_parts[$i])) {
				// Add to the output array with the data type.
				switch($array[$keys[$i]]) {
					case 'bool':
						$output[$keys[$i]] = (bool)($url_parts[$i]);
						break;
					case 'int':
						$output[$keys[$i]] = $url_parts[$i] * 1;
						break;
					case 'string':
					default:
						$output[$keys[$i]] = $url_parts[$i];
						break;
				}
			}
		}
		
		return $output;
	}
	
	/**
	 * Returns the full URL.
	 *
	 * This attempts to give back the full URL used to access this page. This is
	 * using auto-detection, and may give back an inaccurate address. 
	 *
	 * This is called automatically when this object is used as a string.
	 *
	 * @return
	 *	 Returns the full URL used to access this page.
	 */
	public function __toString() {
		return $this->getCurrentUrl();
	}
	
	/**
	 * Parses a HTTP parameter string and returns it as an array.
	 */
	public function parseParameters($string) {
		if (!isset($string) || !is_string($string) || strlen($string) == 0) return array();
		
		$pairs = explode('&', $string);
		
		$parsed_parameters = array();
		foreach ($pairs as $pair) {
			$split = explode('=', $pair, 2);
			$parameter = urldecode($split[0]);
			$value = isset($split[1]) ? urldecode($split[1]) : '';
			
			if (isset($parsed_parameters[$parameter])) {
				// We have already recieved parameter(s) with this name, so add to the list
				// of parameters with this name
				if (is_scalar($parsed_parameters[$parameter])) {
					// This is the first duplicate, so transform scalar (string) into an array
					// so we can add the duplicates
					$parsed_parameters[$parameter] = array($parsed_parameters[$parameter]);
				}
				
				$parsed_parameters[$parameter][] = $value;
			} else {
				$parsed_parameters[$parameter] = $value;
			}
		}
		
		return $parsed_parameters;
	}
}