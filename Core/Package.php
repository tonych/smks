<?php
/**
 * Package Class - Package Object
 *
 * This represents a single package including meta information.
 */
class Package {
	protected $dependencies = array();
	protected $info = array();
	protected $loaded = false;
	protected $kernel;
	
	protected $name;
	protected $version;
	protected $author;
	protected $path;
	protected $shortName;
	
	public function __construct($kernel, $info, $name, $path) {
		$this->kernel = $kernel;
		$this->info = $info;
		$this->path = $path;
		$this->shortName = $name;
		$this->name = $this->getInfo('name');
		$this->author = $this->getInfo('author');
		$this->version = $this->getInfo('version');
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function getShortName() {
		return $this->shortName;
	}
	
	public function getAuthor() {
		return $this->author;
	}

	public function getVersion() {
		return $this->version;
	}
	
	public function getPath() {
		return $this->path;
	}
	
	public function getInfo($field = NULL) {
		if ($field === NULL) {
			return $this->info;
		}
		elseif (isset($this->info[$field])) {
			return $this->info[$field];
		}
		else {
			return NULL;
		}
	}
	
	public function isLoaded() {
		return $this->loaded;
	}
	
	public function getDependencies() {
		return $this->getInfo('dependencies');
	}
	
	public function attachDependency($depend) {
		// only add if inside dependencies.
		if (    in_array($depend->getShortName(), $this->info['dependencies'])
			&& !in_array($depend, $this->dependencies)
		) {
			$this->dependencies[] = $depend;
		}
	}
	
	public function loadAutoload($class) {
		if (isset($this->info['autoload'][$class])) {
			include_once $this->path . $this->info['autoload'][$class];
		}
	}
	
	public function load() {
		if ($this->loaded) { 
			return true;
		}
		
		// load dependencies
		$dep_count = 0;
		foreach ($this->dependencies as $d) {
			if (!$d->load()) {
				return false;
			}
			else {
				$dep_count++;
			}
		}
		
		// check all are loaded.
		if ($dep_count != count($this->getDependencies())) {
			return false;
		}
		
		// include module's files.
		foreach ($this->info['files'] as $f) {
			if (file_exists($this->path . $f)) {
				include_once $this->path . $f;
			}
		}
		
		// mark as loaded.
		$this->loaded = true;
		return true;
	}
	
	public function __toString() {
		return "Package {$this->shortName}({$this->name})";
	}
}