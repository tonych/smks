<?php
/**
 * @file
 * Contains web objects which are useful in various situations.
 */
 
/**
 * WebRequest Class - Web Request Information
 *
 * This class contains information about the user's request. This includes 
 * information about the user, and information about what they are asking to
 * handle.
 */
class WebRequest {
	protected $uri;
	
	protected $clientIp;
	protected $clientAgent;
	
	protected $acceptTypes = array();
	protected $acceptEncoding = array();
	protected $acceptLang = array();
	
	protected $cookie;
	protected $post;
	protected $get;
	
	protected $referer;
	
	protected $requestTime;
	
	public function __construct($kernel) {
		$this->uri = new WebUri($kernel);
		
		$strip_q = function ($value) {
				$mime = explode(';q=', $value);
				return trim($mime[0]);
			};
		
		$this->clientIp = $_SERVER['REMOTE_ADDR'];
		$this->clientAgent = $_SERVER['HTTP_USER_AGENT'];
		if (isset($_SERVER['HTTP_ACCEPT'])) {
			$this->acceptTypes = array_map(
				$strip_q,
				explode(',', $_SERVER['HTTP_ACCEPT'])
				);
		}
		if (isset($_SERVER['HTTP_ACCEPT_ENCODING'])) {
			$this->acceptEncoding = array_map(
				$strip_q,
				explode(',', $_SERVER['HTTP_ACCEPT_ENCODING'])
				);
		}
		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			$this->acceptLang = array_map(
				$strip_q,
				explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE'])
				);
		}
		
		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->referer = $_SERVER['HTTP_REFERER'];
		}
		
		$this->cookie = $_COOKIE;
		$this->post = $_POST;
		$this->get = $_GET;
		
		if (isset($_SERVER['REQUEST_TIME'])) {
			$this->requestTime = floor($_SERVER['REQUEST_TIME']);
		}
		else {
			$this->requestTime = time();
		}
	}
	
	private function getVariable($variable, $name = NULL) {
		if ($name === NULL) {
			return $variable;
		}
		
		if (isset($variable[$name])) {
			return $variable[$name];
		}
		
		return NULL;
	}
	
	public function getCookie($name = NULL) {
		return $this->getVariable($this->cookie, $name);
	}
		
	public function getPost($name = NULL) {
		return $this->getVariable($this->post, $name);
	}
		
	public function getGet($name = NULL) {
		return $this->getVariable($this->get, $name);
	}
	
	public function getClientIp() {
		return $this->clientIp;
	}
	
	public function getClientAgent() {
		return $this->clientAgent;
	}
	
	/**
	 * Returns WebUri Object.
	 * @return WebUri
	 */
	public function getUri() {
		return $this->uri;
	}
	
	public function getReferer() {
		return $this->referer;
	}
	
	public function getRequestTime() {
		return $this->requestTime;
	}
	
	public function getAcceptTypes() {
		return $this->acceptTypes;
	}
	
	public function getAcceptLanguages() {
		return $this->acceptLang;
	}
	
	public function getAcceptEncoding() {
		return $this->acceptEncoding;
	}
}