<?php
    $webroot = $this->kernel->request()->getUri()->getRoot();
	$this->loadJS($webroot . "Content/js/tabbedcontainer.js");

    ob_start();
?>
            <p>
                This tool will clear your cookies at <?php echo $_SERVER['HTTP_HOST']; ?>
                except for the ones required to stay logged into this website.
            </p>
            <p>
                <form action="<?php echo $webroot; ?>submissions/clearCookies" method="post">
                    <input type="hidden" name="subId" value="<?php echo $submissionId; ?>">
                    <input type="submit" value="Clear Cookies/Sessions">
                </form>
            </p>

<?php
$contents = ob_get_contents();
ob_end_clean();

$this->loadView("ContentBox", [
    "heading"   => "Session Tool",
    "content"   => $contents,
    "fullWidth" => TRUE,
    ]);