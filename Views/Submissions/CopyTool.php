<?php
$webroot = $this->kernel->request()->getUri()->getRoot();

$contents = <<<EOF
<form action="{$webroot}submissions/copy" method="post">
	<label for="source">Select student to copy from:</label>
	<input type="hidden" name="dest" value="%d">
	<select name="source" id="source">
		%s
	</select>
	<input type="submit" value="Copy Marks">
</form>
EOF;

ob_start();
foreach ($markedSubmissions as $s):
	printf('<option value="%d">%s (%.1f Marks)</option>',
		$s['submissionId'] * 1,
		$s['name'],
		$s['marks'] * 1
		);
endforeach;
$options = ob_get_contents();
ob_end_clean();

$contents = sprintf($contents, $submissionId, $options);
$this->loadView("ContentBox", [
	"heading" => "Copy Marks Tool",
	"content" => $contents,
	"fullWidth" => TRUE,
	]);