<?php
$webroot = $this->kernel->request()->getUri()->getRoot();
	$this->loadJS($webroot . "Content/js/fileSearch.js");

// Capture the Template
ob_start();
?>

<table class="subFilesTable">
	<thead>
		<tr>
			<th>Files</th>
			<th>Operations</th>
			<th>Errors</th>
		</tr>
	</thead>
	<tbody>
		%s
	</tbody>
</table>

<?php
// Finish
$content = ob_get_contents();
ob_end_clean();

// Prepare Data
// Base Path: $basePath;
$dfs = [];
$tree = [];
foreach ($basePath as $p) $dfs[] = [$p, 0];	// [path, depth level]

ob_start();

function create_tree($path, $depthLevel) {
	$dirs = [];
	$files = [];
	$tree = [[$path, $depthLevel]];
	
	if (is_dir($path)) {
		$fileList = scandir($path);
	
		foreach ($fileList as $f) {
			if ($f == "." || $f == "..") {
				continue;
			}
			
			$fullPath = realpath(sprintf("%s/%s", $path, $f));
			if (is_dir($fullPath)) {
				$dirs[] = [$fullPath, $depthLevel + 1];
			}
			else {
				$files[] = [$fullPath, $depthLevel + 1];
			}
		}
	}
	
	foreach ($dirs as $d)
		$tree = array_merge($tree, create_tree($d[0], $d[1]));
	
	foreach ($files as $f) 
		$tree[] = $f;
		
	return $tree;
}

foreach ($dfs as $d) {
	$tree = array_merge($tree, create_tree($d[0], $d[1]));
}

foreach ($tree as $f) {
	$this->loadView("Submissions/FileTableRow", [
		"file" => [
			"path"  => $f[0],
			"depth" => $f[1]
			],
		"errors" => [],
		]);
}

$rows = ob_get_contents();
ob_end_clean();

// Join Content
$content = sprintf($content, 
	$rows
	);

// Final Output
$this->loadView("ContentBox", [
	'heading' => 'File List <form action="#" method="post" id="searchFile"><input type="text" name="searchTerm" placeholder="Enter Term to Search" /><input type="submit" value="Search"><input type="reset"></form>',
	'content' => $content,
	'fullWidth' => TRUE,
	]);
