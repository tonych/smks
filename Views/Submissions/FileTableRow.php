<?php
$webroot = $this->kernel->request()->getUri()->getRoot();

// Capture the Template
ob_start();
?>

<tr class="hasFile">
	<td class="pathInfo" style="padding-left: %dpx"><img src="%s" alt="%s"><span class="path">%s</span><p class="searchResults hidden"></span></td>
	<td class="fileOps">%s</td>
	<td class="errorInfo"><a href="#">%s</a></td>
</tr>

<?php
// Finish
$content = ob_get_contents();
ob_end_clean();

// Prepare Data
if (is_dir($file['path'])) {
	$filetype = "dir";
}
else {
	$fileext = strtolower(pathinfo($file['path'], PATHINFO_EXTENSION));
	switch($fileext) {
		case 'png':
		case 'jpg':
		case 'gif':
		case 'svg':
		case 'mp3':
		case 'mp4':
			$filetype = "img";
			break;
		case 'js':
		case 'css':
			$filetype = "scripts";
			break;
		default:
			$filetype = "files";
			break;
	}
}

$typeUrl = $webroot . "Content/images/icons/" . $filetype . ".png";
$pathUrl = str_replace("/home/jail/home/", "54.206.43.109/~", $file['path']);
if (strpos($pathUrl, "secure_html") !== FALSE) { // very hacky way of doing it
	$pathUrl = "https://" . $pathUrl;
}
else {
	$pathUrl = "http://" . $pathUrl;
}
$pathUrl = str_replace(['secure_html/', 'public_html/'], ['',''], $pathUrl);
$errors = "";
if ($filetype == "scripts" && $fileext == "css") {
	$errors = sprintf('<a href="%s%s">Validate</a>',
		$this->kernel->config()->get("smks/css_validator"),
		urlencode($pathUrl));
}
else if ($filetype == "files" && $fileext == "html") {
	$errors = sprintf('<a href="%s%s">Validate</a>',
		$this->kernel->config()->get("smks/html_validator"),
		urlencode($pathUrl));
}

$ops = sprintf('<a href="%s" class="url">View</a>', $pathUrl);
if ($filetype == "files" || $filetype == "scripts") {
	$ops .= ' | <a href="#" class="viewSource">Source</a>';
}

// Output final result
echo sprintf($content,
	$file['depth'] * 20,
	$typeUrl,
	$filetype,
	str_replace("/home/jail/home/", "", $file['path']),
	$ops,
	$errors);
