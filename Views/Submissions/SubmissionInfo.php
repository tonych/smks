<?php
$webroot = $this->kernel->request()->getUri()->getRoot();
$this->loadJS($webroot . "Content/js/markingCriteria.js");

// Capture the Template
ob_start();
?>
<script type="text/javascript">
	var submissionId = <?php echo $submissionInfo['submissionId']; ?>;
</script>

<table class="subInfoTable">
	<tbody>
		<tr>
			<th>Assignment Info</th>
			<td>%s</td>
		</tr>
		<tr>
			<th>Student ID</th>
			<td><a href="%s">%s</a></td>
		</tr>
		<tr>
			<th>Student Name</th>
			<td>%s</td>
		</tr>
		<tr>
			<th>Postgraduate</th>
			<td>%s</td>
		</tr>
		<tr>
			<th>Last Commited by</th>
			<td>%s</td>
		</tr>
		<tr>
			<th>Marks</th>
			<td>
				<a href="#" id="showMarkingCriteria">%s / %.1f (Scaled)</a> 
				<p class="criteriaWaiting">
					This page will refresh when Marking Criteria Window is closed. 
				</p>
				<p class="criteriaWaiting">
					<img src="<?php echo $webroot . 'Content/images/ajax-loader.gif'; ?>" alt="ajax loading">
				</p>
			</td>
		</tr>
		<tr>
			<th>Feedback</th>
			<td>%s</td>
		</tr>
	</tbody>
</table>

<?php
// Finish
$content = ob_get_contents();
ob_end_clean();

// Prepare Data
$assignmentInfo = Filter::filterText($submissionInfo['assignmentName']);
$studentLink = $webroot . "students/view/" . urlencode($submissionInfo['studentId']);
$studentName = Filter::filterText(sprintf(
	"%s, %s", 
	$submissionInfo['lastName'], 
	$submissionInfo['firstName']
	));
$currMarks = (is_null($submissionInfo['marks']) || $submissionInfo['marks'] < 0) ?
	"-" :
	sprintf("%.1f", $submissionInfo['marks']);
$totalMarks = $submissionInfo['maxMarks'];
$feedback = $submissionInfo['feedback'];
if (strlen($feedback) <= 0) {
	$feedback = "<em>No Feedback Yet</em>";
}
else {
	// format the feedback
	$feedback = "<p>". Filter::filterText($feedback). "</p>";
	$feedback = str_replace("\n", "<br />", $feedback);
	$feedback = str_replace("<br /><br />", "</p><p>", $feedback);
}

$content = sprintf($content,
	$assignmentInfo,
	$studentLink,
	Filter::filterText($submissionInfo['studentId']),
	$studentName,
	$submissionInfo['postgraduate'] ? "YES" : "NO", 
	Filter::filterText($this->usersManager->getUser($submissionInfo['assignedTo'])->getRealName()),
	$currMarks,
	$totalMarks,
	$feedback);

// Final Output
$this->loadView("ContentBox", [
	'heading' => sprintf("Submission #%d", $submissionInfo['submissionId']),
	'content' => $content,
	'fullWidth' => TRUE,
	]);
