<?php
$webroot = $this->kernel->request()->getUri()->getRoot();
$this->loadJS("{$webroot}Content/js/submissionSourceCode.js");

ob_start();
?>
<pre id="sourceCode" class="prettyprint linenums">
Click on a file above to view the source code.
</pre>
<?php
$content = ob_get_contents();
ob_end_clean();

$this->loadView("ContentBox", [
	'heading' => 'Source Code Viewer <span id="openInBrowser"></span>',
	'content' => $content,
	'fullWidth' => TRUE,
	]);

