<?php
$webroot = $this->kernel->request()->getUri()->getRoot();
$this->loadJS($webroot . "Content/js/criteria2.js");

// Our Page Title
$this->setTitle(sprintf("Marking for #%d: %s, %s", $submissionInfo['submissionId'], $submissionInfo['lastName'], $submissionInfo['firstName']));

// Provide Submission Id for our JavaScript
printf('<script type="text/javascript">var submissionId = %d;</script>',
	$submissionInfo['submissionId']);

// Header
$this->loadView("Submissions/Criteria/2/Header", [
	"submission" => $submissionInfo,
	]);

// Core Tasks
$this->loadView("Submissions/Criteria/2/Table");
