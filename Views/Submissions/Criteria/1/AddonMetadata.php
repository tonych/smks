		<table class="criteria">
			<tr>
				<th rowspan="2">
					Criteria
				</th>
				<th colspan="4">
					Performance Level
				</th>
				<th rowspan="2">
					Score
				</th>
			</tr>
			<tr>
				<th>
					7
				</th>
				<th>
					6/5
				</th>
				<th>
					4/3
				</th>
				<th>
					2/1/0
				</th>
			</tr>
			<tr class="criteria">
				<th rowspan="3" class="criteriaTitle">
					Add on #2: Metadata
				</th>
				<td>
					HTML header has complete metadata for OpenGraph as well as at least fallbacks to Twitter Cards and Google+
				</td>
				<td>
					HTML header has complete metadata for one social media service
				</td>
				<td>
					HTML header has partial metadata for one social media service
				</td>
				<td>
					HTML header does not include social media metadata
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td colspan="3">
					Individual item page includes Place microdata for latitude and longitude
				</td>
				<td>
					Individual item page does not include Place microdata
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					Individual item page includes metadata for both reviews and ratings 
				</td>
				<td colspan="2">
					Individual item page includes metadata for reviews OR ratings 	
				</td>
				<td>
					Individual item page does not include review or rating microdata
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria_subtotal" data-section='{"name":"Add on #2: Metadata","subtotal":10}'>
				<th>
					Comments on Metadata
				</th>
				<td colspan="4">
					<textarea class="comments" cols="80" rows="3"></textarea>
				</td>
				<td>
					<span class="subtotalVal">-</span> / <span class="subtotalTotal">10</span>
				</td>
			</tr>
		</table>
