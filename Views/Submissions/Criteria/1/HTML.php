		<table class="criteria">
			<tr>
				<th rowspan="2">
					Criteria
				</th>
				<th colspan="4">
					Performance Level
				</th>
				<th rowspan="2">
					Score
				</th>
			</tr>
			<tr>
				<th>
					7
				</th>
				<th>
					6/5
				</th>
				<th>
					4/3
				</th>
				<th>
					2/1/0
				</th>
			</tr>
			<tr class="criteria">
				<th rowspan="4" class="criteriaTitle">
					HTML
				</th>
				<td>
					HTML4 or HTML5 validates with no errors
				</td>
				<td>
					HTML4 or HTML5 validates with a single error
				</td>
				<td>
					HTML4 or HTML5 validates with 2-3 errors
				</td>
				<td>
					HTML4 or HTML5 validates with many errors
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					HTML has appropriate titles and basic meta data
				</td>
				<td>
					HTML has appropriate titles and or meta data
				</td>
				<td>
					No meta data in HTML
				</td>
				<td>
					No meta data in HTML
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					HTML code perfectly laid out and easy to read with consistent formatting
				</td>
				<td>
					HTML code is reasonably well laid out but has a few formatting inconsistencies
				</td>
				<td>
					HTML code is readable but has a number of formatting inconsistencies
				</td>
				<td>
					HTML is hard to read with no formatting or a large number of formatting inconsistencies
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					HTML code contains a large number of good explanatory comments
				</td>
				<td>
					HTML code contains some useful comments
				</td>
				<td>
					HTML code contains a few useful comments
				</td>
				<td>
					HTML code contains no comments or comments are not useful
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria_subtotal" data-section='{"name":"HTML","subtotal":15}'>
				<th>
					Comments on HTML
				</th>
				<td colspan="4">
					<textarea class="comments" cols="80" rows="3"></textarea>
				</td>
				<td>
					<span class="subtotalVal">-</span> / <span class="subtotalTotal">15</span>
				</td>
			</tr>
		</table>
