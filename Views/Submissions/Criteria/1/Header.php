<?php
ob_start();
?>
<div class="criteriaHeader">
	<div class="col-left float-left">
		<table>
			<tr>
				<th>Student Number</th>
				<td>%s</td>
			</tr>
			<tr>
				<th>Student Name</th>
				<td>%s</td>
			</tr>
			<tr>
				<th>Postgraduate?</th>
				<td>%s</td>
			</tr>
			<tr class="screenOnly">
				<th>Last Saved by</th>
				<td>%s</td>
			</tr>
			<tr class="screenOnly">
				<th>Commited</th>
				<td>%s</td>
			</tr>
			<tr>
				<th>Marks</th>
				<td>
					<span id="currScaledScore">%s</span>/
					<span id="totalScaledScore">%d</span>
					(<span id="percent">%s</span>%% of Marks Possible)
				</td>
			</tr>
			<tr class="screenOnly">
				<th>Tools</th>
				<td>
					<input type="button" id="saveDraft" value="Save as Draft">
					<input type="button" id="setMarked" value="Set as Marked">
					<input type="button" id="closeWindow" value="Close">
				</td>
			</tr>
		</table>
	</div>
	<div class="col-right float-right screenOnly">
		<p>
			Remember to check how many add-ons each student is required to do and 
			if they are working in a group or not. This will <b>not</b> calculate
			that for you.
		</p>
		<p class="accordian_tools">
			<a href="#" class="expandAll">Expand All</a> | 
			<a href="#" class="collapseAll">Collapse All</a>
		</p>
		<p id="criteriaStatus">
			Status goes here!
		</p>
	</div>
	<div class="float-clear"></div>
</div>
<?php
$contents = ob_get_contents();
ob_end_clean();

$contents = sprintf($contents,
	Filter::filterText($submission['studentId']),
	Filter::filterText(sprintf("%s, %s", $submission['lastName'], $submission['firstName'])),
	($submission['postgraduate']) ? "<b>Yes</b>" : "No",
	Filter::filterText($this->usersManager->getUser($submission['assignedTo'])->getRealName()),
	(!is_null($submission['marks'])) ? "Yes" : "No",
	(!is_null($submission['marks'])) ? $submission['marks'] : "?",
	$submission['maxMarks'],
	(!is_null($submission['marks'])) ? $submission['marks'] / $submission['maxMarks'] : "?"
	);

echo $contents;
