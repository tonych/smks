		<table class="criteria">
			<tr>
				<th rowspan="2">
					Criteria
				</th>
				<th colspan="4">
					Performance Level
				</th>
				<th rowspan="2">
					Score
				</th>
			</tr>
			<tr>
				<th>
					7
				</th>
				<th>
					6/5
				</th>
				<th>
					4/3
				</th>
				<th>
					2/1/0
				</th>
			</tr>
			<tr class="criteria">
				<th rowspan="2" class="criteriaTitle">
					Add on #3: Mobile Design
				</th>
				<td>
					Uses CSS @media queries to dynamically adjust layout and displayed elements to screen widths as low as 320px without flaws
				</td>
				<td colspan="2">
					Uses CSS @media queries to dynamically adjust layout and displayed elements to smaller screen widths with few flaws	
				</td>
				<td>
					No responsive design using CSS @media queries
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					HTML header includes metadata for displaying site well when added to iOS and Android home screens
				</td>
				<td colspan="2">
					HTML header includes some metadata for displaying site when added to iOS OR Android home screens	
				</td>
				<td>
					HTML header includes no metadata related to home screens
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria_subtotal" data-section='{"name":"Add on #3: Mobile Design","subtotal":10}'>
				<th>
					Comments on Mobile Design
				</th>
				<td colspan="4">
					<textarea class="comments" cols="80" rows="3"></textarea>
				</td>
				<td>
					<span class="subtotalVal">-</span> / <span class="subtotalTotal">10</span>
				</td>
			</tr>
		</table>
