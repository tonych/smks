		<table class="criteria">
			<tr>
				<th rowspan="2">
					Criteria
				</th>
				<th colspan="4">
					Performance Level
				</th>
				<th rowspan="2">
					Score
				</th>
			</tr>
			<tr>
				<th>
					7
				</th>
				<th>
					6/5
				</th>
				<th>
					4/3
				</th>
				<th>
					2/1/0
				</th>
			</tr>
			<tr class="criteria">
				<th rowspan="11" class="criteriaTitle">
					Web site core programming tasks design and functionality
				</th>
				<td colspan="2">
					Pages contain a header, footer and menu
				</td>
				<td>
					Pages are missing one of header, footer, menu
				</td>
				<td>
					Pages are missing more than one of header, footer, menu
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td colspan="2">
					Flexible or centred page design
				</td>
				<td colspan="2">
					Fixed page layout
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					Search form implemented and allows users to search according to three or more criteria; includes Geolocation lookup
				</td>
				<td>
					Search form implemented but missing one or two search criteria or Goelocation
				</td>
				<td>
					Search form partially implemented or missing most search criteria
				</td>
				<td>
					Search form not implemented
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					Sample search results page implemented showing relevant data in a tabular format with hyperlinks to sample results page
				</td>
				<td>
					Sample search results page implemented in a tabular format but missing hyperlinks
				</td>
				<td>
					Sample search results page partially implemented or not using tables
				</td>
				<td>
					Sample search results page partially implemented or not using tables
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					Sample individual item page implemented with details for item as well as sample reviews and ratings in a well-organized format
				</td>
				<td>
					Sample individual item page implemented but missing some details for item OR not well-organized format
				</td>
				<td>
					Sample individual item page partial implemented but missing most details, not well organized
				</td>
				<td>
					Sample individual item page not implemented
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					Registration page contains submit button and appropriate form elements of four or more different types
				</td>
				<td>
					Registration page contains submit button and form elements of four or more different types but some may not be appropriate
				</td>
				<td>
					Registration page contains submit button and appropriate form elements of at least three different types
				</td>
				<td>
					Registration page is missing submit button, or contains some form elements, but only one or two types
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					Data of different formats, at least numeric, alphabetic, email and date formats, is validated correctly
				</td>
				<td>
					Data of different formats, at least numeric, alphabetic, email and date formats, is mostly validated correctly
				</td>
				<td>
					Data validation of some different formats OR data validation is present but the validation does not execute correctly
				</td>
				<td>
					Different data formats are not validated - only validates presence of input OR No data validation
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					Validation error messages are shown in-form and give a detailed explanation of the error and how it could be corrected
				</td>
				<td>
					Some validation error messages are shown in-form and give some indication of the error and how it may be corrected
				</td>
				<td>
					Data validation errors are reported singly, or only alert boxes are used
				</td>
				<td>
					No validation error reporting or single error to say input incorrect
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td colspan="2">
					Design is unified with a consistent look and feel
				</td>
				<td>
					Look and feel is mostly consistent
				</td>
				<td>
					Look and feel is not consistent across pages
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td colspan="2">
					Web pages follow good design principles and guidelines and have a very professional appearance
				</td>
				<td>
					Web pages follow some design principles and guidelines and some effort has been made to make them look professional
				</td>
				<td>
					Web pages do not seem to follow most design principles and guidelines
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td colspan="2">
					Web pages follow Web Content Accessibility Guidelines, including text alternatives for images, sufficient contrast display, and semantic markup
				</td>
				<td>
					Web pages follow some WCAG but missing one of guidelines listed at left
				</td>
				<td>
					Web pages ignore several WCAG guideliness and miss 2 or more guidelines at left
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria_subtotal" data-section='{"name":"Core Programming Tasks and Functionality","subtotal":25}'>
				<th>
					Comments on Core Tasks
				</th>
				<td colspan="4">
					<textarea class="comments" cols="80" rows="3"></textarea>
				</td>
				<td>
					<span class="subtotalVal">-</span> / <span class="subtotalTotal">25</span>
				</td>
			</tr>
		</table>
