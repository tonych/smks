		<table class="criteria">
			<tr>
				<th rowspan="2">
					Criteria
				</th>
				<th colspan="4">
					Performance Level
				</th>
				<th rowspan="2">
					Score
				</th>
			</tr>
			<tr>
				<th>
					7
				</th>
				<th>
					6/5
				</th>
				<th>
					4/3
				</th>
				<th>
					2/1/0
				</th>
			</tr>
			<tr class="criteria">
				<th rowspan="5" class="criteriaTitle">
					CSS
				</th>
				<td>
					CSS validates with no errors
				</td>
				<td>
					CSS validates with a single error
				</td>
				<td>
					CSS validates with 2-3 errors
				</td>
				<td>
					CSS validates with many errors
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					CSS in external style sheets
				</td>
				<td>
					CSS in external style sheets
				</td>
				<td>
					CSS used inline
				</td>
				<td>
					No CSS
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					Sophisticated use of div or HTML5 elements and CSS to achieve advanced styling effects
				</td>
				<td>
					Use of div elements and CSS to achieve reasonably advanced styling effects
				</td>
				<td>
					Absolute positioning used for some div elements to achieve page template
				</td>
				<td>
					Minimal CSS and/or uses tables or does not use div elements to layout page
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					CSS code perfectly laid out and easy to read with consistent formatting
				</td>
				<td>
					CSS code is readable but has a few formatting inconsistencies
				</td>
				<td>
					CSS code is readable but has a number of formatting inconsistencies
				</td>
				<td>
					CSS is hard to read with no formatting or a large number of formatting inconsistencies
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					CSS code contains a large number of good explanatory comments
				</td>
				<td>
					CSS code contains some useful comments
				</td>
				<td>
					CSS code contains a few useful comments
				</td>
				<td>
					CSS code contains no comments or comments are not useful
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria_subtotal" data-section='{"name":"CSS","subtotal":15}'>
				<th>
					Comments on CSS
				</th>
				<td colspan="4">
					<textarea class="comments" cols="80" rows="3"></textarea>
				</td>
				<td>
					<span class="subtotalVal">-</span> / <span class="subtotalTotal">15</span>
				</td>
			</tr>
		</table>
