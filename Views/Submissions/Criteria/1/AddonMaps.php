		<table class="criteria">
			<tr>
				<th rowspan="2">
					Criteria
				</th>
				<th colspan="4">
					Performance Level
				</th>
				<th rowspan="2">
					Score
				</th>
			</tr>
			<tr>
				<th>
					7
				</th>
				<th>
					6/5
				</th>
				<th>
					4/3
				</th>
				<th>
					2/1/0
				</th>
			</tr>
			<tr class="criteria">
				<th rowspan="2" class="criteriaTitle">
					Add on #1: Maps
				</th>
				<td>
					Search results page includes map showing markers for all search results; each marker includes information about the result and a link to individual item page
				</td>
				<td colspan="2">
					Search results page includes map showing markers for all search results but markers are not fully functional/informational	
				</td>
				<td>
					Search results page does not include map
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td colspan="2">
					Individual item page includes map showing the item	
				</td>
				<td>
					Invididual item page includes map but is not centred about the item
				</td>
				<td>
					Individual item page does not include map
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria_subtotal" data-section='{"name":"Add on #1: Maps","subtotal":10}'>
				<th>
					Comments on Maps
				</th>
				<td colspan="4">
					<textarea class="comments" cols="80" rows="3"></textarea>
				</td>
				<td>
					<span class="subtotalVal">-</span> / <span class="subtotalTotal">10</span>
				</td>
			</tr>
		</table>
