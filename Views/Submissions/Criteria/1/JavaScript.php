		<table class="criteria">
			<tr>
				<th rowspan="2">
					Criteria
				</th>
				<th colspan="4">
					Performance Level
				</th>
				<th rowspan="2">
					Score
				</th>
			</tr>
			<tr>
				<th>
					7
				</th>
				<th>
					6/5
				</th>
				<th>
					4/3
				</th>
				<th>
					2/1/0
				</th>
			</tr>
			<tr class="criteria">
				<th rowspan="4" class="criteriaTitle">
					JavaScript
				</th>
				<td>
					JavaScript in separate file
				</td>
				<td>
					JavaScript in separate file
				</td>
				<td>
					JavaScript in HTML file
				</td>
				<td>
					No JavaScript
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					JavaScript code has been structured with a number of functions with little or no repeated code
				</td>
				<td>
					JavaScript code has a number of functions but may have some repeated code
				</td>
				<td>
					JavaScript code has a few functions OR the functions contain a large amount of repeated code
				</td>
				<td>
					JavaScript code has no structure (just one long function)
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					JavaScript code perfectly laid out and easy to read with consistent formatting
				</td>
				<td>
					JavaScript code is readable but has a few formatting inconsistencies
				</td>
				<td>
					JavaScript code is readable but has a number of formatting inconsistencies
				</td>
				<td>
					JavaScript is has no formatting or a large number of formatting inconsistencies
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria">
				<td>
					JavaScript code contains a large number of good explanatory comments
				</td>
				<td>
					JavaScript code contains some useful comments
				</td>
				<td>
					JavaScript code contains a few useful comments
				</td>
				<td>
					JavaScript code contains no comments or comments are not useful
				</td>
				<th class="scoreKeeping"></th>
			</tr>
			<tr class="criteria_subtotal" data-section='{"name":"JavaScript","subtotal":15}'>
				<th>
					Comments on JavaScript
				</th>
				<td colspan="4">
					<textarea class="comments" cols="80" rows="3"></textarea>
				</td>
				<td>
					<span class="subtotalVal">-</span> / <span class="subtotalTotal">15</span>
				</td>
			</tr>
		</table>
