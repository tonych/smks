<?php
$webroot = $this->kernel->request()->getUri()->getRoot();
$this->loadJS($webroot . "Content/js/criteria1.js");

// Our Page Title
$this->setTitle(sprintf("Marking for #%d", $submissionInfo['submissionId']));

// Provide Submission Id for our JavaScript
printf('<script type="text/javascript">var submissionId = %d;</script>',
	$submissionInfo['submissionId']);

// Header
$this->loadView("Submissions/Criteria/1/Header", [
	"submission" => $submissionInfo,
	]);

// Core Tasks
$this->loadView("Accordian", [
	"heading" => "Core Tasks and Functionality",
	"content" => $this->loadView("Submissions/Criteria/1/CoreTasks", [], TRUE),
	]);

// HTML
$this->loadView("Accordian", [
	"heading" => "HTML",
	"content" => $this->loadView("Submissions/Criteria/1/HTML", [], TRUE),
	]);

// CSS
$this->loadView("Accordian", [
	"heading" => "CSS",
	"content" => $this->loadView("Submissions/Criteria/1/CSS", [], TRUE),
	]);

// JavaScript
$this->loadView("Accordian", [
	"heading" => "JavaScript",
	"content" => $this->loadView("Submissions/Criteria/1/JavaScript", [], TRUE),
	]);

// Add-on #1: Maps
$this->loadView("Submissions/Criteria/OptionalAccordian", [
	"heading" => "Add on #1: Maps",
	"content" => $this->loadView("Submissions/Criteria/1/AddonMaps", [], TRUE),
	"optional" => TRUE,
	]);

// Add-on #2: Metadata
$this->loadView("Submissions/Criteria/OptionalAccordian", [
	"heading" => "Add on #2: Metadata",
	"content" => $this->loadView("Submissions/Criteria/1/AddonMetadata", [], TRUE),
	"optional" => TRUE,
	]);

// Add-on #3: Mobile Design
$this->loadView("Submissions/Criteria/OptionalAccordian", [
	"heading" => "Add on #3: Mobile Design",
	"content" => $this->loadView("Submissions/Criteria/1/AddonMobile", [], TRUE),
	"optional" => TRUE,
	]);
