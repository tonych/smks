<?php
if (isset($optional) && $optional) {
	$heading = $heading . ' (<span class="optional">Include in Marks Calculation <input type="checkbox" name="optional"></span>)';
}

$this->loadView("Accordian", [
	"heading" => $heading,
	"content" => $content,
	]);