		<tr class="section">
			<th class="rowHeader">
                <p class="sectionTitle">Coding Practices</p>
                <p>[ <span class="currMarks">?</span>/5 Marks ]</p>
            </th>
			<td>
				<p>
					<input type="checkbox" id="60">
					<label for="60">Some good coding practices demonstrated.</label>
				</p>
			</td>
			<td>
				<p>
					<input type="checkbox" id="61">
					<label for="61">Meaningful names used for many files, functions and variables.</label>
				</p>
				<p>
					<input type="checkbox" id="62">
					<label for="62">Some PHP functions created.</label>
				</p>
				<p>
					<input type="checkbox" id="63">
					<label for="63">Folders used to organize files.</label>
				</p>
				<p>
					<input type="checkbox" id="64">
					<label for="64">Fairly consistent indentation.</label>
				</p>
				<p>
					<input type="checkbox" id="65">
					<label for="65">Little code or HTML replication.</label>
				</p>
			</td>
			<td>
				<p>
					<input type="checkbox" id="66">
					<label for="66">Meaningful names used for every file, function and variable.</label>
				</p>
				<p>
					<input type="checkbox" id="67">
					<label for="67">Perfect indentation.</label>
				</p>
				<p>
					<input type="checkbox" id="68">
					<label for="68">
						PHP functions created for each distinct computational task 
						and grouped with related functions in seperate include files.
					</label>
				</p>
				<p>
					<input type="checkbox" id="69">
					<label for="69">No replicated code or HTML (Don't repeat yourself).</label>
				</p>
			</td>
		</tr>
        <tr class="feedback">
            <th class="feedback">Feedback</th>
            <td class="feedback" colspan="3">
                <textarea class="sectionfeedback" rows="4" cols="25"></textarea>
            </td>
        </tr>