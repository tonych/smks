		<tr class="section">
			<th class="rowHeader">
                <p class="sectionTitle">HTML Form Processing</p>
                <p>[ <span class="currMarks">?</span>/5 Marks ]</p>
            </th>
			<td>
				<p>
					<input type="checkbox" id="20">
					<label for="20">HTML forms submit to PHP pages.</label>
				</p>
			</td>
			<td>
				<p>
					<input type="checkbox" id="21">
					<label for="21">Receive input using $_POST and $_GET</label>
				</p>
				<p>
					<input type="checkbox" id="22">
					<label for="22">Query string is used to pass and receive information between pages (e.g. ShowItem.php?id=42)</label>
				</p>
			</td>
			<td>
				<p>
					<input type="checkbox" id="23">
					<label for="23">PHP pages post back to themselves and have clear, easy to follow data processing logic.</label>
				</p>
				<p>
					<input type="checkbox" id="24">
					<label for="24">Does not assume that all expected $_POST or $_GET input fields will always be provided.</label>
				</p>
				<p>
					<input type="checkbox" id="25">
					<label for="25">File upload and parsing demonstrated correctly</label>
				</p>
			</td>
		</tr>
        <tr class="feedback">
            <th class="feedback">Feedback</th>
            <td class="feedback" colspan="3">
                <textarea class="sectionfeedback" rows="4" cols="25"></textarea>
            </td>
        </tr>