		<tr class="section">
			<th class="rowHeader">
                <p class="sectionTitle">User interface design and consistent look and feel</p>
                <p>[ <span class="currMarks">?</span>/5 Marks ]</p>
            </th>
			<td>
				<p>
					<input type="checkbox" id="1">
					<label for="1">Reasonably consistent look and feel across the web site.</label>
				</p>
			</td>
			<td>
				<p>
					<input type="checkbox" id="2">
					<label for="2">Uses CSS and DIVS to achieve most of the page's layout.</label>
				</p>
				<p>
					<input type="checkbox" id="3">
					<label for="3">Some include files used to reduce repeated content between pages.</label>
				</p>
				<p>
					<input type="checkbox" id="4">
					<label for="4">Some attempt at applying user interface design principles.</label>
				</p>
			</td>
			<td>
				<p>
					<input type="checkbox" id="5">
					<label for="5">Clear simple easy to read PHP files with respect to page layout</label>
				</p>
				<p>
					<input type="checkbox" id="6">
					<label for="6">Completely consistent look and feel</label>
				</p>
				<p>
					<input type="checkbox" id="7">
					<label for="7">Professional appearance</label>
				</p>
				<p>
					<input type="checkbox" id="8">
					<label for="8">Demonstrates many principles of user interface design</label>
				</p>
				<p>
					<input type="checkbox" id="9">
					<label for="9">Add-on tasks from assignment 1 carried through to the server</label>
				</p>
			</td>
		</tr>
        <tr class="feedback">
            <th class="feedback">Feedback</th>
            <td class="feedback" colspan="3">
                <textarea class="sectionfeedback" rows="4" cols="25"></textarea>
            </td>
        </tr>