<div class="table-container">
	<table class="criteria">
		<thead>
			<tr>
				<th rowspan="2">Criteria</th>
				<th colspan="3">Standards</th>
			</tr>
			<tr>
				<th>Poor (1 mark)</th>
				<th>Satisfactory (3 marks)</th>
				<th>Outstanding (5 marks)</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$this->loadView("Submissions/Criteria/2/UserInterface");
				$this->loadView("Submissions/Criteria/2/HTMLForm");
				$this->loadView("Submissions/Criteria/2/Server");
				$this->loadView("Submissions/Criteria/2/DB");
				$this->loadView("Submissions/Criteria/2/Security");
				$this->loadView("Submissions/Criteria/2/CodingPractices");
				//$this->loadView("Submissions/Criteria/2/Feedback");
			?>
		</tbody>
	</table>
</div>