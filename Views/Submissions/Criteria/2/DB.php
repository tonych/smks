		<tr class="section">
			<th class="rowHeader">
                <p class="sectionTitle">Database access</p>
                <p>[ <span class="currMarks">?</span>/5 Marks ]</p>
            </th>
			<td>
				<p>
					<input type="checkbox" id="40">
					<label for="40">Some attempt at implementing database access.</label>
				</p>
			</td>
			<td>
				<p>
					<input type="checkbox" id="41">
					<label for="41">Successfully retrieves information from the database - roughly as specified.</label>
				</p>
				<p>
					<input type="checkbox" id="42">
					<label for="42">Successfully inserts information into the database - roughly as specified.</label>
				</p>
			</td>
			<td>
				<p>
					<input type="checkbox" id="43">
					<label for="43">All database operations performed exactly as specified.</label>
				</p>
				<p>
					<input type="checkbox" id="44">
					<label for="44">All database access is done via PDO and via prepared statements whenever untrusted data is involved.</label>
				</p>
				<p>
					<input type="checkbox" id="45">
					<label for="45">Database errors are dealt with appropriately.</label>
				</p>
			</td>
		</tr>
        <tr class="feedback">
            <th class="feedback">Feedback</th>
            <td class="feedback" colspan="3">
                <textarea class="sectionfeedback" rows="4" cols="25"></textarea>
            </td>
        </tr>