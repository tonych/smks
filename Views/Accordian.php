<?php
	$webroot = $this->kernel->request()->getUri()->getRoot();
	$this->loadJS($webroot . "Content/js/accordian.js");
?>
<div class="accordian">
	<div class="accordian-header">
		<h1><?php echo $heading; ?></h1>
		<div class="accordian-state">
			+
		</div>
		<div class="float-clear"></div>
	</div>
	<div class="accordian-content">
		<?php echo $content; ?>
	</div>
</div>
