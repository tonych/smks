<?php
ob_start();

if (count($students) <= 0):
	?>
	<p>There are no students.</p>
	<?php
else:
	?>
	<table id="studentList">
		<tr>
			<th>Student ID</th>
			<th>Name</th>
			<th colspan="<?php echo max(1, count($assessment)); ?>" >Assessment</th>
		</tr>
	<?php
	foreach ($students as $s):
		printf('<tr><td class="studentId"><a href="students/view/%s">%s</a></td><td class="studentName">%s, %s</td>',
			Filter::filterText($s['studentId']),
			Filter::filterText($s['studentId']),
			Filter::filterText($s['lastName']),
			Filter::filterText($s['firstName'])
			);
		foreach ($assessment as $a):
			echo "<td>";
			if (!isset($submissions[$s['studentId']][$a['assignmentId']])):
				$this->loadView('ProgressBar', [
					'backgroundColor' => '#ddd',
					'fillPercent'     => 100,
					'fillColor'       => '#ddd',
					'text'            => 'N',
					]);
			else: 
				if ($submissions[$s['studentId']][$a['assignmentId']]['marks'] === NULL):
					$this->loadView('ProgressBar', [
						'backgroundColor' => '#FFB3B3',
						'fillPercent'     => 100,
						'fillColor'       => '#FFB3B3',
						'text'            => 'S',
						]);
				else:
					$this->loadView('ProgressBar', [
						'backgroundColor' => '#B8DE83',
						'fillPercent'     => 100,
						'fillColor'       => '#B8DE83',
						'text'            => 'M',
						]);
				endif;
			endif;
			echo "</td>";
		endforeach;
		echo "</tr>";
	endforeach;
	?>
	</table>
	<?php
endif;

$content = ob_get_contents();
ob_end_clean();

$heading = "Student Listing";
$this->loadView("ContentBox", [
	'heading' => $heading,
	'content' => $content,
	]);