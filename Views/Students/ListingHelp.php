<?php
$heading = "About this Screen";
$content = <<<EOF
<p>On the left you'll see a listing for all of the 
students in the unit. In the assessment column, there is an
area with coloured boxes labeled, <b>N</b>, <b>S</b> or <b>M</b>.</p>

<p>
They represent No Submission, Submitted and Marked. 
Ideally all boxes should be coloured green (marked) by the
end of semester.
</p>
EOF;

$this->loadView("ContentBox", [
	'heading' => $heading,
	'content' => $content,
	]);