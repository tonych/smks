<?php
$webroot = $this->kernel->request()->getUri()->getRoot();
$this->loadJS("{$webroot}Content/js/studentListingSearch.js");

$content = <<<EOF
<div id="studentWidget">
	<label for="studentSearch">Student Search</label>
	<input type="text" name="value" id="studentListingSearch" placeholder="Search for ID/Name..." />
</div>
EOF;

$this->loadView("ContentBox", [
	'heading' => 'Student Search',
	'content' => $content,
	]);
