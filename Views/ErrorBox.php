<div class="errorBox">
	<?php if (isset($heading)): ?>
		<h1><?php echo $heading; ?></h1>
	<?php endif; ?>
	<p><?php echo $content; ?></p>
</div>