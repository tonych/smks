<?php
    $webroot = $this->kernel->request()->getUri()->getRoot();
	$this->loadJS($webroot . "Content/js/tabbedcontainer.js");

    ob_start();
?>
<div class="tabbedContainer">
    <div class="tabs">
        <h1 data-target=".clear" class="active">Session Tool</h1>
        <h1 data-target=".copy">Copy Tool</h1>
    </div>
    <div class="content">
        <div class=".clear">
            <p>
                This tool will clear your cookies at <?php echo $_SERVER['HTTP_HOST']; ?>
                except for the ones required to stay logged into this website.
            </p>
            <p>
                <form action="<?php echo $webroot; ?>/submissions/clearCookies" method="post">
                    <input type="hidden" name="subId" value="<?php echo $submissionId; ?>">
                    <input type="submit" value="Clear Cookies/Sessions">
                </form>
            </p>
        </div>
        <div class=".copy">
            <p>
                Copy tool goes here.
            </p>
        </div>
    </div>
</div>

<?php
$contents = ob_get_contents();
ob_end_clean();

$this->loadView("ContentBox", [
    "content" => $contents,
    "fullWidth" => TRUE,
    ]);