<?php
if (!isset($backgroundColor))
	$backgroundColor = "white";

printf('<div class="multiProgBarContainer" style="background-color: %s">', $backgroundColor);

foreach ($sections as $s) {
	printf('<div class="multiProgBarFill" style="width: %d%%; background-color: %s">%s</div>',
		$s['fillPercent'],
		$s['fillColor'],
		Filter::filterText($s['text'])
		);
}

echo '<div class="float-clear"></div></div>';