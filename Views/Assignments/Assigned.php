<?php
$webroot = $this->kernel->request()->getUri()->getRoot();

function outputSubmissions($submissions) {
	
}

// Start the output
ob_start();

if (count($submissions) > 0):
	for ($postgrad = 0; $postgrad <= 1; $postgrad++):
		if (!isset($submissions[$postgrad]))
			continue;

		$data = $submissions[$postgrad];
		if ($postgrad == 0):
			echo "<h2>Undergraduate</h2>";
		else:
			echo "<h2>Postgraduate</h2>";
		endif;
		
			echo <<<EOF
		<table>
			<thead>
				<tr>
					<th>Student</th>
					<th>Marks</th>
				</tr>
			</thead>
			<tbody>
EOF;

		foreach ($data as $o):
			$url = sprintf("%ssubmissions/view/%d",
				$webroot, 
				$o['submissionId']);

			$link = sprintf('<a href="%s">%s, %s</a>', 
				$url,
				Filter::filterText($students[$o['studentId']]['lastName']),
				Filter::filterText($students[$o['studentId']]['firstName'])
				);

			$assignedUser = $this->usersManager->getUser($o['assignedTo']);

			ob_start();
			if (is_null($o['marks']) || $o['marks'] < 0) {
				$this->loadView('ProgressBar', [
					'backgroundColor' => '#FFB3B3',
					'fillPercent'     => 100,
					'fillColor'       => '#FFB3B3',
					'text'            => '-',
					]);
			}
			else {
				$this->loadView('ProgressBar', [
					'backgroundColor' => '#B8DE83',
					'fillPercent'     => 100,
					'fillColor'       => '#B8DE83',
					'text'            => Filter::filterText($o['marks']),
					]);
			}
			$marks = ob_get_contents();
			ob_end_clean();

			printf('<tr><td>%s</td><td>%s</td></tr>',
				$link,
				$marks
				);


		endforeach;

		echo '</tbody></table>';
	endfor;
else:
	printf("<p>%s</p>", 
		"There are no submissions assigned to you to mark. How about picking a 
		few from the other side to mark?"
		);
endif;

// Finish
$content = ob_get_contents();
ob_end_clean();

$this->loadView("ContentBox", [
	'heading' => "Assigned To Me",
	'content' => $content,
	]);
