<?php
$webroot = $this->kernel->request()->getUri()->getRoot();
$this->loadJS("{$webroot}Content/AssignmentDetails.js");

$date = date('d/m/y H:i:s', $assignment['dueDate']);

$pendingPercent = floor(($submissionData['total'] - $submissionData['marked']) / $studentCount * 100);
$markedPercent = floor($submissionData['marked'] / $studentCount * 100);
$nonsubPercent = 100 - $pendingPercent - $markedPercent;

$bar_data = [];

if ($markedPercent >= 1)
	$bar_data[] = [
		'fillPercent' => $markedPercent,
		'fillColor'   => '#B8DE83',
		'text'        => sprintf('%d Marked', $submissionData['marked'])
	];

if ($pendingPercent >= 1)
	$bar_data[] =[
		'fillPercent' => $pendingPercent,
		'fillColor'   => '#FFB3B3',
		'text'        => sprintf('%d Pending', $submissionData['total'] - $submissionData['marked'])
	];

if ($nonsubPercent >= 1)
	$bar_data[] =[
		'fillPercent' => $nonsubPercent,
		'fillColor'   => 'white',
		'text'        => sprintf('%d Non-submissions', $studentCount - $submissionData['total'])
	];

ob_start();

echo "<p>Due date: <span class=''>$date</span></p>";
//echo "<h2>Submission Status</h2>";

$this->loadView('MultiProgressBar', [
	'sections' => $bar_data
	]);

$content = ob_get_contents();
ob_end_clean();

$heading = Filter::filterText($assignment['name']);
$link = sprintf('<a href="%sassignments/view/%d">%s</a>', $webroot, $assignment['assignmentId'], $heading);

$this->loadView("ContentBox", [
	'heading' => $link,
	'content' => $content,
	]);