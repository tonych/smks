<?php
$webroot = $this->kernel->request()->getUri()->getRoot();

// Start the output
ob_start();

if (count($submissions) > 0):
	for ($postgrad = 0; $postgrad <= 1; $postgrad++):
		if (!isset($submissions[$postgrad]))
			continue;

		$data = $submissions[$postgrad];
		if ($postgrad == 0):
			echo "<h2>Undergraduate</h2>";
		else:
			echo "<h2>Postgraduate</h2>";
		endif;

		echo <<<EOF
		<table>
			<thead>
				<tr>
					<th>Student</th>
					<th>Assigned</th>
					<th>Marks</th>
					<th>Addons</th>
				</tr>
			</thead>
			<tbody>
EOF;
        $optCountQuery = $this->dbc->getPreparedQuery(
            "SELECT * FROM submission_marking WHERE submissionId = ? ORDER BY dateMarked DESC LIMIT 1"
        );
		foreach ($data as $o):
			$url = sprintf("%ssubmissions/view/%d",
				$webroot, 
				$o['submissionId']);

			$link = sprintf('<a href="%s">%s, %s</a>', 
				$url,
				Filter::filterText($students[$o['studentId']]['lastName']),
				Filter::filterText($students[$o['studentId']]['firstName'])
				);

			$assignedUser = $this->usersManager->getUser($o['assignedTo']);

			ob_start();
			if (is_null($o['marks']) || $o['marks'] < 0) {
				$this->loadView('ProgressBar', [
					'backgroundColor' => '#FFB3B3',
					'fillPercent'     => 100,
					'fillColor'       => '#FFB3B3',
					'text'            => '-',
					]);
			}
			else {
				$this->loadView('ProgressBar', [
					'backgroundColor' => '#B8DE83',
					'fillPercent'     => 100,
					'fillColor'       => '#B8DE83',
					'text'            => Filter::filterText(sprintf("%.2f", $o['marks'])),
					]);
			}
			$marks = ob_get_contents();
			ob_end_clean();

			ob_start();
			if (is_null($o['marks']) || $o['marks'] < 0) {
				$this->loadView('ProgressBar', [
					'backgroundColor' => '#ddd',
					'fillPercent'     => 100,
					'fillColor'       => '#ddd',
					'text'            => 'N/A',
					]);
			}
			else {
				// count number of "Was not included."
                if (!$this->kernel->config()->get("smks/countAddonViaFeedback", FALSE)) {
                    $optionalEnabled = -99;
                    $optCountQuery->bindValue(1, $o['submissionId'], PDO::PARAM_INT);
                    $optCountQuery->execute();
                    $marking = $optCountQuery->fetch(PDO::FETCH_ASSOC);
                    $optCountQuery->closeCursor();
                    
                    if ($marking) {
                        $optionalEnabled = 0;
                        $decoded = json_decode($marking['serializedData']);
                        // decode and count
                        foreach ($decoded->data as $k => $v) {
                            if ($v->optional && $v->optionalValue) {
                                $optionalEnabled++;
                            }
                        }
                    }
                    else {
                        $optionalEnabled = $this->dbc->errorMessage(); 
                    }

                    $count = $optionalEnabled;
                }
                else {
                    $count = preg_match_all("/This criteria was not included in marks calculation/",
                                            $o['feedback']);
                    $count = 3 - $count;
                }
				
				if (($postgrad == 0 && $count >= 1) || ($postgrad > 0 && $count >= 2)) {
					$this->loadView('ProgressBar', [
						'backgroundColor' => '#B8DE83',
						'fillPercent'     => 100,
						'fillColor'       => '#B8DE83',
						'text'            => $count,
						]);
				}
				else {
					$this->loadView('ProgressBar', [
						'backgroundColor' => '#FFB3B3',
						'fillPercent'     => 100,
						'fillColor'       => '#FFB3B3',
						'text'            => $count,
						]);
				}
			}
			$addons = ob_get_contents();
			ob_end_clean();

			printf('<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
				$link,
				Filter::filterText($assignedUser->getRealName()),
				$marks,
				$addons
				);
		endforeach;

		echo '</tbody></table>';
	endfor;
else:
	printf("<p>%s</p>", 
		"There are no submissions or students in this assessment item!"
		);
endif;

// Finish
$content = ob_get_contents();
ob_end_clean();

$this->loadView("ContentBox", [
	'heading' => "Other Submissions",
	'content' => $content,
	]);
