<?php
$webroot = $this->kernel->request()->getUri()->getRoot();

$content = <<<EOL
<p>
	<form action="{$webroot}assignments/create" method="post" id="createAssessment">
		<label for="assname">Assessment Name</label>
		<input type="text" name="assignmentName" id="assname" placeholder="Assessment Name" />
		<label for="maxmarks">Max Marks</label>
		<input type="text" name="maxMarks" id="maxmarks" placeholder="Max Marks" />
		<label for="duedate">Due Date</label>
		<input type="text" name="dueDate" id="duedate" placeholder="Due Date" />
		<input type="submit" value="Create" />
	</form>
</p>
<p>
	Alternatively, you may <a href="{$webroot}import/">import</a> a Grade Centre
	export from blackboard. This will also import students as well as assessment.
</p>
EOL;

$this->loadView("ContentBox", [
	'heading' => 'Create Assessment Item',
	'content' => $content
	]);
