<?php	
if (!isset($fillColor))
	$fillColor = "#eee";
if (!isset($backgroundColor))
	$backgroundColor = "white";

printf('
	<div class="progBarContainer" style="background-color: %s">
		<div class="progBarFill" style="width: %d%%; background-color: %s">%s</div>
	</div>
	',
	$backgroundColor,
	$fillPercent,
	$fillColor,
	Filter::filterText($text)
	);