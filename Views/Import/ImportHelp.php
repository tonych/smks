<?php
$webroot = $this->kernel->request()->getUri()->getRoot();

$content = <<<EOL
	<ol id="importHelp">
		<li>
			Select Grade Centre from the Unit Management Menu. 
			<img src="{$webroot}Content/images/import_1.png" alt="step1" />
		</li>
		<li>
			From the Work Offline Menu, select Download
			<img src="{$webroot}Content/images/import_2.png" alt="step2" />
		</li>
		<li>
			Make sure you select Tab-delimited for the export type and Full Grade Centre. 
			This will allow you to import assessment as well as User Information. 
			<img src="{$webroot}Content/images/import_3.png" alt="step3" />
		</li>
	</ol>
EOL;

$this->loadView("ContentBox", [
	'heading' => 'How to Import from Blackboard',
	'content' => $content
	]);
