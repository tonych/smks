<?php
$webroot = $this->kernel->request()->getUri()->getRoot();

$content = <<<EOL
<p>
	<form enctype="multipart/form-data" action="{$webroot}import/process" method="post" id="importForm">
		<p><label for="file">Upload File</label></p>
		<input type="file" name="importFile" id="file" placeholder="Assessment Name" />
		<p>Type</p>
		<input type="radio" name="importType" id="typeXls" value="xls" checked="checked" /><label for="typeXls">XLS</label>
		<p>Student Type</p>
		<input type="radio" name="studentType" id="studentUndergrad" value="undergrad" checked="checked" />
		<label for="studentUndergrad">Undergraduate</label>
		<input type="radio" name="studentType" id="studentPostgrad" value="postgrad" />
		<label for="studentUndergrad">Postgraduate</label>
		<p><input type="submit" value="Import" /></p>
	</form>
</p>
EOL;

$this->loadView("ContentBox", [
	'heading' => 'Import from Blackboard',
	'content' => $content
	]);
