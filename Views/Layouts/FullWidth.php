<?php
// Pre-Content Variables
$webroot = $this->kernel->request()->getUri()->getRoot();
$urlroot = $this->kernel->request()->getUri()->getRoot('Content/');

$mainNavCurrent = function($class) {
	if ($class == $this->controller) {
		echo 'class="current"';
	}
};

// Get notifications if any.
if ($this->isLoggedIn()) {
	$nQuery = $this->dbc->select(
		"notifications", 
		[
			'userId' => $this->getCurrentUser()->getUserId()
		],
		'date'
		);
	$notifications = $nQuery->fetchAll();
	$dQuery = $this->dbc->delete('notifications', ['userId' => $this->getCurrentUser()->getUserId()]);
}
else {
	$notifications = [];
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>SMKS <?php if (isset($title) && strlen($title) > 0): printf("&middot; %s", Filter::filterText($title)); endif; ?></title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
		<style type="text/css">
			<?php echo $styles; ?>
		</style>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src='<?php echo $urlroot; ?>js/google-code-prettify/prettify.js'></script>
		<script src='<?php echo $urlroot; ?>js/google-code-prettify/lang-css.js'></script>
		<link href="<?php echo $urlroot; ?>js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			var webRoot = "<?php echo addslashes($webroot); ?>";
		</script>
		<script type="text/javascript">
			<?php if (count($notifications) > 0): ?>
			$(function() {
				setTimeout(function() {
					$('div#notifications').slideUp();
				}, 3000);
			});
			<?php else: ?>
			$(function() {
				$('div#notifications').hide();
			});
			<?php endif; ?>
		</script>
		<?php echo $js; ?>
	</head>
	<body>
		<?php if ($this->isLoggedIn()): ?>
		<div id="notifications">
			<p>You have notifications: </p>
			<ul id="notificationList">
				<?php
				if (count($notifications) > 0) {
					foreach ($notifications as $n) {
						printf('<li>%s</li>', $n['text']);
					}
				}
				?>
			</ul>
		</div>
		<?php endif; ?>
		<div id="header">
			<div id="headerContents">
				<div id="logo">
					<h1><a href="<?php echo $webroot; ?>">SMKS</a></h1>
				</div>
				<?php if ($this->isLoggedIn()): ?>
				<div id="main-nav">
					<ul>
						<li>
							<a href="<?php echo $webroot; ?>" <?php $mainNavCurrent("HomeController"); ?>>
								Dashboard
							</a>
						</li>
						<li>
							<a href="<?php echo $webroot; ?>assignments" <?php $mainNavCurrent("AssignmentsController"); ?>>
								Assessment
							</a>
						</li>
						<li>
							<a href="<?php echo $webroot; ?>students" <?php $mainNavCurrent("StudentsController"); ?>>
								Students
							</a>
						</li>
						<li>
							<a href="<?php echo $webroot; ?>tutors" <?php $mainNavCurrent("TutorsController"); ?>>
								Tutors
							</a>
						</li>
					</ul>
				</div>
				<?php endif; ?>
				<div id="sign-inout">
					<?php if ($this->isLoggedIn()):	?>
					<ul>
						<li><?php echo Filter::filterText($this->getCurrentUser()->getRealName()); ?>
							<ul id="profileMenu">
								<?php if ($this->getCurrentUser()->hasState('specialUserAdmin')): ?>
								<li><a href="<?php echo $webroot; ?>system/settings">System Settings</a></li>
								<?php endif; ?>
								<li><a href="<?php echo $webroot; ?>user/logout">Log Off</a></li>
							</ul>
						</li>
					</ul>
					<?php else: ?>
						<ul>
							<li>
								<form action="<?php echo $webroot; ?>user/check" method="post">
								<a href="<?php echo $webroot; ?>user/">Login</a>
								<ul id="profileMenu">
									<li>
										<label for="navUname">Email</label>
										<input type="text" name="uname" id="navUname" placeholder="Email" />
									</li>
									<li>
										<label for="navPword">Password</label>
										<input type="password" name="pword" id="navPword" placeholder="Password" />
									</li>
									<li><input type="submit" value="Login" /></li>
								</ul>
								</form>
							</li>
						</ul>
					<?php endif; ?>
				</div>
				<div class="float-clear"></div>
			</div>
		</div>
		<div id="content">
			<?php echo $content; ?>
		</div>
	</body>
</html>
