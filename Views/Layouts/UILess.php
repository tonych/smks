<?php
// Pre-Content Variables
$webroot = $this->kernel->request()->getUri()->getRoot();
$urlroot = $this->kernel->request()->getUri()->getRoot('Content/');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>SMKS <?php if (isset($title) && strlen($title) > 0): printf("&middot; %s", Filter::filterText($title)); endif; ?></title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
		<style type="text/css">
			<?php echo $styles; ?>
		</style>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src='<?php echo $urlroot; ?>js/google-code-prettify/prettify.js'></script>
		<script src='<?php echo $urlroot; ?>js/google-code-prettify/lang-css.js'></script>
		<link href="<?php echo $urlroot; ?>js/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			var webRoot = "<?php echo addslashes($webroot); ?>";
		</script>
		<?php echo $js; ?>
	</head>
	<body>
		<?php echo $content; ?>
	</body>
</html>
