<?php
$class = "content-box";
if (isset($fullWidth) && $fullWidth) {
	$class .= " full-width";
}
?>
			<div class='<?php echo $class; ?>'>
				<?php
				if (isset($heading)) {
					printf("<h1>%s</h1>", $heading);
				}
				
				echo $content;
				?>
			</div>