<?php
$webroot = $this->kernel->request()->getUri()->getRoot();
?>
<h1 id="loginHeading">Login</h1>
<form action="<?php echo $webroot; ?>user/check" method="post" id="loginForm">
	<label for="uname">Username</label>
	<input type="text" id="uname" name="uname" value="<?php echo Filter::filterText($uname); ?>" placeholder="Email" />
	<label for="pword">Password</label>
	<input type="password" name="pword" id="pword" placeholder="Password" />
	<input type="submit" value="Login" />
</form>
