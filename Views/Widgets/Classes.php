<?php
/******
 * Students Widget
 */
$webroot = $this->kernel->request()->getUri()->getRoot();

$content = '<div id="classWidget"><table class="classTable">';
$classStatus = NULL;
for($i = 0; $i < count($classes); $i++) {
	if ($classes[$i]['postgraduate'] != $classStatus) {
		$classStatus = $classes[$i]['postgraduate'];
		$content .= '<tr><th colspan="2">';

		if ($classes[$i]['postgraduate'])
			$content .= 'Postgraduate Classes';
		else
			$content .= 'Undergraduate Classes';

		$content .= '</th></tr>';
	}
	$content .= sprintf(
		'<tr><td><a href="%sstudents/class/%d/%s">%s</a></td><td class="progBarCol">%s</td></tr>', 
		$webroot, 
		$classes[$i]['postgraduate'] ? 1 : 0,
		urlencode($classes[$i]['workshop']),
		strlen($classes[$i]['workshop']) <= 0 ? "Unassigned Workshop" : Filter::filterText($classes[$i]['workshop']),
		$this->loadView("ProgressBar", [
			'fillColor' => '#B8DE83',
			'fillPercent' => floor($classes[$i]['numStudents'] / 30 * 100),
			'text' => $classes[$i]['numStudents'],
			], TRUE)
		);
}

$content .= '</table></div>';

$this->loadView("ContentBox", [
	'heading' => 'Classes',
	'content' => $content
	]);
