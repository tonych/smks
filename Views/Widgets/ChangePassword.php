<?php
$webroot = $this->kernel->request()->getUri()->getRoot();

ob_start();

?>
<p>
	You can change your password below. There's no confirm your password box so 
	make sure you get it right first time around.
</p>
<?php

printf('<form action="%suser/changePw" method="post" class="changePasswordForm">', $webroot);

?>
<p>
	<label for="currPw">Current Password</label>
	<input type="password" name="currpw" id="currPw" placeholder="Current Password" />
</p>
<p>
	<label for="newPw">New Password</label>
	<input type="password" name="newpw" id="newPw" placeholder="New Password" />
</p>
<p><input type="submit" value="Change Password" /></p>
</form>
<?php

$content = ob_get_contents();
ob_end_clean();

$this->loadView("ContentBox", [
	'heading' => 'Change Password',
	'content' => $content
	]);
