<?php
/******
 * Assignments Widget
 */
$webroot = $this->kernel->request()->getUri()->getRoot();
$this->loadJS("{$webroot}Content/js/time.js");
$this->loadJS("{$webroot}Content/widgets/AssignmentWidget.js");

$content = '<div id="assignmentWidget"><table id="assignmentTable">';

for ($i = 0; $i < count($assignments); $i++) {
	$content .= sprintf('<tr><td class="assignmentName"><a href="%sassignments/view/%s">%s</a></td></tr>',
		$webroot,
		Filter::filterText($assignments[$i]['assignmentId']),
		Filter::filterText($assignments[$i]['name'])
		);
}

$content .= '</table></div>';


$this->loadView("ContentBox", [
	'heading' => 'Assessment',
	'content' => $content
	]);
