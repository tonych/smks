<?php
/******
 * Students Widget
 */
$webroot = $this->kernel->request()->getUri()->getRoot();
$this->loadJS("{$webroot}Content/widgets/StudentWidget.js");

$content = <<<EOF
<div id="studentWidget">
	<form action="{$webroot}students/search" method="post">
		<label for="studentSearch">Student Search</label>
		<input type="text" name="value" id="studentSearch" placeholder="Search for ID/Name..." />
		<input type="submit" value="Search" />
	</form>
</div>
EOF;

$this->loadView("ContentBox", [
	'heading' => 'Student Search',
	'content' => $content
	]);
