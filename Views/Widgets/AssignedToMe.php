<?php
$webroot = $this->kernel->request()->getUri()->getRoot();

ob_start();
if (count($assignedToMe) <= 0) {
	echo "No Assignments assigned to you to mark.";
}
else {
	echo '<table class="assignedToMeTable">';
	foreach ($assignedToMe as $assigned) {
		$assName = "Unknown Assignment";
		foreach ($assignments as $a) {
			if ($assigned['assignmentId'] == $a['assignmentId'])
				$assName = $a['name'];
		}

		$markedPercent = floor($assigned['marked'] / $assigned['total'] * 100);
		$unmarkedPercent = min(max(99 - $markedPercent, 0), 100);
		$pending = $assigned['total'] - $assigned['marked'];

		printf(
			'<tr><td class="assignedName"><div><a href="%sassignments/view/%d">%s</a></div></td><td class="progBarArea">%s</td></tr>',
			$webroot,
			$assigned['assignmentId'] * 1,
			Filter::filterText($assName),
			$this->loadView('MultiProgressBar', [
				'sections' => [
						[
						'fillPercent' => $markedPercent,
						'fillColor'   => '#B8DE83',
						'text'        => sprintf("%d Marked", $assigned['marked'])
						],
						[
						'fillPercent' => $unmarkedPercent,
						'fillColor'   => '#FFB3B3',
						'text'        => sprintf("%d Pending", $pending)
						],
					],
				],
				TRUE)
			);
	}
	echo '</table>';
}

$content = ob_get_contents();
ob_end_clean();


$this->loadView("ContentBox", [
	'heading' => 'Assigned To Me',
	'content' => $content
	]);
