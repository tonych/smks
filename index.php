<?php
/**
 * Base Path
 *   This is the base path of the 
 */
define('BASE_PATH', dirname(__FILE__) . '/');

/**
 * Core Path
 *   This is where the files containing the core is located.
 */
define('CORE_PATH', BASE_PATH . 'Core/');

/**
 * Configuration Path
 *   This is where the configuration files go.
 */
define('CONFIG_PATH', BASE_PATH . 'Config/');

/**
 * Controller Path
 *   This is where the page handlers will go.
 */
define('CONTROLLER_PATH', BASE_PATH . 'Controllers/');

/**
 * View Path
 *   This is where the view templates will go.
 */
define('VIEW_PATH', BASE_PATH . 'Views/');

//----------------------------------------------------------------------------
// End Configurable Area
//----------------------------------------------------------------------------
if (!file_exists(CORE_PATH . 'Kernel.php')) {
	die('SNS Kernel could not be found.');
}

// Load Kernel.
require CORE_PATH . 'Extensible.php';
require CORE_PATH . 'Kernel.php';
kernel();

// Load Packages from Package Path set in Configuration File.
$packagePaths = kernel()->config()->get('system/packagePaths', array());
if (!is_array($packagePaths)) {
	$packagePaths = array();
}
foreach ($packagePaths as $path) {
	if (is_dir($path)) {
		kernel()->packages()->addPackagePath($path);
	}
}

unset($packagePaths);
unset($path);

kernel()->callHook('onReady');