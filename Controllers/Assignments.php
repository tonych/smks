<?php
class AssignmentsController extends LoginRequiredController {
	private function showCreateForm($name, $marks, $dueDate) {
		// Create an assignment form.
		$this->loadView("Assignments/CreateForm", [
			]);
	}
	
	private function getSubmissions($id) {
		// get further assignment details
		$query = sprintf("
			SELECT COUNT(sub.assignmentId) as total, COUNT(sub.marks) as marked
			FROM students s
			LEFT JOIN submissions sub
			  ON s.studentId = sub.studentId
			WHERE 
			  sub.assignmentId = %d OR
			  sub.assignmentId IS NULL",
			$id);
		$detailQuery = $this->dbc->getPreparedQuery($query);
		$rs = new KDatabaseResult($detailQuery, $detailQuery->execute());
		$row = $rs->fetchAssoc();
		
		return $row;
	}
	
	private function getStudentCount() {
		$scQuery = $this->dbc->getPreparedQuery(
			"SELECT COUNT(s.studentId) as studentCount FROM students s"
			);
		$rs = new KDatabaseResult($scQuery, $scQuery->execute());
		$scCount = $rs->fetchRow();
		return $scCount[0];
	}

	public function view() {
		// get first ID
		$id = $this->kernel->request()->getUri()->getPart(2);
		
		// not found
		$error = FALSE;
		if ($id != NULL) {
			// try and find it
			$aQuery = $this->dbc->select("assignments", ["assignmentId" => $id]);
			$assignment = $aQuery->fetchAssoc();
			
			// Assignment not found in DB
			if (!$assignment)
				$error = TRUE;
		}
		// ID was either NULL or not an INT.
		else
			$error = TRUE;

		if ($error) {
			$this->setTitle("Assignment not found");
			return $this->loadView('ErrorBox', ['content' => "Assignment not found"]);
		}
		
		// set title
		$this->setTitle("Viewing assignment #". $assignment['assignmentId']);
		
		// get all students
		$students = [];
		$sQuery = $this->dbc->select("students", [], null, 'postgraduate');
		while ($row = $sQuery->fetchAssoc()) {
			$students[$row['studentId']]  = $row;
		}
		
		// get all submissions.
		$assignedSubmissions = [];
		$other = [];
		$subQuery = $this->dbc->select("submissions", 
			['assignmentId' => $assignment['assignmentId']],
			['submissionId', 'studentId', 'assignmentId', 'assignedTo', 'marks', 'feedback'],
			'assignedTo');
		while($r = $subQuery->fetchAssoc()) {
			$postgraduate = ($students[$r['studentId']]['postgraduate'] > 0)? 1 : 0;
			
			if ($r['assignedTo'] == $this->getCurrentUser()->getUserId()) {
				$assignedSubmissions[$postgraduate][$r['studentId']] = $r;
			}
			else {
				$other[$postgraduate][$r['studentId']] = $r;
			}
		}
		
		$submissionData = $this->getSubmissions($assignment['assignmentId']);
		$studentCount = $this->getStudentCount();
		
		// assessment overview.
		$this->loadView("Assignments/AssessmentDetails", [
			'assignment' => $assignment,
			'submissionData' => $submissionData,
			'studentCount'   => $studentCount,
			]);
		
		if ($assignment['assignmentId'] == 1) {
			// assigned
			$this->loadView("Assignments/Assigned1", [
				'submissions' => $assignedSubmissions,
				'students'    => $students,
				]);
			
			// 'others'
			$this->loadView("Assignments/Others1", [
				'submissions' => $other,
				'students'    => $students,
				]);
		}
		else {
			// assigned
			$this->loadView("Assignments/Assigned", [
				'submissions' => $assignedSubmissions,
				'students'    => $students,
				]);
			
			// 'others'
			$this->loadView("Assignments/Others", [
				'submissions' => $other,
				'students'    => $students,
				]);
		}
	}

	public function defaultPage() {
		$this->enableLayout = TRUE;
		$this->setTitle("Assessment");
		
		// Get all assignments
		$aQuery = $this->dbc->select("assignments");
		$assignments = $aQuery->fetchAll();

		// Create Form
		//$this->showCreateForm("","","");
		
		$studentCount = $this->getStudentCount();
		// Show details for each assessment item.
		for ($i = 0; $i < count($assignments); $i++) {
			$submissionData = $this->getSubmissions($assignments[$i]['assignmentId']);

			$this->loadView("Assignments/AssessmentDetails", [
				'assignment' => $assignments[$i],
				'submissionData' => $submissionData,
				'studentCount'   => $studentCount,
				]);
		}
	}
}
