<?php
class ImportController extends LoginRequiredController {
	public function process() {
		// Get file details
		if (!isset($_FILES['importFile']) || $_FILES['importFile']['error'] > 0) {
			$this->loadView("ErrorBox", [
				'content' => 'Either no file was uploaded, or an invalid file was uploaded.',
				]);
			
			return $this->defaultPage();
		}

		// ok, so here we should have at least a file, lets check if it's
		// at least the right format.
		// Need to move to a temp area.
		define('MOVE_LOCATION', BASE_PATH . 'Temp/' . $_FILES['importFile']['name']);
		if (!move_uploaded_file($_FILES['importFile']['tmp_name'], MOVE_LOCATION)) {
			// Ok we couldn't move the file (it wasn't uploaded?)
			// or the destination location isn't a proper path (filename attack?)
			$this->loadView("ErrorBox", [
				'content' => 'Your file was unable to be processed.',
				]);
			
			return $this->defaultPage();
		}

		// Ok, so we can move the file, lets actually read it now and check if 
		// it's a proper file
		$contents = iconv("UTF-16", "UTF-8", file_get_contents(MOVE_LOCATION));
		$lines = array_map("trim", explode("\n", $contents));
		if (count($lines) > 0) {
			// Split the first line
			$removeQuotes = function($str) {
				$str = trim($str);
				$trimBOMchars = chr(0xEF) . chr(0xBB) . chr(0xBF) . chr(0xFF) . chr(0xFE);
				$str = trim($str, $trimBOMchars);
				if (strlen($str) > 3) {
					return trim($str, '"');
				}
				else
					return "";
			};

			$columns = array_map($removeQuotes, explode("\t", $lines[0]));
			if (count($columns) >= 3
				&& in_array("Last Name", $columns)
				&& in_array("First Name", $columns)
				&& in_array("Username", $columns)) {
				// It's all good!
				// Get our col indexes.
				$assessment = [];
				$lname = 0;
				$fname = 1;
				$uname = 2;

				foreach ($columns as $k => $c) {
					$matches = [];
					if ($c == "Last Name")
						$lname = $k;
					else if ($c == "First Name")
						$fname = $k;
					else if ($c == "Username") 
						$uname = $k;
					else if (preg_match("/^([\w\s\-]+)\[[\w\s\-\d\:]+?([\d\.]+)\]\s+\|(\d+)$/u", $c, $matches)) {
						$assessment[] = array($matches[1], $matches[2], $matches[3]); // Name, Points, ID
					}
				}

				// Lets import the accessment (minus the total)
				foreach ($assessment as $a) {
					// Don't add total columns.
					if (stripos($a[0], "Total") === FALSE) {
						$this->dbc->insert("assignments", [
							'assignmentId'   => $a[2],
							'name' => $a[0],
							'maxMarks' => $a[1]
							]);
					}
				}

				// a few more details.
				$postgrad = 0;
				if (isset($_POST['studentType']) && $_POST['studentType'] == "postgrad")
					$postgrad = 1;

				// Now continue reading and import students.
				for ($i = 1; $i < count($lines); $i++) {
					$studentData = array_map($removeQuotes, explode("\t", $lines[$i]));
					
					if (count($studentData) >= 3) {
						$this->dbc->insert('students', [
							'studentId' => $studentData[$uname],
							'firstName' => $studentData[$fname],
							'lastName'  => $studentData[$lname],
							'postgraduate' => $postgrad
							]);
					}
				}

				// All done, lets redirect them back to the dashboard!
				$allDone = TRUE;
			}
			else {
			}
		}
		else {
			echo "This doesn't look like a valid file.";
		}

		// Remember to clean up after yourself!
		unlink(MOVE_LOCATION);

		if (isset($allDone) && $allDone)
			$this->kernel->response()->redirectInternal('');
	}

	public function defaultPage() {
		$this->enableLayout = TRUE;
		$this->setTitle("Import");
		
		// Get all assignments
		$aQuery = $this->dbc->select("assignments");
		$assignments = $aQuery->fetchAll();

		// Get all students
		$sQuery = $this->dbc->select("students");
		$students = $sQuery->fetchAll();
		
		// Create Form
		$this->loadView("Import/ImportForm");
		$this->loadView("Import/ImportHelp");
	}

	public function crash() {
		throw new Exception();
	}
}
