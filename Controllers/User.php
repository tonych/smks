<?php
class UserController extends SessionController {
	private function checkLen($var, $minlen = 0) {
		return isset($var) && strlen($var) > $minlen;
	}

	private function checkPW($user, $checkPw) {
		$hex = hash(
			"sha512",
			$user->getSalt() . $checkPw . kernel()->config()->get('app/pepper', 'abcdef')
			);
		return $hex == $user->getPassword();
	}

	public function changePw() {
		if (!$this->isLoggedIn()) {
			$this->loadView('ErrorBox', [
				'content' => 'You are not logged in!',
				]);
			return $this->defaultPage();
		}

		// get vars
		$currpw = "";
		$newpw = "";
		if (isset($_POST['currpw']))
			$currpw = $_POST['currpw'];
		if (isset($_POST['newpw']))
			$newpw = $_POST['newpw'];

		// check curr pw
		$currUser = $this->getCurrentUser();
		if (!$this->checkPW($currUser, $currpw)) {
			$this->loadView("ErrorBox", [
				"content" => "The current password does not match.",
				]);
			return;
		}

		$currUser->setPassword($newpw);
		$this->kernel->response()->redirectInternal('');
	}

	public function logout() {
		if ($this->isLoggedIn()) {
			$this->session->setExpiry(time() - 3600);
		}

		$this->kernel->response()->redirectInternal('');
	}

	public function check() {
		$uname = $this->kernel->request()->getPost("uname");
		$pword = $this->kernel->request()->getPost("pword");
		$this->defaultPage($uname);

		if (!$this->checkLen($uname) || !$this->checkLen($pword)) {
			$this->loadView('ErrorBox', ['content' => 'You need both a username and password.']);
		}
		else {
			// Search for Username
			try {
				$uid = $this->usersManager->lookupEmail($uname);
				$user = $this->usersManager->getUser($uid);
			} catch (UserNotFoundException $e) {
				$this->loadView('ErrorBox', ['content' => 'Your username/password was incorrect.']);
				return;
			}

			// Check Disabled/Banned
			if ($user->hasState('disableUserSystem') || $user->hasState('disableUserDisabled')) {
				$this->loadView('ErrorBox', ['content' => 'This user has been disabled by a System Administrator.']);
				return;
			}

			// Check PW
			if ($this->checkPW($user, $pword)) {
				// Create Session
				$cookieName = $this->kernel->config()->get('app/sessionCookie', 'KSession');
				$sessionTime = $this->kernel->config()->get('app/sessionTime', 3600);

				$session = $user->createSession($sessionTime);
				$session->saveCookie($cookieName);
				$this->kernel->response()->redirectInternal('');
			}
			else {
				$this->loadView('ErrorBox', ['content' => 'Your username/password was incorrect. ']);
				return;
			}
		}
	}

	public function defaultPage($uname = "") {
		$this->setTitle("Login");

		/*$this->loadView('ContentBox', [
			'heading' => 'Login',
			'content' => $this->loadView('LoginBox', [], TRUE),
			]);
			*/
		$this->loadView('LoginBox', ['uname' => $uname]);
	}
}
