<?php
class StudentsController extends LoginRequiredController {
	public function defaultPage() {
		$this->enableLayout = TRUE;
		$this->setTitle("Students");
		
		// Get all Students (Sorted by UG/PG)
		$sQuery = $this->dbc->select("students", [], null, 'postgraduate');
		$students = $sQuery->fetchAll();

		// Get all Assignments
		$aQuery = $this->dbc->select("assignments");
		$assessment = $aQuery->fetchAll();

		// Get student's results.
		$subQuery = $this->dbc->select("submissions");
		$submissions = array();
		while ($row = $subQuery->fetchAssoc()) {
			$submissions[$row['studentId']][$row['assignmentId']] = $row;
		}
		
		// Pass onto the view...
		$this->loadView("Students/ListingHelp");
		$this->loadView("Students/StudentList", [
			"students"    => $students,
			"assessment"  => $assessment,
			"submissions" => $submissions,
			]);
		$this->loadView("Students/Search");
	}
}
