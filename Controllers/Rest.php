<?php
class RestController extends LoginRequiredController {
	public function __construct() {
		parent::__construct();
		$this->enableLayout = FALSE;
	}

	public function assignments() {
		// get assignments
		$aQuery = $this->dbc->select("assignments");
		$assignments = $aQuery->fetchAll();
	
		echo json_encode([
			'count'       => count($assignments),
			'assignments' => $assignments,
			]);
	}

	public function defaultPage() {
		echo json_encode([
			'error' => TRUE,
			'message' => 'Invalid Request'
			]);
	}
}
