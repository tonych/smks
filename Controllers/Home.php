<?php
class HomeController extends LoginRequiredController {
	public function defaultPage() {
		$this->enableLayout = TRUE;
		$this->setTitle("Dashboard");

		// Load classes.
		$cQuery = $this->dbc->getPreparedQuery(
			"SELECT workshop, postgraduate, COUNT(studentId) as numStudents "
		   ."FROM students GROUP BY postgraduate, workshop ORDER BY postgraduate, workshop ASC");
		$classResultSet = new KDatabaseResult($cQuery, $cQuery->execute());

		// Load Assignments
		$aQuery = $this->dbc->select("assignments");
		$assignments = $aQuery->fetchAll();

		// Load Assigned-To-Me
		$aQuery = $this->dbc->getPreparedQuery(sprintf(
			"SELECT assignmentId, COUNT(s.submissionId) as total, COUNT(s.marks) as marked "
		   ."FROM submissions s "
		   ."WHERE assignedTo = %d "
		   ."GROUP BY s.assignmentId "
		   ."ORDER BY s.assignmentId ",
			$this->getCurrentUser()->getUserId()
			));
		$aRs = new KDatabaseResult($aQuery, $aQuery->execute());
		$assignedAssignments = $aRs->fetchAll();

		// Load Widgets!
		$this->loadView("Widgets/Assignments", [
			'assignments' => $assignments,
			]);
		$this->loadView("Widgets/Students");
		$this->loadView("Widgets/Classes", [
			'classes' => $classResultSet->fetchAll(),
			]);
		$this->loadView("Widgets/AssignedToMe", [
			'assignedToMe' => $assignedAssignments,
			'assignments'  => $assignments,
			]);
		$this->loadView("Widgets/ChangePassword");
	}
}
