<?php
class SubmissionsController extends LoginRequiredController {
	public function __construct() {
		parent::__construct();
	}
	
	public function copy() {
		if (!isset($_POST['source']) || !isset($_POST['dest'])) {
			$this->loadView("ErrorBox", [
				"content" => "The source and destination must both be specified.",
				]);
			return;
		}
		
		$src = $_POST['source'] * 1;
		$dst = $_POST['dest'] * 1;
		
		// get both submissions.
		$q = $this->dbc->select("submissions", ["submissionId" => $src]);
		$srcSub = $q->fetchAssoc();
		$q1 = $this->dbc->select("submissions", ["submissionId" => $dst]);
		$dstSub = $q1->fetchAssoc();
		
		// check exist
		if (!$srcSub) {
			$this->loadView("ErrorBox", [
				"content" => "The source must exist.",
				]);
			return;
		}
		if (!$dstSub) {
			$this->loadView("ErrorBox", [
				"content" => "The destination $dst must exist.",
				]);
			return;
		}
		
		// get latest marking record from src
		$q = $this->dbc->select("submission_marking", 
			["submissionId" => $srcSub['submissionId']], 
			null, 
			"dateMarked", 
			'desc',
			1);
		$subMarking = $q->fetchAssoc();
		
		// check exist.
		if (!$subMarking) {
			$this->loadView("ErrorBox", [
				"content" => "The source does not have any marking records.",
				]);
			return;
		}
		
		// modify data to match
		$subMarking['submissionId'] = $dstSub['submissionId'];
		$subMarking['dateMarked'] = time();
		
		// insert 
		$this->dbc->insert("submission_marking", $subMarking);
		// update
		$this->dbc->update("submissions", [
			"marks"    => $srcSub['marks'],
			"feedback" => $srcSub['feedback'],
			],
			[ "submissionId" => $dstSub['submissionId'] ]
			);
		
		// redirect back to submission.
		$this->kernel->response()->redirectInternal("submissions/view/". $src);
	}
	
	public function view() {
		// New Layout
		$this->setLayout("Layouts/FullWidth");

		// get first ID
		$id = $this->kernel->request()->getUri()->getPart(2);
		
		// not found
		$error = FALSE;
		if ($id != NULL) {
			// try and find it
			$sQuery = $this->dbc->getPreparedQuery(sprintf("
				SELECT 
					submissionId, sub.assignmentId, sub.studentId, 
					sub.assignedTo, sub.dateSubmitted, sub.marks, 
					sub.feedback, s.firstName, s.lastName, s.email, 
					s.postgraduate, s.workshop, a.name AS assignmentName, 
					a.maxMarks, a.dueDate
				FROM submissions sub, students s, assignments a
				WHERE
				sub.submissionId = %d AND 
				sub.studentId = s.studentId AND
				sub.assignmentId = a.assignmentId",
				$id * 1));
			$sQuery = new KDatabaseResult($sQuery, $sQuery->execute());
			$submission = $sQuery->fetchAssoc();
			
			// Submission not found in DB
			if (!$submission)
				$error = TRUE;
		}
		// ID was either NULL or not an INT.
		else {
			$error = TRUE;
		}

		if ($error) {
			$this->setTitle("Submission not found");
			return $this->loadView('ErrorBox', ['content' => "Submission not found"]);
		}

		$this->setTitle("Viewing submission #" . Filter::filterText($submission['submissionId']));
		
		// Ok, lets pass this onto the View.
		$this->loadView("Submissions/SubmissionInfo", [
			"submissionInfo" => $submission,
			]);

		$basePath = [];
		$paths = kernel()->config()->get("smks/searchPaths", ["public_html"]);
		$homePath = kernel()->config()->get("smks/studentHomePath", "/home/");

		foreach ($paths as $p) {
			$newPath = realpath(sprintf("%s/%s/%s",
				$homePath,
				$submission['studentId'],
				$p));
			if (strpos($newPath, $homePath) === 0) {
				$basePath[] = $newPath;
			}
		}
		
		// get all submissions + students
		$sQuery = $this->dbc->getPreparedQuery(sprintf("
			SELECT 
				submissionId, sub.assignmentId, sub.studentId, 
				sub.marks, s.firstName, s.lastName, 
				s.postgraduate
			FROM submissions sub, students s
			WHERE 
			sub.studentId = s.studentId AND
			sub.assignmentId = %d AND
			sub.marks IS NOT NULL
            ORDER BY s.lastName",
			$submission['assignmentId'] * 1
			));
		$sQuery = new KDatabaseResult($sQuery, $sQuery->execute());

		$markedSubmissions = [];
		while ($row = $sQuery->fetchAssoc()) {
			$markedSubmissions[] = [
				"submissionId" => $row['submissionId'] * 1,
				"studentId"    => Filter::filterText($row['studentId']),
				"marks"        => $row['marks'] * 1,
				"name"         => Filter::filterText(sprintf("%s, %s", $row['lastName'], $row['firstName'])),
				"postgradute"  => $row['postgraduate'] * 1,
				];
		}
        
        // Tabbed container for tools.
		$this->loadView("Submissions/SessionTool", [
			"submissionId" => $submission['submissionId'],
			]);
		
		// Student Marking Tools
		$this->loadView("Submissions/CopyTool", [
			"submissionId" => $submission['submissionId'],
			"markedSubmissions" => $markedSubmissions,
			]);
        

		// File Listing.
		$this->loadView("Submissions/FileList", [
			"basePath" =>  $basePath,
			]);

		// Source Code Viewer
		$this->loadView("Submissions/SourceCodeViewer", [
			]);
	}

	public function read() {
		// Don't need a layout here. We're returning JSON.
		$this->enableLayout = FALSE;

		$homePath = kernel()->config()->get("smks/studentHomePath", "/home/");
		
		//
		$error = true;
		if (isset($_POST['filePath']) && strpos(($realPath = $homePath . $_POST['filePath']), $homePath) === 0 && !$this->getCurrentUser()->hasState("user/debug")) {
			$contents = ['fileContents' => Filter::removeInvalidCharacters(file_get_contents($realPath))];
		}
		else {
			$fileContents = "Error reading file: \n";
			$fileContents .= "Path Requested: ";
			if (isset($_POST['filePath'])) {
				$fileContents .= Filter::filterText($_POST['filePath']) . "\n";
			}
			else {
				$fileContents .= "[No Path Requested]\n";
			}
			
			$checkPath = strpos(($realPath = $homePath . $_POST['filePath']), $homePath) === 0;
			$fileContents .= "Is in home: ". (($checkPath) ? "TRUE" : "FALSE");
			$fileContents .= "\n";
			$fileContents .= "Last PHP Error: \n";
			$fileContents .= print_r(error_get_last(), TRUE) . "\n\n";
			
			if ($this->getCurrentUser()->hasState("user/debug")) {
				$fileContents .= "Contents: \n";
				$fileContents .= print_r(file_get_contents($realPath), true) . "\n\n";	
			}
			$contents = ['fileContents' => Filter::removeInvalidCharacters($fileContents)];
		}

		// set as JSON
		kernel()->response()->setHeader("Content-Type: text/json");
		$json = json_encode($contents);
		echo $json;
	}

	public function save() {
		// Don't need a layout here. We're returning JSON.
		$this->enableLayout = FALSE;

		// get first ID
		$id = $this->kernel->request()->getUri()->getPart(2);
		
		// not found
		$error = FALSE;
		if ($id != NULL) {
			// try and find it
			$sQuery = $this->dbc->getPreparedQuery(sprintf("
				SELECT 
					submissionId, sub.assignmentId, sub.studentId, 
					sub.assignedTo, sub.dateSubmitted, sub.marks, 
					sub.feedback, s.firstName, s.lastName, s.email, 
					s.postgraduate, s.workshop, a.name AS assignmentName, 
					a.maxMarks, a.dueDate
				FROM submissions sub, students s, assignments a
				WHERE
				sub.submissionId = %d AND 
				sub.studentId = s.studentId AND
				sub.assignmentId = a.assignmentId",
				$id * 1));
			$sQuery = new KDatabaseResult($sQuery, $sQuery->execute());
			$submission = $sQuery->fetchAssoc();
			
			// Submission not found in DB
			if (!$submission)
				$error = TRUE;
		}
		// ID was either NULL or not an INT.
		else {
			$error = TRUE;
		}

		if ($error) {
			$this->setTitle("Submission not found");
			return $this->loadView('ErrorBox', ['content' => "Submission not found"]);
		}

		// save data as is
		$success = $this->dbc->insert("submission_marking", [
			"submissionId"   => $submission['submissionId'],
			"dateMarked"     => time(),
			"markerId"       => $this->getCurrentUser()->getUserId(),
			"serializedData" => $_POST['data'], // not a security issue, KDBConnection creates a prepared  
			                                    // statement internally and automatically escapes.
			"marks"          => $_POST['marks'],
			"feedback"       => $_POST['feedback'],
		]);

		$contents = [
			"error" => !$success,
			];

		// set as JSON
		kernel()->response()->setHeader("Content-Type: text/json");
		echo json_encode($contents);
	}

	public function commit() {
		// Don't need a layout here. We're returning JSON.
		$this->enableLayout = FALSE;

		// get first ID
		$id = $this->kernel->request()->getUri()->getPart(2);
		
		// not found
		$error = FALSE;
		if ($id != NULL) {
			// try and find it
			$sQuery = $this->dbc->select("submissions", ["submissionId" => $id*1]);
			$submission = $sQuery->fetchAssoc();
			
			// Submission not found in DB
			if (!$submission)
				$error = TRUE;
		}
		// ID was either NULL or not an INT.
		else {
			$error = TRUE;
		}

		if ($error) {
			$contents = [
				"error"   => TRUE,
				"message" => "Submission ID not found.",
				];
		}
		else {
			// get last draft and save it to the main table.
			$draftQuery = $this->dbc->select("submission_marking", 
				[ "submissionId" => $submission['submissionId'], ], // where
				[ "marks", "feedback" ],                            // fields
				"dateMarked",                                       // order by field
				"desc",                                             // asc or desc
				1);                                                 // limit
			$lastDraft = $draftQuery->fetchAssoc();

			if (!$lastDraft) {
				$contents = [
					"error" => TRUE,
					"message" => "No drafts were found.",
					];
			}
			else {
				// save it.
                $saveData = [ // set
						"marks"      => $lastDraft['marks'] * 1,
						"feedback"   => $lastDraft['feedback'],
					];
                
                if (!$this->getCurrentUser()->hasState("user/ignoreCommit")) {
                    $saveData["assignedTo"] = $this->getCurrentUser()->getUserId();
                }
                
				$success = $this->dbc->update("submissions", 
					$saveData,
					[ // where
						"submissionId" => $submission['submissionId'],
					]);

				$contents = [
					"error" => !$success,
					"message" => (!$success) ? $this->dbc->errorMessage() : "Success",
				];
			}
		}

		// set as JSON
		kernel()->response()->setHeader("Content-Type: text/json");
		echo json_encode($contents);
	}

	public function load() {
		// Don't need a layout here. We're returning JSON.
		$this->enableLayout = FALSE;

		// get first ID
		$id = $this->kernel->request()->getUri()->getPart(2);
		
		// not found
		$error = FALSE;
		if ($id != NULL) {
			// try and find it
			$sQuery = $this->dbc->select("submissions", ["submissionId" => $id*1]);
			$submission = $sQuery->fetchAssoc();
			
			// Submission not found in DB
			if (!$submission)
				$error = TRUE;
		}
		// ID was either NULL or not an INT.
		else {
			$error = TRUE;
		}

		if ($error) {
			$contents = [
				"error"   => TRUE,
				"message" => "Submission ID not found.",
				];
		}
		else {
			// get last draft and save it to the main table.
			$draftQuery = $this->dbc->select("submission_marking", 
				[ "submissionId" => $submission['submissionId'], ], // where
				NULL,                                              // fields
				"dateMarked",                                       // order by field
				"desc",                                             // asc or desc
				1);                                                 // limit
			$lastDraft = $draftQuery->fetchAssoc();

			if (!$lastDraft) {
				$contents = [
					"error" => TRUE,
					"No drafts were found.",
					];
			}
			else {
				$contents = [
					"error"     => FALSE,
					"data"      => $lastDraft['serializedData'],
					"marks"     => $lastDraft['marks'],
					"feedback"  => $lastDraft['feedback'],
					"lastSaved" => date("H:i:s d/M/Y", $lastDraft['dateMarked']),
				];
			}
		}

		// set as JSON
		kernel()->response()->setHeader("Content-Type: text/json");
		echo json_encode($contents);
	}

	public function mark() {
		// New Layout
		$this->setLayout("Layouts/UILess");

		// get first ID
		$id = $this->kernel->request()->getUri()->getPart(2);
		
		// not found
		$error = FALSE;
		if ($id != NULL) {
			// try and find it
			$sQuery = $this->dbc->getPreparedQuery(sprintf("
				SELECT 
					submissionId, sub.assignmentId, sub.studentId, 
					sub.assignedTo, sub.dateSubmitted, sub.marks, 
					sub.feedback, s.firstName, s.lastName, s.email, 
					s.postgraduate, s.workshop, a.name AS assignmentName, 
					a.maxMarks, a.dueDate
				FROM submissions sub, students s, assignments a
				WHERE
				sub.submissionId = %d AND 
				sub.studentId = s.studentId AND
				sub.assignmentId = a.assignmentId",
				$id * 1));
			$sQuery = new KDatabaseResult($sQuery, $sQuery->execute());
			$submission = $sQuery->fetchAssoc();
			
			// Submission not found in DB
			if (!$submission)
				$error = TRUE;
		}
		// ID was either NULL or not an INT.
		else {
			$error = TRUE;
		}

		if ($error) {
			$this->setTitle("Submission not found");
			return $this->loadView('ErrorBox', ['content' => "Submission not found"]);
		}
			
		// load criteria sheet
		$this->loadView("Submissions/Criteria/{$submission['assignmentId']}", [
			"submissionInfo" => $submission,
			]);
	}
    
    private function resetSubmission($id) {
		// not found
		$error = FALSE;
		if ($id != NULL) {
			// try and find it
			$sQuery = $this->dbc->select(
                "submission_marking", 
                ["submissionId" => $id*1],
                null,
                'dateMarked',
                'desc',
                1);

			$submission = $sQuery->fetchAssoc();
			
			// Submission not found in DB
			if (!$submission)
			 return false;
		}
		// ID was either NULL or not an INT.
		else {
			return false;
		}
        
        //print_r(json_decode($submission['serializedData']));
        
        // calc marks
        $totalValues = [25, 15, 15, 15, 10, 10, 10];
        $curr  = 0;
        $total = 0;
        
        $decoded = json_decode($submission['serializedData']);
        foreach ($decoded->data as $k => $v) {
            $pCurr  = 0;
            $pTotal = 0;
            foreach ($v->marks as $m) {
                $pCurr  += $m * 1;
                $pTotal += 7;
            }
            
            if ($v->optional && !$v->optionalValue) {
                // do nothing
            }
            else {
                $curr  += ($pCurr / $pTotal) * $totalValues[$k];
                $total += $totalValues[$k];
            }
        }
        
        $actualMarks = $curr / $total * 25;
        $data = [
            "submissionId"   => $submission['submissionId'],
            "dateMarked"     => time(),
            "markerId"       => $submission['markerId'],
            "serializedData" => $submission['serializedData'],
            "marks"          => $actualMarks,
            "feedback"       => $submission['feedback'],
        ];
        $this->dbc->insert("submission_marking", $data);
        
        // update on main table.
        $update = [
            "marks"    => $actualMarks,
            "feedback" => $submission['feedback'],
        ];
        
        $whereUpdate = [
            "submissionId" => $submission['submissionId'],
        ];
        $this->dbc->update("submissions", $update, $whereUpdate);
        
        return true;
    }
    
    private function resetMarking() {
        $this->enableLayout = FALSE;
        
        $sQuery = $this->dbc->getPreparedQuery("
            SELECT DISTINCT submissionId 
            FROM `submission_marking` 
            GROUP BY submissionId");
        $sQuery = new KDatabaseResult($sQuery, $sQuery->execute());
        
        while ($r = $sQuery->fetchAssoc()) {
            if ($this->resetSubmission($r['submissionId'])) {
                printf("%d: SUCCESS!<br>\n", $r['submissionId']);
            }
            else {
                printf("%d: FAILURE!<br>\n", $r['submissionId']);
            }
        }
    }
    
    public function clearCookies() {
		// get subid if set.
		$id = isset($_POST['subId']) ? $_POST['subId'] : NULL;
        
        // get all cookies
        $phpSessConfig = session_get_cookie_params();
        
        foreach ($_COOKIE as $name => $value) {
            if ($name != $this->kernel->config()->get("app/sessionCookie", "ksnsSession"))
                setcookie($name, "", time() - 3600, 
                    $phpSessConfig["path"],
                    $phpSessConfig["domain"],
                    $phpSessConfig["secure"],
                    $phpSessConfig["httponly"]);
        }
        
        if (!is_null($id))
            $this->kernel->response()->redirectInternal("submissions/view/$id");
        else
            $this->kernel->response()->redirectInternal("assignments");
    }
    
    public function searchTerm() {
		// Don't need a layout here. We're returning JSON.
		$this->enableLayout = FALSE;
        $contents = ['matches' => []];

		$homePath = kernel()->config()->get("smks/studentHomePath", "/home/");
        $term = isset($_POST['term']) ? $_POST['term'] : NULL;
        $contents['id'] = isset($_POST['id']) ? $_POST['id'] : NULL;
		
		//
		$error = true;
		if (isset($_POST['filePath']) && 
            isset($term) &&
            strpos(($realPath = $homePath . $_POST['filePath']), $homePath) === 0 && 
            !$this->getCurrentUser()->hasState("user/debug")) 
        {
            $fileContents = file_get_contents($realPath);
            
            // 
            $lines = explode("\n", Filter::removeInvalidCharacters($fileContents));
            
            for ($i = 0; $i < count($lines); $i++) {
                if (stristr($lines[$i], $term)) {
                    $contents['matches'][] = [$i, $lines[$i]];
                }
            }
		}
		else {
			$fileContents = "Error reading file: \n";
			$fileContents .= "Path Requested: ";
			if (isset($_POST['filePath'])) {
				$fileContents .= Filter::filterText($_POST['filePath']) . "\n";
			}
			else {
				$fileContents .= "[No Path Requested]\n";
			}
			
			$checkPath = strpos(($realPath = $homePath . $_POST['filePath']), $homePath) === 0;
			$fileContents .= "Is in home: ". (($checkPath) ? "TRUE" : "FALSE");
			$fileContents .= "\n";
			$fileContents .= "Last PHP Error: \n";
			$fileContents .= print_r(error_get_last(), TRUE) . "\n\n";
			
			if ($this->getCurrentUser()->hasState("user/debug")) {
				$fileContents .= "Contents: \n";
				$fileContents .= print_r(file_get_contents($realPath), true) . "\n\n";	
			}
			$contents = ['fileContents' => Filter::removeInvalidCharacters($fileContents)];
		}

		// set as JSON
		kernel()->response()->setHeader("Content-Type: text/json");
		$json = json_encode($contents);
		echo $json;
    }
    
    public function viewDebug() {
        $this->enableLayout = FALSE;

		// get first ID
		$id = $this->kernel->request()->getUri()->getPart(2);
        
        // not found
		$error = FALSE;
		if ($id != NULL) {
			// try and find it
			$sQuery = $this->dbc->select(
                "submission_marking", 
                ["submissionId" => $id*1],
                null,
                'dateMarked',
                'desc',
                1);

			$submission = $sQuery->fetchAssoc();
			
			// Submission not found in DB
			if (!$submission)
			 return false;
		}
		// ID was either NULL or not an INT.
		else {
			return false;
		}
        
        print_r(json_decode($submission['serializedData']));
        $decoded = json_decode($submission['serializedData']);
        $optionalEnabled = 0;
        
        echo "<br><br>";
        // decode and count
        foreach ($decoded->data as $k => $v) {
            if ($v->optional && $v->optionalValue) {
                $optionalEnabled++;
            }
        }
        printf("Optional: %d", $optionalEnabled);
    }

	public function defaultPage() {
		echo "<h1>You shouldn't be here.</h1>";
	}
}
