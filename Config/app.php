<?php
return array(
	/**
	 * Application Settings
	 *
	 * These are settings not related to packages or extensions but just the controllers.
	 */
	'app/sessionCookie' => 'ksnsSession',
	'app/sessionTime'   => 6 * 60 * 60,
	'app/sessionPath'   => '/smks/',
	'app/dateFormat'    => 'j M Y, H:i:s',
	'app/pepper'        => 'vv[)W,HaE(()L&;Q{AY)9M)i\GvP3\3',

	/**
	* KDB Database Connection Options
	*
	* This is where you can setup your database support. You can opt to not have
	* a database connection in which you can just leave this as a blank array
	* (i.e. array()).
	*
	* If you choose to have a database connection, you can assign ID's for
	* multiple database connections which can be accessed by doing
	* getDbConnection($id).
	*
	* If you do have multiple database connections, one must be assigned the
	* default database connection.
	*
	* Note that if multiple are set as "default", only the last one will take
	* effect.
	*/
	'app/db' => array(
		'defaultDb' => array(
			'dbClass' => 'KMySqlConnection',
			'dbHost'  => '127.0.0.1',
			'dbUser'  => 'smks',
			'dbPass'  => '4yGMGw8aB7KTUTRe',
			'dbName'  => 'smks',
			'default' => TRUE,
		),
	),

	/**
	 * Web Extension Options
	 *
	 * This extension extends the Kernel to include a View-Controller system
	 * which allows seperation of logic and view. These options here are
	 * to mainly control what is the default page to load when none has
	 * been specifically loaded.
	 *
	 * app/defaultController:
	 *   This sets the default controller to load when none is specified.
	 * app/defaultAction:
	 *   This sets the default action to load when none has been specified.
	 * app/defaultLayout:
	 *   This sets the default layout to use when none has been set within
	 *   the controller.
	 */
	"app/defaultController" => "home",
	"app/defaultAction" => "defaultPage",
	"app/defaultLayout" => "Layouts/Main",
	"app/defaultLogin" => "user/",

	// SMKS specific
	"smks/studentHomePath" => "/home/jail/home/",
	"smks/searchPaths"     => [
		"secure_html",
		"public_html",
		],
	"smks/css_validator"         => "http://jigsaw.w3.org/css-validator/validator?uri=",
	"smks/html_validator"        => "http://validator.w3.org/check?uri=",
    "smks/countAddonViaFeedback" => TRUE,
);
