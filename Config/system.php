<?php
return array(
	// This is the path which leads to the packages. As this is automatically 
	// loaded upon startup, any packages should be place within these paths. 
	"system/packagePaths" => array(
		CORE_PATH . 'Packages/',
		),
	
	// This is the URL which leads to this website. Whilst this does not have
	// to be set, the auto-detection method does not always work in some 
	// server configurations. Some configurations where this will not work are
	// behind a reverse proxy and in a load balancer configuration.
	//"system/urlroot" => "http://put.your.website.here/"

	// This should be set to where your default timezone is.
	"system/defaultTimezone" => "Australia/Brisbane",
	
	// This is where packages can be added to be ignored by the package 
	// manager. Placing a package here will render it invisible to the manager,
	// and will basically mean that the package will not be loaded. 
	"system/ignorePackages" => array('Contents'),
);