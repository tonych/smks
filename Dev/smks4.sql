-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2014 at 05:04 PM
-- Server version: 5.6.16
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smks`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE IF NOT EXISTS `assignments` (
  `assignmentId` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `maxMarks` double NOT NULL,
  `dueDate` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`assignmentId`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `groupId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `groupName` (`groupName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`setting_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `stateName` varchar(64) COLLATE utf8_bin NOT NULL,
  `isGroupState` tinyint(1) NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`stateName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `studentId` varchar(10) CHARACTER SET latin1 NOT NULL,
  `firstName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `lastName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `postgraduate` int(1) unsigned NOT NULL DEFAULT '0',
  `workshop` varchar(10) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`studentId`),
  KEY `firstName` (`firstName`,`lastName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

CREATE TABLE IF NOT EXISTS `submissions` (
  `submissionId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `studentId` varchar(10) NOT NULL,
  `assignmentId` int(11) NOT NULL,
  `dateSubmitted` bigint(20) unsigned NOT NULL,
  `assignedTo` mediumint(8) unsigned NOT NULL,
  `marks` double DEFAULT NULL,
  `feedback` text NOT NULL,
  PRIMARY KEY (`submissionId`),
  KEY `assignmentId` (`assignmentId`),
  KEY `studentId` (`studentId`),
  KEY `assignedTo` (`assignedTo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `submission_files`
--

CREATE TABLE IF NOT EXISTS `submission_files` (
  `fileId` mediumint(8) unsigned NOT NULL,
  `submissionId` int(10) unsigned NOT NULL,
  `path` text NOT NULL,
  `dateModified` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`fileId`),
  KEY `submissionId` (`submissionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `submission_marking`
--

CREATE TABLE IF NOT EXISTS `submission_marking` (
  `submissionId` mediumint(8) unsigned NOT NULL,
  `dateMarked` bigint(20) unsigned NOT NULL,
  `markerId` mediumint(8) unsigned NOT NULL,
  `marks` double DEFAULT NULL,
  `feedback` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`submissionId`,`dateMarked`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `salt` varchar(40) COLLATE utf8_bin NOT NULL,
  `realName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_sessions`
--

CREATE TABLE IF NOT EXISTS `users_sessions` (
  `sessionId` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` mediumint(8) unsigned NOT NULL,
  `sessionKey` varchar(64) COLLATE utf8_bin NOT NULL,
  `sessionStart` bigint(20) unsigned NOT NULL,
  `expiry` bigint(20) unsigned NOT NULL,
  `userAgent` varchar(255) COLLATE utf8_bin NOT NULL,
  `ipAddress` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`sessionId`,`userId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users_settings`
--

CREATE TABLE IF NOT EXISTS `users_settings` (
  `userId` mediumint(8) unsigned NOT NULL,
  `settingName` varchar(64) COLLATE utf8_bin NOT NULL,
  `settingValue` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`,`settingName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users_states`
--

CREATE TABLE IF NOT EXISTS `users_states` (
  `userId` mediumint(8) unsigned NOT NULL,
  `stateName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`,`stateName`),
  KEY `stateName` (`stateName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_sessions`
--
ALTER TABLE `users_sessions`
  ADD CONSTRAINT `users_sessions_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_settings`
--
ALTER TABLE `users_settings`
  ADD CONSTRAINT `users_settings_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_states`
--
ALTER TABLE `users_states`
  ADD CONSTRAINT `users_states_ibfk_1` FOREIGN KEY (`stateName`) REFERENCES `states` (`stateName`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_states_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
