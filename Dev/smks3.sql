-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2014 at 12:52 PM
-- Server version: 5.6.16
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smks`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE IF NOT EXISTS `assignments` (
  `assignmentId` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `maxMarks` double NOT NULL,
  `dueDate` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`assignmentId`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`assignmentId`, `name`, `maxMarks`, `dueDate`) VALUES
(1, 'Assignment 1 - Web Design ', 25, 0),
(2, 'Assignment 2 - Dynamic web site implementation ', 30, 0),
(490396, 'Final Exam Part A ', 20, 0),
(490397, 'Final Exam Part B ', 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `groupId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `groupName` (`groupName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`setting_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_name`, `value`) VALUES
('smks/gracePeriod', '3600');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `stateName` varchar(64) COLLATE utf8_bin NOT NULL,
  `isGroupState` tinyint(1) NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`stateName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`stateName`, `isGroupState`, `description`) VALUES
('disableGroupSystem', 1, 'Do not DELETE THIS GROUP! System Group.'),
('disableUserDisabled', 0, 'This state is given to users who have been disabled.'),
('disableUserSystem', 0, 'Do not DELETE THIS USER! System User.'),
('specialGroupEveryone', 1, 'This is the Everyone Group. It represents Everyone.'),
('specialGroupFriends', 1, 'This is the Friends Group. It represents all your Friends (those you have added to your groups). '),
('specialUserAdmin', 0, 'This is an Admin User.'),
('specialUserEveryone', 0, 'This is the Everyone User. It represents everyone.'),
('specialUserFriends', 0, 'This is the Friends User. It represents your friends. ');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `studentId` varchar(10) CHARACTER SET latin1 NOT NULL,
  `firstName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `lastName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `postgraduate` int(1) unsigned NOT NULL DEFAULT '0',
  `workshop` varchar(10) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`studentId`),
  KEY `firstName` (`firstName`,`lastName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`studentId`, `firstName`, `lastName`, `email`, `postgraduate`, `workshop`) VALUES
('n1155911', 'KEN', 'MCQUEEN', 'ken.mcqueen@student.qut.edu.au', 0, 'WOR 2'),
('n1581287', 'JULIE NARELLE', 'PETERS', 'jn.peters@student.qut.edu.au', 1, 'WOR 3'),
('n2280418', 'RAFI UDDIN', 'MUZAFFAR', 'r.muzaffar@student.qut.edu.au', 1, 'WOR 2'),
('n4768205', 'STEVEN RUSSELL', 'KELLY', 'sr.kelly@student.qut.edu.au', 0, 'WOR 10'),
('n5396905', 'GREGORY LAWRENCE', 'SANKOWSKY', 'g.sankowsky@student.qut.edu.au', 0, 'WOR 4'),
('n5420687', 'JESSE RYAN', 'CUNNINGHAM - CREECH', 'j.cunningham-creech@student.qut.edu.au', 0, 'WOR 3'),
('n5700892', 'ANTHONY PAUL', 'COLLISHAW', 'a.collishaw@student.qut.edu.au', 0, 'WOR 4'),
('n609498', 'NICHOLAS ANDREW', 'THOMPSON', 'na.thompson@student.qut.edu.au', 0, 'WOR 6'),
('n6561985', 'DANIEL', 'LUCKHURST', 'daniel.luckhurst@student.qut.edu.au', 0, 'WOR 4'),
('n6777198', 'RONALD', 'LIOPISA', 'ronald.liopisa@student.qut.edu.au', 0, 'WOR 9'),
('n6871089', 'DOMONIC RICHARD', 'GRAHAM', 'domonic.graham@student.qut.edu.au', 0, 'WOR 5'),
('n6878296', 'ALEXANDER JOHN', 'CRICHTON', 'alexander.crichton@student.qut.edu.au', 1, 'WOR 1'),
('n7052952', 'CHHODEN', 'CHHODEN', 'chhoden@student.qut.edu.au', 0, 'WOR 7'),
('n7061579', 'VINO', 'JACOB', 'vino.jacob@student.qut.edu.au', 0, 'WOR 10'),
('n7084412', 'MASON', 'TRAN', 'mason.tran@student.qut.edu.au', 0, 'WOR 2'),
('n7111258', 'JIN PING', 'CHNG', 'jin.chng@student.qut.edu.au', 1, 'WOR 3'),
('n7116594', 'ASHLEY ROBERT', 'DAVIS', 'a1.davis@student.qut.edu.au', 1, 'WOR 1'),
('n7133634', 'GAVIN', 'SUDDREY', 'gavin.suddrey@student.qut.edu.au', 1, 'WOR 1'),
('n7148526', 'LUKE RAYMOND', 'KEARNEY', 'luke.kearney@student.qut.edu.au', 0, 'WOR 9'),
('n7164092', 'PHILLIP DENIS', 'HEWSTON', 'phillip.hewston@student.qut.edu.au', 0, 'WOR 10'),
('n7227639', 'VICTOR', 'PUC', 'victor.puc@student.qut.edu.au', 0, 'WOR 6'),
('n7237502', 'TOM JAMES', 'HORAN', 'tom.horan@student.qut.edu.au', 0, 'WOR 9'),
('n7431775', 'MICHAEL JAMES', 'SWARBRICK', 'michael.swarbrick@student.qut.edu.au', 0, 'WOR 10'),
('n7449852', 'NICHOLAS ANDREW', 'JOHNSON', 'n7.johnson@student.qut.edu.au', 0, 'WOR 9'),
('n7497547', 'RICHARD TRISTAN', 'FLANAGAN', 'tristan.flanagan@student.qut.edu.au', 1, 'WOR 1'),
('n7507267', 'ZACHARY LEE CHARLES', 'REIMERS', 'zachary.reimers@student.qut.edu.au', 0, 'WOR 4'),
('n7515537', 'AARON PHILLIP', 'BLAIR', 'aaron.blair@student.qut.edu.au', 0, 'WOR 4'),
('n7519885', 'JACOB MICHAEL', 'FIFE', 'jacob.fife@student.qut.edu.au', 0, 'WOR 4'),
('n7533586', 'TRISTAN', 'VENABLES', 'tristan.venables@student.qut.edu.au', 1, 'WOR 1'),
('n7547242', 'ROHAN', 'STEVENSON', 'rohan.stevenson@student.qut.edu.au', 0, 'WOR 6'),
('n7551339', 'MATTHEW WILLIAM', 'FINCH', 'matthew.finch@student.qut.edu.au', 0, 'WOR 9'),
('n7571682', 'BENJAMIN SCOTT', 'GRAHAM', 'benjamin.graham@student.qut.edu.au', 0, 'WOR 4'),
('n7617313', 'LI PEI', 'WANG', 'lp.wang@student.qut.edu.au', 0, 'WOR 6'),
('n7639325', 'PANPAN', 'ZHAO', 'panpan.zhao@student.qut.edu.au', 1, 'WOR 1'),
('n7639694', 'YANQIU', 'YAN', 'yq.yan@student.qut.edu.au', 0, 'WOR 9'),
('n7658788', 'DAVID ANDREW', 'HRYCKIEWICZ', 'david.hryckiewicz@student.qut.edu.au', 1, 'WOR 3'),
('n7677448', 'WEIJIE', 'HE', 'wj.he@student.qut.edu.au', 0, 'WOR 3'),
('n7680058', 'TARIQ FARDAN A', 'ALAMRI', 'tariq.alamri@student.qut.edu.au', 1, 'WOR 1'),
('n7683014', 'TOMASZ', 'GOLINSKI', 'tomasz.golinski@student.qut.edu.au', 0, 'WOR 4'),
('n7687206', 'PENG', 'LIN', 'peng.lin@student.qut.edu.au', 1, 'WOR 1'),
('n7707436', 'SHUOKUN ALEX', 'LIU', 'shuoken.liu@student.qut.edu.au', 0, 'WOR 6'),
('n8005290', 'RORY DANIEL', 'LEAHY', 'rory.leahy@student.qut.edu.au', 0, 'WOR 1'),
('n8019347', 'LACHLAN MITCHELL JOHN', 'WEH', 'lachlan.weh@student.qut.edu.au', 0, 'WOR 6'),
('n8020396', 'MADOLYN THERESE', 'O''SULLIVAN', 'madolyn.osullivan@student.qut.edu.au', 0, 'WOR 5'),
('n8020892', 'SEBASTIAN FRANCIS', 'MEIER', 'sebastian.meier@student.qut.edu.au', 0, 'WOR 10'),
('n8036535', 'KARL ANDREW', 'PETERS', 'karl.peters@student.qut.edu.au', 0, 'WOR 8'),
('n8037256', 'FREDERICK', 'DU PLESSIS', 'frederick.duplessis@student.qut.edu.au', 0, 'WOR 6'),
('n8037523', 'GLEN LEWIS', 'BRYER', 'glen.bryer@student.qut.edu.au', 0, 'WOR 6'),
('n8096562', 'ANDREW ROBERT', 'SNODGRASS', 'a.snodgrass@student.qut.edu.au', 0, 'WOR 10'),
('n8102210', 'SEAN DANIEL', 'BURNETT', 'sean.burnett@student.qut.edu.au', 0, 'WOR 8'),
('n8276617', 'HAYDEN PETER', 'JACKSON', 'hayden.jackson@student.qut.edu.au', 0, 'WOR 1'),
('n8283290', 'JOSHUA PETER', 'RUTTEN', 'joshua.rutten@student.qut.edu.au', 0, 'WOR 4'),
('n8292230', 'MITCHELL JAMES', 'KAVANAGH', 'mitchell.kavanagh@student.qut.edu.au', 0, 'WOR 5'),
('n8292663', 'JAMES', 'ROBERTSON', 'j11.robertson@student.qut.edu.au', 0, 'WOR 10'),
('n8296383', 'BRIDGET ELLEN', 'KELLY', 'bridget.kelly@student.qut.edu.au', 0, 'WOR 6'),
('n8307148', 'JORDIE ALEXANDER', 'KNOWLES', 'jordie.knowles@student.qut.edu.au', 0, 'WOR 1'),
('n8320799', 'REECE JOHN ARTHUR', 'PERHAM', 'reece.perham@student.qut.edu.au', 0, 'WOR 4'),
('n8324395', 'HARRISON ZACARIAH', 'REYNOLDS', 'harrison.reynolds@student.qut.edu.au', 0, 'WOR 10'),
('n8326932', 'JAMES GREGORY', 'ALLAN', 'james.allan@student.qut.edu.au', 0, 'WOR 7'),
('n8334528', 'DAVID CHARLES', 'SWEENEY', 'david.sweeney@student.qut.edu.au', 0, 'WOR 9'),
('n8334579', 'ALEXANDER JAMES', 'MCNEILL', 'alexander.mcneill@student.qut.edu.au', 0, 'WOR 1'),
('n8342113', 'MICHAEL PHILLIP', 'ARGYRIS', 'michael.argyris@student.qut.edu.au', 0, 'WOR 6'),
('n8351465', 'DANIEL JAMES', 'STENNETT', 'daniel.stennett@student.qut.edu.au', 0, 'WOR 4'),
('n8401438', 'BRADLEY KURT', 'NOHREITER', 'bradley.nohreiter@student.qut.edu.au', 0, 'WOR 1'),
('n8421871', 'VICTOR', 'TRISNATA', 'victor.trisnata@student.qut.edu.au', 0, 'WOR 9'),
('n8426040', 'RAJAKARUNA MUDIYANSELAGE RUWAN PRASANNA', 'PERERA', 'rajakarunanprasanna.perera@student.qut.edu.au', 0, 'WOR 10'),
('n8427631', 'DANIEL PETER', 'JOHNSTON', 'd13.johnston@student.qut.edu.au', 0, 'WOR 10'),
('n8436801', 'TSZ CHUNG', 'WU', 'tszchung.wu@student.qut.edu.au', 0, 'WOR 8'),
('n8445851', 'HAN LIN', 'LIANG', 'h2.liang@student.qut.edu.au', 0, 'WOR 9'),
('n8449139', 'ADNAN ALI M', 'ALZAHRANI', 'adnanalim.alzahrani@student.qut.edu.au', 1, 'WOR 3'),
('n8449538', 'MINSUK', 'KIM', 'minsuk.kim@student.qut.edu.au', 0, 'WOR 4'),
('n8455074', 'CHRISTOPHER AARON', 'THORNE', 'christopher.thorne@student.qut.edu.au', 0, 'WOR 5'),
('n8477311', 'JUSTIN ZHEN TIAN', 'YUNG', 'justin.yung@student.qut.edu.au', 0, 'WOR 3'),
('n8490040', 'RAMYA', 'RAPARTHY', 'ramya.raparthy@student.qut.edu.au', 0, 'WOR 10'),
('n8495505', 'KARL RICARDO', 'MARKIEWICZ', 'karl.markiewicz@student.qut.edu.au', 0, 'WOR 3'),
('n8495581', 'SEAN PATRICK', 'MAGIN', 'sean.magin@student.qut.edu.au', 0, 'WOR 2'),
('n8498415', 'JAYDEN BENJAMIN', 'WHILEY', 'jayden.whiley@student.qut.edu.au', 0, 'WOR 6'),
('n8505161', 'ABDULLAH MUGHAMIS M', 'ALMUGHAMIS', 'abdullahmughamism.almughamis@student.qut.edu.au', 1, 'WOR 2'),
('n8515301', 'BRANDON', 'SOO QUAN YIN', 'brandon.sooquanyin@student.qut.edu.au', 0, 'WOR 9'),
('n8539812', 'MARCO', 'DIOTALLEVI', 'marco.diotallevi@student.qut.edu.au', 0, 'WOR 5'),
('n8539944', 'STEVEN ALDO', 'CENTOFANTI', 'steven.centofanti@student.qut.edu.au', 0, 'WOR 1'),
('n8549257', 'QUENTIN JOHN', 'PROUT', 'quentin.prout@student.qut.edu.au', 0, 'WOR 3'),
('n8549346', 'KANNON KA - LONG', 'CHAN', 'kannon.chan@student.qut.edu.au', 0, 'WOR 1'),
('n8551235', 'ELLIOT CRAIG', 'ANDERSON', 'elliot.anderson@student.qut.edu.au', 0, 'WOR 8'),
('n8553980', 'BUSHRA SULAIMAN A', 'ABU ABAH', 'bushrasulaimana.abuabah@student.qut.edu.au', 0, 'WOR 3'),
('n8554064', 'AI YAO', 'GUO', 'aiyao.guo@student.qut.edu.au', 0, 'WOR 9'),
('n8558302', 'LAKEESHA ISHANI JAYASOORIYA', 'SABAPATHI MUDIYANSELAGE', 'l.sabapathimudiyanselage@student.qut.edu.au', 0, 'WOR 5'),
('n8562971', 'ZACKARY KEITH', 'WATSON', 'zackary.watson@student.qut.edu.au', 0, 'WOR 5'),
('n8564825', 'JOHN THOMAS', 'HEGARTY', 'jt.hegarty@student.qut.edu.au', 0, 'WOR 6'),
('n8568944', 'BENJAMIN JOHN', 'ARMSTRONG', 'b6.armstrong@student.qut.edu.au', 0, 'WOR 7'),
('n8570655', 'BRENDEN', 'LOUW', 'brenden.louw@student.qut.edu.au', 0, 'WOR 6'),
('n8572801', 'JOSEPH', 'SBEGHEN', 'joseph.sbeghen@student.qut.edu.au', 0, 'WOR 5'),
('n8572810', 'THOMAS RAYMOND', 'KAZAKOFF', 'thomas.kazakoff@student.qut.edu.au', 0, 'WOR 5'),
('n8581827', 'ANDREW THOMAS', 'LEARMONTH', 'andrew.learmonth@student.qut.edu.au', 0, 'WOR 6'),
('n8582017', 'QUENTIN ROSS', 'TWYFORD', 'quentin.twyford@student.qut.edu.au', 0, 'WOR 10'),
('n8582131', 'MATTHEW NICHOLAS', 'BALL', 'matthew.ball@student.qut.edu.au', 0, 'WOR 5'),
('n8584001', 'ANDREW DAWSON', 'HOLTHAM', 'andrew.holtham@student.qut.edu.au', 0, 'WOR 4'),
('n8584095', 'THOMAS EDWARD', 'PARKER', 'thomas.parker@student.qut.edu.au', 0, 'WOR 6'),
('n8589542', 'JAMESON LIAM', 'LOCK', 'jameson.lock@student.qut.edu.au', 0, 'WOR 7'),
('n8591733', 'LUCAS CHRISTIAN', 'NIELSEN', 'lucas.nielsen@student.qut.edu.au', 0, 'WOR 6'),
('n8593370', 'OLIVER JAMES', 'GIESS', 'oliver.giess@student.qut.edu.au', 0, 'WOR 1'),
('n8593469', 'ELLIOT', 'SPECK', 'elliot.speck@student.qut.edu.au', 0, 'WOR 8'),
('n8594031', 'DYLAN GEORGE', 'SCHUMACHER', 'dylan.schumacher@student.qut.edu.au', 0, 'WOR 2'),
('n8594996', 'MITCHELL FRANCIS', 'NUGENT', 'mitchell.nugent@student.qut.edu.au', 0, 'WOR 9'),
('n8598525', 'COREY JAMES', 'COLAK', 'corey.colak@student.qut.edu.au', 0, 'WOR 2'),
('n8600287', 'MICHELLE MAI - THIEN', 'TRAN', 'mm.tran@student.qut.edu.au', 0, 'WOR 9'),
('n8600571', 'JACKSON DAVID', 'POWELL', 'jackson.powell@student.qut.edu.au', 0, 'WOR 1'),
('n8601097', 'JAMIE WILLIAM', 'CROKER', 'jamie.croker@student.qut.edu.au', 0, 'WOR 6'),
('n8601429', 'STEVEN JAMES CALE', 'LOWE', 's6.lowe@student.qut.edu.au', 0, 'WOR 7'),
('n8602476', 'ALBERT', 'CHEN', 'albert.chen@student.qut.edu.au', 0, 'WOR 10'),
('n8602484', 'CEDRIC', 'CARLOS', 'cedric.carlos@student.qut.edu.au', 0, 'WOR 2'),
('n8603081', 'CHRISTOPHER JOHN', 'LEACH', 'christopher.leach@student.qut.edu.au', 0, 'WOR 6'),
('n8603715', 'REECE ETHAN', 'GIVNEY', 'reece.givney@student.qut.edu.au', 0, 'WOR 6'),
('n8604932', 'PETER', 'MARTINI', 'peter.martini@student.qut.edu.au', 0, 'WOR 4'),
('n8605025', 'TOBY JOHN', 'FRENCH', 'toby.french@student.qut.edu.au', 0, 'WOR 1'),
('n8605262', 'MICHAEL ROSS', 'BUTLER', 'mr.butler@student.qut.edu.au', 0, 'WOR 9'),
('n8605599', 'BRENDAN', 'NGO', 'brendan.ngo@student.qut.edu.au', 0, 'WOR 1'),
('n8608032', 'ANDREW FREDERICK', 'ROFFEY', 'andrew.roffey@student.qut.edu.au', 0, 'WOR 7'),
('n8609195', 'AARON BRENT', 'FITZGERALD', 'aaron.fitzgerald@student.qut.edu.au', 0, 'WOR 3'),
('n8609888', 'NELSON', 'RYAN', 'nelson.ryan@student.qut.edu.au', 0, 'WOR 6'),
('n8612251', 'SHANE PATRICK', 'BENNETT', 'shane.bennett@student.qut.edu.au', 0, 'WOR 4'),
('n8615136', 'YINLONG', 'SUN', 'yinlong.sun@student.qut.edu.au', 1, 'WOR 3'),
('n8615560', 'DANIEL JAMES', 'ARMSTRONG', 'd4.armstrong@student.qut.edu.au', 0, 'WOR 9'),
('n8616426', 'ROMMEL DE VERA', 'LAPUZ', 'rommel.lapuz@student.qut.edu.au', 0, 'WOR 7'),
('n8617791', 'BLAKE KIRBY', 'SHERWOOD', 'blake.sherwood@student.qut.edu.au', 0, 'WOR 8'),
('n8620156', 'IVAN ARTHUR', 'GRAVEUR', 'ivan.graveur@student.qut.edu.au', 0, 'WOR 7'),
('n8620954', 'TRISTAN', 'ALLEN', 'tristan.allen@student.qut.edu.au', 0, 'WOR 6'),
('n8622108', 'ANDREW DAVID', 'KIRKWOOD', 'andrew.kirkwood@student.qut.edu.au', 0, 'WOR 6'),
('n8623139', 'LUCKY EMMANUEL', 'KATAHANAS', 'lakis.katahanas@student.qut.edu.au', 0, 'WOR 1'),
('n8626863', 'ZAFER ALI Z', 'ALQARNI', 'zaferaliz.alqarni@student.qut.edu.au', 1, 'WOR 1'),
('n8628769', 'YU', 'ZHANG', 'y125.zhang@student.qut.edu.au', 0, 'WOR 2'),
('n8632618', 'CHUN TING', 'WEI', 'chun.wei@student.qut.edu.au', 1, 'WOR 3'),
('n8641528', 'IAIN PATRICK', 'MCDOWALL', 'iain.mcdowall@student.qut.edu.au', 0, 'WOR 2'),
('n8642087', 'JEREMY BRYAN', 'SMITH', 'jeremy.smith@student.qut.edu.au', 0, 'WOR 2'),
('n8646236', 'ZEHUI', 'ZHANG', 'zehui.zhang@student.qut.edu.au', 0, 'WOR 9'),
('n8649341', 'MICHELLE PAULA', 'MARFIGA', 'michellepaula.marfiga@student.qut.edu.au', 1, 'WOR 3'),
('n8655251', 'ABDULRAHMAN TAHER H', 'ALHASHIMI', 'abdulrahmantaherh.alhashimi@student.qut.edu.au', 1, 'WOR 1'),
('n8666229', 'YOW KUEN', 'KAN', 'yowkuen.kan@student.qut.edu.au', 1, 'WOR 1'),
('n8667799', 'GURWINDER SINGH', 'CHAHAL', 'gurwinder.chahal@student.qut.edu.au', 0, 'WOR 3'),
('n8677565', 'YANGYANG', 'XIAO', 'yangyang.xiao@student.qut.edu.au', 1, 'WOR 3'),
('n8682836', 'WEI', 'LIU', 'w25.liu@student.qut.edu.au', 0, 'WOR 7'),
('n8683093', 'DANIEL AVELINO', 'CHAM', 'danielavelino.cham@student.qut.edu.au', 0, 'WOR 3'),
('n8683417', 'PETTER', 'HARSEM', 'petter.harsem@student.qut.edu.au', 0, 'WOR 2'),
('n8686505', 'EMMANUEL ANDREW', 'SAVAGE', 'emmanuel.savage@student.qut.edu.au', 0, 'WOR 7'),
('n8687382', 'SAMUEL JEFFREY', 'BOPF', 'samuel.bopf@student.qut.edu.au', 0, 'WOR 7'),
('n8687447', 'LUKE', 'EDWARDS', 'luke.edwards@student.qut.edu.au', 0, 'WOR 7'),
('n8688478', 'WILLIAM CHARLES', 'JUBB', 'william.jubb@student.qut.edu.au', 0, 'WOR 2'),
('n8689491', 'SAMUEL', 'HILLIER', 'samuel.hillier@student.qut.edu.au', 0, 'WOR 3'),
('n8692700', 'MINJUNG', 'LEE', 'minjung.lee@student.qut.edu.au', 1, 'WOR 3'),
('n8696179', 'XIANG', 'WANG', 'x58.wang@student.qut.edu.au', 1, 'WOR 2'),
('n8701709', 'CHUN HUNG', 'CHUNG', 'chunhung.chung@student.qut.edu.au', 0, 'WOR 5'),
('n8702462', 'CHUN HIN JUSTIN', 'HO', 'chunhinjustin.ho@student.qut.edu.au', 0, 'WOR 8'),
('n8706638', 'CHUN PUI', 'CHAN', 'chunpui.chan@student.qut.edu.au', 0, 'WOR 2'),
('n8710414', 'KIN FU', 'WONG', 'kinfu.wong@student.qut.edu.au', 0, 'WOR 5'),
('n8712719', 'JOSEPH', 'RIZK', 'joseph.rizk@student.qut.edu.au', 0, 'WOR 5'),
('n8718563', 'HUI SHANG', 'KWAN', 'huishang.kwan@student.qut.edu.au', 0, 'WOR 4'),
('n8721424', 'ZHUO', 'CHEN', 'z16.chen@student.qut.edu.au', 1, 'WOR 3'),
('n8722561', 'DIMPLE', 'MAISURIA', 'dimple.maisuria@student.qut.edu.au', 1, 'WOR 2'),
('n8724296', 'SUDARSHAN', 'SUNDER RAJAN', 'sudarshan.sunderrajan@student.qut.edu.au', 1, 'WOR 2'),
('n8724521', 'MURALITHARAN', 'RAMALINGAM', 'muralitharan.ramalingam@student.qut.edu.au', 0, 'WOR 8'),
('n8725578', 'ARMIN', 'KHOSHBIN', 'armin.khoshbin@student.qut.edu.au', 0, 'WOR 3'),
('n8729417', 'SAMSON MAEASI AIHUNU', 'WA''AHERO', 'samsonmaeasiaihunu.waahero@student.qut.edu.au', 0, 'WOR 9'),
('n8735361', 'ABDALLAH SALIM SULAIYAM NASSER', 'AL MAZIDI', 'a.almazidi@student.qut.edu.au', 1, 'WOR 2'),
('n8735751', 'MINGZHUO', 'LIU', 'mingzhuo.liu@student.qut.edu.au', 1, 'WOR 3'),
('n8739030', 'JACK FRANCIS', 'PHILIPPI', 'jack.philippi@student.qut.edu.au', 0, 'WOR 10'),
('n8745013', 'QUANG VU', 'LE', 'quangvu.le@student.qut.edu.au', 1, 'WOR 3'),
('n8766118', 'HENRY ALEXIS EFRAIN', 'ALPACA SALAS', 'henryalexisefrain.alpacasalas@student.qut.edu.au', 1, 'WOR 1'),
('n8769583', 'TING-HOU', 'CHEN', 'tinghou.chen@student.qut.edu.au', 0, 'WOR 5'),
('n8777284', 'DONGMIN', 'LEE', 'dongmin.lee@student.qut.edu.au', 0, 'WOR 10'),
('n8781303', 'XIAOHAN', 'GUO', 'xiaohan.guo@student.qut.edu.au', 1, 'WOR 2'),
('n8781371', 'LUN', 'YAN', 'lun.yan@student.qut.edu.au', 1, 'WOR 3'),
('n8787034', 'YEE HENN JARIC', 'NG', 'yeehennjaric.ng@student.qut.edu.au', 0, 'WOR 2'),
('n8787280', '', 'WIN MIN PHYO', 'winminphyo@student.qut.edu.au', 0, 'WOR 5'),
('n8788511', 'MOHAMMAD DAWOOD', 'KARIMI', 'mohammaddawood.karimi@student.qut.edu.au', 1, 'WOR 3'),
('n8789584', 'NISHANTH', 'SANKAR', 'nishanth.sankar@student.qut.edu.au', 1, 'WOR 2'),
('n8791040', 'TAKESHI', 'USUDA', 'takeshi.usuda@student.qut.edu.au', 1, 'WOR 3'),
('n8794707', 'FRANK BENNO', 'HART', 'frank.hart@student.qut.edu.au', 0, 'WOR 8'),
('n8795428', 'KIRA PETROVNA', 'JAMISON', 'kira.jamison@student.qut.edu.au', 0, 'WOR 2'),
('n8796181', 'ZACHARY', 'BARCLAY', 'zachary.barclay@student.qut.edu.au', 0, 'WOR 10'),
('n8796441', 'BENJAMIN GASPERE', 'CURCIO', 'benjamin.curcio@student.qut.edu.au', 0, 'WOR 7'),
('n8797676', 'SHANE MICHAEL', 'JACKSON', 'shane.jackson@student.qut.edu.au', 0, 'WOR 3'),
('n8798885', 'ASHLEY REX', 'MCVEIGH', 'ashley.mcveigh@student.qut.edu.au', 0, 'WOR 3'),
('n8799458', 'PATRICK STEPHEN', 'BYRNE', 'patrick.byrne@student.qut.edu.au', 0, 'WOR 9'),
('n8801096', 'ERIK', 'EDELING', 'erik.edeling@student.qut.edu.au', 0, 'WOR 8'),
('n8801444', 'JONATHON', 'STUART', 'jonathon.stuart@student.qut.edu.au', 0, 'WOR 8'),
('n8801550', 'CHRISTOPHER JAMES', 'ADAMS', 'c13.adams@student.qut.edu.au', 0, 'WOR 4'),
('n8802475', 'GRACE LEIA', 'SHAW', 'grace.shaw@student.qut.edu.au', 0, 'WOR 4'),
('n8803609', 'PHILLIP JAMES', 'SAFFIOTI', 'phillip.saffioti@student.qut.edu.au', 0, 'WOR 1'),
('n8804265', 'OWEN VAUGHAN', 'COLE', 'ov.cole@student.qut.edu.au', 0, 'WOR 5'),
('n8804869', 'DAVID THOMAS', 'KEMPE', 'david.kempe@student.qut.edu.au', 0, 'WOR 10'),
('n8806756', 'BENJAMIN JAMES', 'BUTTENSHAW', 'benjamin.buttenshaw@student.qut.edu.au', 0, 'WOR 1'),
('n8809887', 'PENG', 'LIU', 'p14.liu@student.qut.edu.au', 1, 'WOR 2'),
('n8811229', 'SAMUEL JAMES', 'HAMMILL', 'samuel.hammill@student.qut.edu.au', 0, 'WOR 8'),
('n8811431', 'JASON MATTHEW', 'NOBLE', 'jason.noble@student.qut.edu.au', 0, 'WOR 2'),
('n8818452', 'CHUAN', 'LI', 'c58.li@student.qut.edu.au', 1, 'WOR 1'),
('n8819912', 'PREETHAM', 'ASHOK KUMAR', 'preetham.ashokkumar@student.qut.edu.au', 1, 'WOR 1'),
('n8820945', 'CAMERON RYAN', 'MCCLOY', 'cameron.mccloy@student.qut.edu.au', 0, 'WOR 9'),
('n8821623', 'MUHAMMAD MUSLIM NABEEL', 'AWAN', 'm.awan@student.qut.edu.au', 1, 'WOR 2'),
('n8824037', 'ALYKSANDR CASEY', 'SKYE', 'alyksandr.skye@student.qut.edu.au', 0, 'WOR 5'),
('n8824657', 'JORDAN GARETH', 'REES', 'jordan.rees@student.qut.edu.au', 0, 'WOR 9'),
('n8828041', 'JIEUN', 'JANG', 'jieun.jang@student.qut.edu.au', 0, 'WOR 5'),
('n8829519', 'JACK ANTHONY', 'EATON', 'jack.eaton@student.qut.edu.au', 0, 'WOR 7'),
('n8831009', 'VICTOR ALEXANDER', 'FOGGON', 'victor.foggon@student.qut.edu.au', 0, 'WOR 1'),
('n8831556', 'TIMOTHY MICHAEL', 'VIZE', 'timothy.vize@student.qut.edu.au', 0, 'WOR 8'),
('n8832382', 'THOMAS MICHAEL', 'KIRKE', 'thomas.kirke@student.qut.edu.au', 0, 'WOR 8'),
('n8835462', 'THOMAS CHARLES', 'PONTING', 'thomas.ponting@student.qut.edu.au', 0, 'WOR 1'),
('n8835683', 'STEVEN JOHN', 'BERKA', 'steven.berka@student.qut.edu.au', 0, 'WOR 4'),
('n8837601', 'MONIKA', 'SYKTUS', 'monika.syktus@student.qut.edu.au', 0, 'WOR 4'),
('n8838071', 'KAIN CHUNG - SENG', 'WONG', 'kain.wong@student.qut.edu.au', 0, 'WOR 8'),
('n8839417', 'ADAM HEATH', 'SHUR', 'adam.shur@student.qut.edu.au', 0, 'WOR 7'),
('n8840091', 'LUKE JAMMIN', 'D''ROZARIO', 'luke.drozario@student.qut.edu.au', 0, 'WOR 7'),
('n8840725', 'MITCHELL JAMES', 'PERRY', 'm7.perry@student.qut.edu.au', 0, 'WOR 10'),
('n8841811', 'REBECCA LEE', 'RUHLE', 'rebecca.ruhle@student.qut.edu.au', 0, 'WOR 8'),
('n8844810', 'WILLIAM ROLAND', 'JAKOBS', 'william.jakobs@student.qut.edu.au', 0, 'WOR 4'),
('n8847410', 'OLIVIA ASHLEIGH', 'WARD', 'olivia.ward@student.qut.edu.au', 0, 'WOR 2'),
('n8847487', 'NATHAN JAMES', 'NORRIS', 'nathan.norris@student.qut.edu.au', 0, 'WOR 8'),
('n8849269', 'CHRISTOPHER MICHAEL JOHN', 'LITHERLAND', 'christopher.litherland@student.qut.edu.au', 0, 'WOR 3'),
('n8850330', 'BENJAMIN DIRK', 'FORD', 'bd.ford@student.qut.edu.au', 0, 'WOR 3'),
('n8850798', 'JORDAN TAYLOR', 'PEARCE', 'jordan.pearce@student.qut.edu.au', 0, 'WOR 8'),
('n8853312', 'JAMIE KENDALL', 'CARR', 'jamie.carr@student.qut.edu.au', 0, 'WOR 2'),
('n8853673', 'JAMIE WARREN', 'NEVIN', 'jamie.nevin@student.qut.edu.au', 0, 'WOR 2'),
('n8854114', 'KRISTEN GERARDAGH', 'O''FARRELL', 'kristen.ofarrell@student.qut.edu.au', 0, 'WOR 3'),
('n8854751', 'LUCY KATE', 'MACGREGOR', 'lucy.macgregor@student.qut.edu.au', 0, 'WOR 7'),
('n8855013', 'DAMIN CHRISTOPHER JAY', 'SCHAFFERIUS', 'damin.schafferius@student.qut.edu.au', 0, 'WOR 4'),
('n8855315', 'JOSHUA', 'JOHNSTON', 'j10.johnston@student.qut.edu.au', 0, 'WOR 10'),
('n8856567', 'NATHANIEL MICHAEL', 'JONES', 'nathaniel.jones@student.qut.edu.au', 0, 'WOR 9'),
('n8857580', 'HARRISEN PAUL', 'SCELLS', 'harrisen.scells@student.qut.edu.au', 0, 'WOR 3'),
('n8857954', 'DAMON COLIN', 'JONES', 'd72.jones@student.qut.edu.au', 0, 'WOR 10'),
('n8858594', 'JOSHUA ERIC', 'HENLEY', 'joshua.henley@student.qut.edu.au', 0, 'WOR 10'),
('n8860114', 'BRADLEY BARTON', 'SHEPHERD', 'bradley.shepherd@student.qut.edu.au', 0, 'WOR 2'),
('n8862079', 'NATHAN JOHN', 'COLLINS', 'n4.collins@student.qut.edu.au', 0, 'WOR 2'),
('n8862699', 'LACHLAN CIAN', 'SPENCER', 'lc.spencer@student.qut.edu.au', 0, 'WOR 10'),
('n8862877', 'STEVEN JAMES', 'HOLLEY', 'steven.holley@student.qut.edu.au', 0, 'WOR 2'),
('n8862923', 'WILLIAM HENRY', 'HACKETT', 'william.hackett@student.qut.edu.au', 0, 'WOR 5'),
('n8862931', 'JAMES PHILLIP', 'MAKEPEACE', 'james.makepeace@student.qut.edu.au', 0, 'WOR 1'),
('n8863458', 'MATTHEW PAUL', 'DOWNS', 'matthew.downs@student.qut.edu.au', 0, 'WOR 9'),
('n8863628', 'YSABELLE DEIRDRE', 'MELLAM', 'ysabelle.mellam@student.qut.edu.au', 0, 'WOR 8'),
('n8864560', 'ANGER MAJOK MAC RACH', 'PAC', 'anger.pac@student.qut.edu.au', 0, 'WOR 6'),
('n8865141', 'ZACH JUSTIN', 'GARNER', 'zach.garner@student.qut.edu.au', 0, 'WOR 7'),
('n8865361', 'JACOB ROBERT', 'WAUCHOPE', 'jacob.wauchope@student.qut.edu.au', 0, 'WOR 2'),
('n8865876', 'MATTHEW JAMES', 'VAN DER BOOR', 'matthew.vanderboor@student.qut.edu.au', 0, 'WOR 2'),
('n8866198', 'ANTHONY SERGIO', 'HORWOOD', 'anthony.horwood@student.qut.edu.au', 0, 'WOR 7'),
('n8867585', 'MATTHEW BRENDAN', 'KNIGHT', 'mb.knight@student.qut.edu.au', 0, 'WOR 3'),
('n8867968', 'CHRISTOPHER', 'WEIR', 'christopher.weir@student.qut.edu.au', 0, 'WOR 8'),
('n8868344', 'JACK BOYDE', 'REBETZKE', 'jack.rebetzke@student.qut.edu.au', 0, 'WOR 1'),
('n8872465', 'NEIL', 'MATHUR', 'neil.mathur@student.qut.edu.au', 0, 'WOR 1'),
('n8874182', 'DANIELLE NADINE', 'VAZ', 'danielle.vaz@student.qut.edu.au', 0, 'WOR 6'),
('n8875855', 'ALEC MICHAEL', 'TUTIN', 'alec.tutin@student.qut.edu.au', 0, 'WOR 4'),
('n8877092', 'ASHLEY GRAHAM', 'MALETZ', 'ashley.maletz@student.qut.edu.au', 0, 'WOR 1'),
('n8877131', 'STEFAN IVAN', 'KUJUNDZIC', 'stefan.kujundzic@student.qut.edu.au', 0, 'WOR 9'),
('n8878722', 'JOANNE KARIN', 'BOYD', 'joanne.boyd@student.qut.edu.au', 0, 'WOR 7'),
('n8880620', 'MICHAEL JOHN', 'WHALE', 'michael.whale@student.qut.edu.au', 0, 'WOR 1'),
('n8881588', 'RICHARD DAVID', 'WILTSHIRE', 'richard.wiltshire@student.qut.edu.au', 0, 'WOR 4'),
('n8882185', 'HARRISON MATTHEW', 'BATH', 'harrison.bath@student.qut.edu.au', 0, 'WOR 3'),
('n8882398', 'NATHAN ANDREW', 'ZERK', 'nathan.zerk@student.qut.edu.au', 0, 'WOR 3'),
('n8882720', 'OSCAR HARVEY', 'FERNE', 'oscar.ferne@student.qut.edu.au', 0, 'WOR 1'),
('n8883378', 'JACK RYAN', 'BYRNE', 'jr.byrne@student.qut.edu.au', 0, 'WOR 9'),
('n8887373', 'MORGAN ALEXANDER', 'MACKAY - PAYNE', 'morgan.mackaypayne@student.qut.edu.au', 0, 'WOR 3'),
('n8887918', 'ALBERT', 'LEUNG', 'albert.leung@student.qut.edu.au', 0, 'WOR 3'),
('n8888141', 'JAMES ROBERT', 'CLELLAND', 'james.clelland@student.qut.edu.au', 0, 'WOR 2'),
('n8888205', 'PETER', 'COLEMAN', 'peter.coleman@student.qut.edu.au', 0, 'WOR 1'),
('n8888817', 'HARVEY RYAN', 'MEALE', 'harvey.meale@student.qut.edu.au', 0, 'WOR 1'),
('n8893381', 'ANDREW JOHN', 'WILLIAMS', 'a69.williams@student.qut.edu.au', 0, 'WOR 10'),
('n8893900', 'ALVIN', 'MALUTO', 'alvin.maluto@student.qut.edu.au', 0, 'WOR 1'),
('n8894001', 'SARAH ANNE', 'HORSUP', 'sarah.horsup@student.qut.edu.au', 0, 'WOR 6'),
('n8894108', 'ALEXANDER JOHN', 'HOWARD', 'alexander.howard@student.qut.edu.au', 0, 'WOR 7'),
('n8898642', 'CLEARENCE LIZANDRO', 'WISSAR DOMINGUEZ', 'c.wissardominguez@student.qut.edu.au', 0, 'WOR 3'),
('n8908672', 'ZACHARY GEORGE', 'BOWMAN', 'zachary.bowman@student.qut.edu.au', 0, 'WOR 2'),
('n8909920', 'PETER MARIO', 'RONCHESE', 'peter.ronchese@student.qut.edu.au', 0, 'WOR 10'),
('n8910057', 'JAMES NICHOLAS', 'FARDOULYS', 'james.fardoulys@student.qut.edu.au', 0, 'WOR 6'),
('n8910570', 'JAMES EDWARD', 'DOIDGE', 'james.doidge@student.qut.edu.au', 0, 'WOR 4'),
('n8910723', 'EHREN CLEMENT', 'HYDE', 'ehren.hyde@student.qut.edu.au', 0, 'WOR 8'),
('n8911517', 'BO', 'SUN', 'bo.sun@student.qut.edu.au', 0, 'WOR 1'),
('n8915539', 'SEEMA', 'BHARTI', 'seema.bharti@student.qut.edu.au', 1, ''),
('n8916829', 'SARVESH SUNIL', 'GUNDA', 'sarveshsunil.gunda@student.qut.edu.au', 1, 'WOR 3'),
('n8922977', 'ASHWIN', 'SONALE', 'ashwin.sonale@student.qut.edu.au', 1, 'WOR 2'),
('n8923078', 'SOON KOK', 'KOAY', 'soonkok.koay@student.qut.edu.au', 0, 'WOR 8'),
('n8924228', 'KIM MELGAR', 'DE CHAVEZ', 'kimmelgar.dechavez@student.qut.edu.au', 0, 'WOR 8'),
('n8931062', 'VENKATA VANI SRI VAISHNAVI', 'MADDI', 'venkatavanisrivaishnavi.maddi@student.qut.edu.au', 0, 'WOR 2'),
('n8935254', 'PEIR GEN', 'AW', 'peirgen.aw@student.qut.edu.au', 0, 'WOR 8'),
('n8938466', 'HALLDOR ANDRI', 'HALLDORSSON', 'jagdeepsingh@student.qut.edu.au', 1, 'WOR 3'),
('n8938971', 'LIANGBIN', 'CHEN', 'liangbin.chen@student.qut.edu.au', 0, 'WOR 7'),
('n8940215', 'DAVIT', 'SANPOTE', 'davit.sanpote@student.qut.edu.au', 1, 'WOR 3'),
('n8941777', 'MATTHEW DAVID', 'ALCOCK', 'matthew.alcock@student.qut.edu.au', 0, 'WOR 7'),
('n8942170', 'ADAM NIGEL', 'BALLARIN', 'adam.ballarin@student.qut.edu.au', 0, 'WOR 7'),
('n8943290', 'KOU CHENG', 'FAN', 'koucheng.fan@student.qut.edu.au', 0, 'WOR 5'),
('n8945241', 'GLENN', 'BJOERLO', 'glenn.bjoerlo@student.qut.edu.au', 0, 'WOR 5'),
('n8948798', 'ASHWIN RAVAL KAREGUDDA', 'PATIL', 'ashwin.patil@student.qut.edu.au', 1, 'WOR 3'),
('n8952361', 'LUKE DANIEL', 'NICHOLS', 'luke.nichols@student.qut.edu.au', 0, 'WOR 3'),
('n8953619', 'SAHIL', 'BAWEJA', 'sahil.baweja@student.qut.edu.au', 1, 'WOR 3'),
('n8954470', 'ROMAN', 'YAGOVKIN', 'roman.yagovkin@student.qut.edu.au', 1, 'WOR 2'),
('n8958718', 'GRIGORY', 'PAVLOV', 'grigory.pavlov@student.qut.edu.au', 0, 'WOR 7'),
('n8960674', 'JAMES LEIGH', 'HALL', 'j48.hall@student.qut.edu.au', 0, 'WOR 3'),
('n8961212', 'DANIEL JOHN', 'MCLELLAN', 'daniel.mclellan@student.qut.edu.au', 0, 'WOR 5'),
('n8961221', 'HOANG CAO MINH DAVID', 'NGUYEN', 'h98.nguyen@student.qut.edu.au', 0, 'WOR 9'),
('n8961565', 'LEAH', 'CARROLL', 'leah.carroll@student.qut.edu.au', 0, 'WOR 8'),
('n8962367', 'ADAM SEBASTIAN', 'BLYDE', 'adam.blyde@student.qut.edu.au', 0, 'WOR 3'),
('n8962375', 'OWEN ASHLEY', 'CHARTERS', 'owen.charters@student.qut.edu.au', 0, 'WOR 9'),
('n8962481', 'BENJAMIN HAMMOND', 'COOK', 'bh.cook@student.qut.edu.au', 0, 'WOR 5'),
('n8962588', 'NATHAN CHRISTOPHER', 'WARD', 'nc.ward@student.qut.edu.au', 0, 'WOR 8'),
('n8968632', 'MATTHEW', 'BRENNAN', 'matthew.brennan@student.qut.edu.au', 1, 'WOR 1'),
('n8969507', 'GEETANJALI', 'BHATT', 'geetanjali.bhatt@student.qut.edu.au', 1, 'WOR 1'),
('n8975469', 'ANITA ROSE', 'DIAMOND', 'anita.diamond@student.qut.edu.au', 1, 'WOR 1'),
('n8975698', 'ZUTAO', 'WU', 'zutao.wu@student.qut.edu.au', 1, 'WOR 1'),
('n8977330', 'DIXITGIRI', 'GOSWAMI', 'dixitgiri.goswami@student.qut.edu.au', 1, 'WOR 1'),
('n8984611', 'SHAH SAMIULLAH HUSSAINI', 'SYED', 'shahsamiullahhussaini.syed@student.qut.edu.au', 1, 'WOR 2'),
('n8990786', 'QIANGQIANG', 'DUAN', 'qiangqiang.duan@student.qut.edu.au', 1, 'WOR 1'),
('n8999848', 'ASHLEY DAVID', 'DUNBABIN', 'ashley.dunbabin@student.qut.edu.au', 1, 'WOR 3'),
('n9000305', 'DEREK RONALD', 'MCGREGOR', 'derek.mcgregor@student.qut.edu.au', 1, 'WOR 3'),
('n9014292', 'MARGOT LOUISE', 'GREENHALGH', 'margot.greenhalgh@student.qut.edu.au', 0, 'WOR 7'),
('n9019286', 'LEONARD', 'SEBASTIAN WIYADHARMA', 'leonard.sebastianwiyadharma@student.qut.edu.au', 1, 'WOR 3'),
('n9031227', 'SAHIL', 'JAKHAR', 'sahil.jakhar@student.qut.edu.au', 1, 'WOR 3'),
('n9037748', 'SYED MOHAMMAD WAHEED', 'AHMAD', 's4.ahmad@student.qut.edu.au', 1, 'WOR 2'),
('n9065041', 'PRASAD', 'PRASANNA KUMARI SASIKUMAR', 'prasad.sasikumar@student.qut.edu.au', 1, 'WOR 1'),
('n9065458', 'SAURAV', 'KHADKA', 'saurav.khadka@student.qut.edu.au', 1, 'WOR 3'),
('n9077588', '', 'GARIMA', 'garima@student.qut.edu.au', 1, 'WOR 1'),
('n9107584', 'NATHAN SAMUEL', 'HAYWARD', 'nathan.hayward@student.qut.edu.au', 0, 'WOR 4'),
('n9123091', 'CHAO', 'XU', 'c21.xu@student.qut.edu.au', 1, 'WOR 1'),
('n9127470', 'PUNEETH MANJUNATH', 'BEDRE', 'puneeth.bedre@student.qut.edu.au', 1, 'WOR 2'),
('n9128379', 'ZUBIN', 'PIYA', 'zubin.piya@student.qut.edu.au', 1, 'WOR 2'),
('n9131663', 'GHADEER SALIM AWADH', 'AL NAJAR', 'ghadeersalimawadh.alnajar@student.qut.edu.au', 1, 'WOR 1'),
('n9152962', 'TIMOTHY RONALD', 'HAWKINS', 'timothy.hawkins@student.qut.edu.au', 1, 'WOR 2'),
('n9200321', 'JASON CHIEN CHENG', 'CHEN', 'j205.chen@student.qut.edu.au', 1, 'WOR 1'),
('n9200398', 'KIPUM', 'LEE', 'kipum.lee@student.qut.edu.au', 1, 'WOR 1'),
('n9205624', 'TARA MICHELLE', 'CAPEL', 'tara.capel@student.qut.edu.au', 1, 'WOR 3'),
('n9207503', 'LINH LEILA', 'ESMAEL', 'linh.esmael@student.qut.edu.au', 1, 'WOR 2');

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

CREATE TABLE IF NOT EXISTS `submissions` (
  `submissionId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `studentId` varchar(10) NOT NULL,
  `assignmentId` int(11) NOT NULL,
  `dateSubmitted` bigint(20) unsigned NOT NULL,
  `assignedTo` mediumint(8) unsigned NOT NULL,
  `marks` double DEFAULT NULL,
  `feedback` text NOT NULL,
  PRIMARY KEY (`submissionId`),
  KEY `assignmentId` (`assignmentId`),
  KEY `studentId` (`studentId`),
  KEY `assignedTo` (`assignedTo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `submissions`
--

INSERT INTO `submissions` (`submissionId`, `studentId`, `assignmentId`, `dateSubmitted`, `assignedTo`, `marks`, `feedback`) VALUES
(1, '1', 1, 1, 1, NULL, ''),
(2, '2', 1, 1, 1, NULL, ''),
(3, '1', 2, 1, 1, NULL, ''),
(4, '2', 2, 1, 1, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `submission_files`
--

CREATE TABLE IF NOT EXISTS `submission_files` (
  `fileId` mediumint(8) unsigned NOT NULL,
  `submissionId` int(10) unsigned NOT NULL,
  `path` text NOT NULL,
  `dateModified` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`fileId`),
  KEY `submissionId` (`submissionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `submission_marking`
--

CREATE TABLE IF NOT EXISTS `submission_marking` (
  `submissionId` mediumint(8) unsigned NOT NULL,
  `dateMarked` bigint(20) unsigned NOT NULL,
  `markerId` mediumint(8) unsigned NOT NULL,
  `marks` double DEFAULT NULL,
  `feedback` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`submissionId`,`dateMarked`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `submission_marking`
--

INSERT INTO `submission_marking` (`submissionId`, `dateMarked`, `markerId`, `marks`, `feedback`) VALUES
(1, 1, 1, NULL, ''),
(1, 2, 1, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(128) COLLATE utf8_bin NOT NULL,
  `salt` varchar(40) COLLATE utf8_bin NOT NULL,
  `realName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `email`, `password`, `salt`, `realName`) VALUES
(1, 'nobody@localhost', '3deef4ccfe133c50704434d876766dbc8b1d479b15144c66cd0246fdee78c21c53cb832e372266702c4f156b743a33a849c285cf6661fb16d91e9a1ca39d51e1', 'b03863f12c7673f51f626665f8f780742a052114', 'Nobody User'),
(2, 'tonyqvc@live.com', 'f744d4e1c1b9757bf0c7285f7bcebb81c32655738219e2320077f30edfd9bde3f8b9581a2c04479564405ee0e8f977770c2dd897b9ec36ddf82b54df66c4945b', '3da541559918a808c2402bba5012f6c60b27661c', 'Tony Chau');

-- --------------------------------------------------------

--
-- Table structure for table `users_sessions`
--

CREATE TABLE IF NOT EXISTS `users_sessions` (
  `sessionId` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` mediumint(8) unsigned NOT NULL,
  `sessionKey` varchar(64) COLLATE utf8_bin NOT NULL,
  `sessionStart` bigint(20) unsigned NOT NULL,
  `expiry` bigint(20) unsigned NOT NULL,
  `userAgent` varchar(255) COLLATE utf8_bin NOT NULL,
  `ipAddress` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`sessionId`,`userId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users_sessions`
--

INSERT INTO `users_sessions` (`sessionId`, `userId`, `sessionKey`, `sessionStart`, `expiry`, `userAgent`, `ipAddress`) VALUES
('016c8e2c-09e9-4a29-b2b0-1b2559be7ea2', 2, '54771dae5520e423e5b080d07e3a898c3f098271', 1393853392, 1393874992, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36', '::1'),
('0b04df46-ac1a-4293-814c-4408753acd07', 2, 'f49533d9ce32e4ded68af19c141eb45947ed707e', 1394673082, 1394694682, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.149 Safari/537.36', '::1'),
('18790dcb-fedd-4162-9872-153a95a42092', 2, '04fb7806a02d6c27a647398a2f2b1c8574102741', 1394633526, 1394655126, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.149 Safari/537.36', '::1'),
('389381e0-e245-499b-be63-4b8a4e772148', 2, 'ce3f428a7abea69dcffc2e6cb76756c73786551a', 1392906421, 1392910021, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('4bbfcee0-0b03-4b4b-bc59-7659bf8fb2b6', 2, '4b05f61db05aa0a9607c168372d2e799994d1c7e', 1393206428, 1393228028, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36', '::1'),
('95ae493e-6cb3-4fea-ba63-8cf1bb787f0e', 2, '1f052c0c3c0cf073a40248fb403b8205e282b71e', 1337052234, 1337138634, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', '::1'),
('b62d1596-414e-4647-8041-16f194628d79', 2, '18a5e78907c3cf9bbbb632f9de7bb43d31ed377d', 1394197171, 1394218771, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36', '::1'),
('e14378af-3e8b-4ad0-a619-7ee6e907d70e', 2, 'b27612665458adbe88f7d17a9a6eef3c06928d4d', 1392906975, 1392928575, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('ebd336f6-8f38-46cd-a1f6-2c839e47a982', 2, '0695b91ba30c77e127ff753ca9dad58f0040d6f4', 1392906485, 1392903304, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('ec62fa14-6cb0-4e40-8533-cf4f417f0586', 2, '09034e2d13c78aecdde7f030d8e8c0cb2ad97d77', 1392906415, 1392910015, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('eca78566-4852-4e4b-8117-f53520ac99dc', 2, '32bf517be0f0227d01930b4531a742b9d3252ec1', 1394671257, 1394669288, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.149 Safari/537.36', '::1'),
('ed0741fb-dc79-406b-a89e-f66e08c3b078', 2, '1751e96993b78fb4f441559d5c90773781a0f66e', 1393904740, 1393926340, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36', '::1'),
('f7851f0a-8f00-49a0-a97a-d55a1c311bf0', 2, '700bcf312b144ecb2b0422961b7038e9953e70b6', 1394419468, 1394416351, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `users_settings`
--

CREATE TABLE IF NOT EXISTS `users_settings` (
  `userId` mediumint(8) unsigned NOT NULL,
  `settingName` varchar(64) COLLATE utf8_bin NOT NULL,
  `settingValue` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`,`settingName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users_states`
--

CREATE TABLE IF NOT EXISTS `users_states` (
  `userId` mediumint(8) unsigned NOT NULL,
  `stateName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`,`stateName`),
  KEY `stateName` (`stateName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users_states`
--

INSERT INTO `users_states` (`userId`, `stateName`) VALUES
(1, 'disableUserSystem'),
(2, 'specialUserAdmin');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_sessions`
--
ALTER TABLE `users_sessions`
  ADD CONSTRAINT `users_sessions_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_settings`
--
ALTER TABLE `users_settings`
  ADD CONSTRAINT `users_settings_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_states`
--
ALTER TABLE `users_states`
  ADD CONSTRAINT `users_states_ibfk_1` FOREIGN KEY (`stateName`) REFERENCES `states` (`stateName`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_states_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
