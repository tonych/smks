-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 21, 2014 at 11:38 AM
-- Server version: 5.6.16
-- PHP Version: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smks`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE IF NOT EXISTS `assignments` (
  `assignmentId` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `maxMarks` double NOT NULL,
  `dueDate` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`assignmentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `groupId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `groupName` (`groupName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`setting_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_name`, `value`) VALUES
('smks/gracePeriod', '3600');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `stateName` varchar(64) COLLATE utf8_bin NOT NULL,
  `isGroupState` tinyint(1) NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`stateName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`stateName`, `isGroupState`, `description`) VALUES
('disableGroupSystem', 1, 'Do not DELETE THIS GROUP! System Group.'),
('disableUserDisabled', 0, 'This state is given to users who have been disabled.'),
('disableUserSystem', 0, 'Do not DELETE THIS USER! System User.'),
('specialGroupEveryone', 1, 'This is the Everyone Group. It represents Everyone.'),
('specialGroupFriends', 1, 'This is the Friends Group. It represents all your Friends (those you have added to your groups). '),
('specialUserAdmin', 0, 'This is an Admin User.'),
('specialUserEveryone', 0, 'This is the Everyone User. It represents everyone.'),
('specialUserFriends', 0, 'This is the Friends User. It represents your friends. ');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `studentId` varchar(10) CHARACTER SET latin1 NOT NULL,
  `firstName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `lastName` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`studentId`),
  KEY `firstName` (`firstName`,`lastName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(40) COLLATE utf8_bin NOT NULL,
  `salt` varchar(40) COLLATE utf8_bin NOT NULL,
  `realName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `email`, `password`, `salt`, `realName`) VALUES
(1, 'nobody@localhost', 'd186e8dac48a24d0115b568d0ab2c9e8b82e6adb', 'b03863f12c7673f51f626665f8f780742a052114', 'Nobody User'),
(2, 'tonyqvc@live.com', 'f581d3e82ceada1c111ab1a5c05e24fc196474c7', '3da541559918a808c2402bba5012f6c60b27661c', 'Tony Chau');

-- --------------------------------------------------------

--
-- Table structure for table `users_sessions`
--

CREATE TABLE IF NOT EXISTS `users_sessions` (
  `sessionId` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` mediumint(8) unsigned NOT NULL,
  `sessionKey` varchar(64) COLLATE utf8_bin NOT NULL,
  `sessionStart` bigint(20) unsigned NOT NULL,
  `expiry` bigint(20) unsigned NOT NULL,
  `userAgent` varchar(255) COLLATE utf8_bin NOT NULL,
  `ipAddress` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`sessionId`,`userId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users_sessions`
--

INSERT INTO `users_sessions` (`sessionId`, `userId`, `sessionKey`, `sessionStart`, `expiry`, `userAgent`, `ipAddress`) VALUES
('389381e0-e245-499b-be63-4b8a4e772148', 2, 'ce3f428a7abea69dcffc2e6cb76756c73786551a', 1392906421, 1392910021, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('95ae493e-6cb3-4fea-ba63-8cf1bb787f0e', 2, '1f052c0c3c0cf073a40248fb403b8205e282b71e', 1337052234, 1337138634, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', '::1'),
('e14378af-3e8b-4ad0-a619-7ee6e907d70e', 2, 'b27612665458adbe88f7d17a9a6eef3c06928d4d', 1392906975, 1392928575, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('ebd336f6-8f38-46cd-a1f6-2c839e47a982', 2, '0695b91ba30c77e127ff753ca9dad58f0040d6f4', 1392906485, 1392903304, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('ec62fa14-6cb0-4e40-8533-cf4f417f0586', 2, '09034e2d13c78aecdde7f030d8e8c0cb2ad97d77', 1392906415, 1392910015, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `users_settings`
--

CREATE TABLE IF NOT EXISTS `users_settings` (
  `userId` mediumint(8) unsigned NOT NULL,
  `settingName` varchar(64) COLLATE utf8_bin NOT NULL,
  `settingValue` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`,`settingName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users_states`
--

CREATE TABLE IF NOT EXISTS `users_states` (
  `userId` mediumint(8) unsigned NOT NULL,
  `stateName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`,`stateName`),
  KEY `stateName` (`stateName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users_states`
--

INSERT INTO `users_states` (`userId`, `stateName`) VALUES
(1, 'disableUserSystem'),
(2, 'specialUserAdmin');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_sessions`
--
ALTER TABLE `users_sessions`
  ADD CONSTRAINT `users_sessions_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_settings`
--
ALTER TABLE `users_settings`
  ADD CONSTRAINT `users_settings_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_states`
--
ALTER TABLE `users_states`
  ADD CONSTRAINT `users_states_ibfk_1` FOREIGN KEY (`stateName`) REFERENCES `states` (`stateName`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_states_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
