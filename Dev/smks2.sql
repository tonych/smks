-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 07, 2014 at 09:58 PM
-- Server version: 5.6.16
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smks`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE IF NOT EXISTS `assignments` (
  `assignmentId` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `maxMarks` double NOT NULL,
  `dueDate` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`assignmentId`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`assignmentId`, `name`, `maxMarks`, `dueDate`) VALUES
(467606, 'Assignment 1 - Web Design ', 25, 0),
(470035, 'Assignment 2 - Dynamic web site implementation ', 30, 0),
(490396, 'Final Exam Part A ', 20, 0),
(490397, 'Final Exam Part B ', 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `groupId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `groupName` (`groupName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`setting_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_name`, `value`) VALUES
('smks/gracePeriod', '3600');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `stateName` varchar(64) COLLATE utf8_bin NOT NULL,
  `isGroupState` tinyint(1) NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`stateName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`stateName`, `isGroupState`, `description`) VALUES
('disableGroupSystem', 1, 'Do not DELETE THIS GROUP! System Group.'),
('disableUserDisabled', 0, 'This state is given to users who have been disabled.'),
('disableUserSystem', 0, 'Do not DELETE THIS USER! System User.'),
('specialGroupEveryone', 1, 'This is the Everyone Group. It represents Everyone.'),
('specialGroupFriends', 1, 'This is the Friends Group. It represents all your Friends (those you have added to your groups). '),
('specialUserAdmin', 0, 'This is an Admin User.'),
('specialUserEveryone', 0, 'This is the Everyone User. It represents everyone.'),
('specialUserFriends', 0, 'This is the Friends User. It represents your friends. ');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `studentId` varchar(10) CHARACTER SET latin1 NOT NULL,
  `firstName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `lastName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `postgraduate` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`studentId`),
  KEY `firstName` (`firstName`,`lastName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`studentId`, `firstName`, `lastName`, `postgraduate`) VALUES
('n0259373', 'Michael', 'Sachs', 0),
('n1860917', 'Rachelle', 'Lequesne', 0),
('n1936182', 'Slawomir', 'Swiac', 0),
('n2728834', 'Wendy', 'Gorsuch', 0),
('n4089022', 'Mike', 'Walton', 0),
('n4416937', 'Trevor', 'Gudenswager', 0),
('n4481585', 'Megan', 'Lewis', 0),
('n5114519', 'Chien Cheng', 'Pan', 0),
('n6070566', 'Matthew', 'Turner', 0),
('n6375138', 'Nicholas', 'Wood', 0),
('n6535488', 'Phanupong', 'Phadungkiat', 0),
('n6749283', 'Quan', 'Chen', 0),
('n6821693', 'Matthew', 'Wardle', 0),
('n6889123', 'Daniel', 'Kim', 0),
('n6893392', 'Simon', 'Jordan', 0),
('n6903444', 'Geoffrey', 'Polzin', 0),
('n6922988', 'Kao-I', 'Yeh', 0),
('n7163002', 'David', 'Sharpe', 0),
('n7177054', 'Aaron', 'Williams', 0),
('n7195508', 'Thuan', 'Nguyen', 0),
('n7212224', 'Luke', 'Goeree', 0),
('n7219237', 'James', 'Peterson', 0),
('n7227558', 'Matthew', 'Lane', 0),
('n7229666', 'Mohammed Saeed', 'Alkadi', 0),
('n7244436', 'Zhonghao', 'Liang', 0),
('n7245327', 'Jia Ling', 'Chen', 0),
('n7256639', 'Liang', 'Wu', 0),
('n7322127', 'Jonathan', 'Kennedy', 0),
('n7328567', 'Xin', 'Liu', 0),
('n7343345', 'Julian', 'Low', 0),
('n7358504', 'Michelle', 'Lisik', 0),
('n7373988', 'Tom', 'Parker', 0),
('n7377495', 'Luke Thomas', 'Durkan', 0),
('n7382464', 'Nan', 'Chen', 0),
('n7382545', 'Chun Wei', 'Cheng', 0),
('n7425406', 'Samuel', 'Burns', 0),
('n7431775', 'Michael', 'Swarbrick', 0),
('n7461054', 'Antoni', 'Sukic', 0),
('n7464363', 'Luis', 'Aguirre Orozco', 0),
('n7468679', 'Timothy', 'McIntyre', 0),
('n7482655', 'Nicholas', 'Semple', 0),
('n7500289', 'Mischa', 'Colley', 0),
('n7520018', 'Dylan', 'Court', 0),
('n7524838', 'Storm', 'Saich', 0),
('n7535945', 'Nathan', 'Wong', 0),
('n7550774', 'Peter', 'De Lacy', 0),
('n7563647', 'Matthew', 'Stephenson', 0),
('n7570104', 'Douglas', 'Campbell', 0),
('n7576307', 'Yao', 'Wang', 0),
('n7622236', 'Shu Hung', 'Chou', 0),
('n7630115', 'Jie', 'Wu', 0),
('n7656696', 'Joyce', 'Kanofski', 0),
('n7680678', 'Ziyao', 'Tang', 0),
('n7684266', 'Wei', 'Lu', 0),
('n7685882', 'Tsz Fung Chris', 'Cheng', 0),
('n8002401', 'Cassie', 'Morrison', 0),
('n8004102', 'Jack', 'Parry', 0),
('n8012857', 'Deric', 'Dinh', 0),
('n8016631', 'Anna', 'Kalma', 0),
('n8017115', 'Ben', 'Apacible', 0),
('n8020914', 'Jessie', 'Ehrenberg', 0),
('n8021210', 'Chris', 'Forrest', 0),
('n8021333', 'Ryan', 'Hawes', 0),
('n8032769', 'Jie', 'Chen', 0),
('n8036926', 'Jesse', 'Evans', 0),
('n8037311', 'Daniel', 'Smith', 0),
('n8037698', 'Perry', 'Nicholas', 0),
('n8037833', 'Viswajit', 'Noronha', 0),
('n8038325', 'Miranda', 'Lorkin', 0),
('n8087636', 'Aden', 'Jones', 0),
('n8090190', 'Adam', 'Gibbon', 0),
('n8099596', 'John', 'Macpherson', 0),
('n8102171', 'Christopher', 'Dahlgren', 0),
('n8102597', 'Christopher', 'Kelly', 0),
('n8103801', 'Michael', 'Haynes', 0),
('n8104395', 'Laurence', 'McCabe', 0),
('n8105561', 'Dongkook', 'Seo', 0),
('n8276161', 'Damian', 'Walker', 0),
('n8278270', 'Jack', 'Kimberley', 0),
('n8281645', 'Amanda', 'Kay', 0),
('n8285888', 'Joseph', 'Wootton', 0),
('n8295751', 'Barnabas', 'Hamilton', 0),
('n8297070', 'Mitch', 'Bradford', 0),
('n8300453', 'Maree', 'Bosua', 0),
('n8301786', 'Daniel', 'Feltwell', 0),
('n8305480', 'Nicholas', 'Mok', 0),
('n8310092', 'Lauren', 'Skelly', 0),
('n8311382', 'Michael', 'Munchin', 0),
('n8316228', 'David', 'Nguyen', 0),
('n8316392', 'Christian', 'O''Grady - Mott', 0),
('n8316449', 'Sean', 'Melis', 0),
('n8317402', 'Sushant', 'Chawla', 0),
('n8318522', 'Pearl', 'Gariano', 0),
('n8318549', 'Michael', 'Whitlock', 0),
('n8318824', 'Haydn', 'Brindley', 0),
('n8320250', 'Harold', 'Nash', 0),
('n8321043', 'Nicole', 'Mao', 0),
('n8321744', 'Sam', 'Pourzinal', 0),
('n8322252', 'Christopher', 'McLeish', 0),
('n8323534', 'Joel', 'McRae', 0),
('n8324433', 'Joshua', 'Nguyen', 0),
('n8326479', 'Fraser', 'Haer', 0),
('n8328153', 'Caitlin', 'Schmidt', 0),
('n8330590', 'Todd', 'Denman', 0),
('n8330654', 'Alexander', 'Eeles', 0),
('n8331472', 'Jordan', 'Hennell', 0),
('n8332029', 'William', 'Pierce', 0),
('n8334552', 'Mei', 'Brough - Smyth', 0),
('n8339368', 'Pratik', 'Sati', 0),
('n8341257', 'Jamal Nasser M', 'Bin Samih', 0),
('n8355045', 'Sarah', 'Dawson', 0),
('n8357072', 'Gabriel Antonio', 'Sunol', 0),
('n8363722', 'Sadeen Sulaiman M', 'Alharbi', 0),
('n8365148', 'Dihua', 'Huang', 0),
('n8373701', 'Sultan Khudhair H', 'Alshammriy', 0),
('n8374881', 'Jacques Louis David', 'Kerguelen Lopez', 0),
('n8375941', 'Minh Phat', 'Tran', 0),
('n8383243', 'Corey', 'Thompson', 0),
('n8386099', 'Chung Lin', 'Wu', 0),
('n8389772', 'Daniel', 'Duong', 0),
('n8401039', 'Jason', 'Collier', 0),
('n8404721', 'Jeffrey', 'Van Duuren', 0),
('n8407126', 'Xi', 'Shang', 0),
('n8409323', 'Sing Yan', 'Lee', 0),
('n8412057', 'Abdulrahman Ibrahim A', 'Alokiely', 0),
('n8430128', 'Vu', 'Ngo', 0),
('n8430276', 'Luke', 'Boettcher', 0),
('n8431019', 'Henry', 'Mailo', 0),
('n8432520', 'Vladan', 'Miladinovic', 0),
('n8445443', 'Andy', 'Rosales', 0),
('n8468184', 'Ahmed Humaid Obaid', 'Al Sakiti', 0),
('n8485861', 'Xin', 'Luo', 0),
('n8495220', 'Baris', 'Altayli', 0),
('n8503605', 'Sebastian', 'Cortes Tamayo', 0),
('n8513716', 'Wing-Kit', 'Lam', 0),
('n8537828', 'Thomas', 'Pascoe', 0),
('n8540764', 'Sasu', 'Kankaanpaa', 0),
('n8541868', 'Daniel', 'Ruffo', 0),
('n8543330', 'Christopher', 'Inch', 0),
('n8549311', 'Samuel', 'Tayler', 0),
('n8552614', 'Benjamin', 'Harris', 0),
('n8562041', 'Jonathan', 'Yii', 0),
('n8567361', 'Charles', 'Kilburn', 0),
('n8568430', 'Cameron', 'Layfield', 0),
('n8568944', 'Benjamin', 'Armstrong', 0),
('n8571091', 'Anthony', 'Price', 0),
('n8571520', 'Joseph', 'Maher', 0),
('n8571538', 'Samara', 'Aiken', 0),
('n8571724', 'Jonathon', 'Scanes', 0),
('n8572054', 'Samuel', 'Andrews', 0),
('n8572755', 'John', 'Tran', 0),
('n8573557', 'Guy', 'Richards', 0),
('n8573727', 'Gary', 'Perrigo', 0),
('n8577889', 'James', 'Rogers', 0),
('n8578290', 'Anthony', 'Gough', 0),
('n8578443', 'Steven', 'Lomas', 0),
('n8580189', 'Mitchell', 'De Bruyn', 0),
('n8580260', 'Shamus', 'Ford', 0),
('n8581797', 'Brandon', 'Milsom', 0),
('n8583331', 'Michael', 'Clark', 0),
('n8583820', 'Melissa', 'Kroes', 0),
('n8584249', 'James', 'Church', 0),
('n8584281', 'George', 'Gilbert', 0),
('n8584338', 'Lincoln', 'Parsons', 0),
('n8584362', 'Reece', 'Stevens', 0),
('n8585423', 'Lachlan', 'Preston', 0),
('n8585911', 'Michael', 'Hanlon', 0),
('n8586781', 'Victoria', 'Smith', 0),
('n8587451', 'Alex', 'Armatys', 0),
('n8590397', 'Imran', 'Thaggard', 0),
('n8591946', 'Austin', 'Horn', 0),
('n8593396', 'Grace', 'O''Brien', 0),
('n8593540', 'Brian', 'Fernandez', 0),
('n8594473', 'Wei - Ming', 'Tan', 0),
('n8595003', 'Edward', 'Lloyd', 0),
('n8595976', 'Emma', 'Harvey', 0),
('n8596433', 'Alexander', 'McMahon', 0),
('n8596671', 'Henry', 'Widiapradja', 0),
('n8596841', 'Qiling', 'Li', 0),
('n8596972', 'Lathaam', 'Berkhout', 0),
('n8598096', 'Joshua', 'Graham', 0),
('n8598550', 'Joel', 'Hendriks', 0),
('n8599122', 'Lillita', 'Lalla', 0),
('n8599327', 'Timothy', 'Brown', 0),
('n8599599', 'Daniel', 'Hunter', 0),
('n8599874', 'Kayla', 'Degraaf', 0),
('n8600040', 'Isabella', 'Hunt', 0),
('n8601178', 'Bradley', 'Thater', 0),
('n8601712', 'Harrison', 'Davies', 0),
('n8602484', 'Cedric', 'Carlos', 0),
('n8602701', 'Jacob', 'Warhurst', 0),
('n8603723', 'Nicholas', 'Leete', 0),
('n8605599', 'Brendan', 'Ngo', 0),
('n8606196', 'Morgan', 'Gate - Leven', 0),
('n8606242', 'Todd', 'Lewis', 0),
('n8606668', 'Nicholas', 'Devin', 0),
('n8606935', 'Viktor', 'Polak', 0),
('n8607168', 'Timothy', 'Loiterton', 0),
('n8611386', 'Alexander', 'Brabyn', 0),
('n8611441', 'Harrison', 'Armstrong', 0),
('n8612307', 'Hannah', 'Usher', 0),
('n8612315', 'Maxwell', 'Eichhorn', 0),
('n8612463', 'Claudia', 'Barr', 0),
('n8613125', 'Daniel', 'Holman', 0),
('n8614521', 'Benjamin', 'McCloskey', 0),
('n8614776', 'Tyson', 'Kerridge', 0),
('n8615560', 'Daniel', 'Armstrong', 0),
('n8616574', 'Benjamin', 'Maggacis', 0),
('n8617015', 'Nicholas', 'Vivanco', 0),
('n8617724', 'Jandale', 'De Pano', 0),
('n8618194', 'Elliott', 'Polzin', 0),
('n8618887', 'Harrison', 'Davis', 0),
('n8620849', 'Samuel', 'Haakman', 0),
('n8620873', 'Joshua', 'Khoury', 0),
('n8621322', 'Adam', 'Rowe', 0),
('n8621934', 'Cameron', 'Maine', 0),
('n8623881', 'Ruslan', 'Abylkassov', 0),
('n8625409', 'Kristy', 'Winter', 0),
('n8637342', 'Patrick', 'Baggerman', 0),
('n8638721', 'Michael', 'Porch', 0),
('n8639957', 'Richard', 'Allen', 0),
('n8643458', 'Jacob', 'Parker', 0),
('n8648565', 'Julian', 'Luck', 0),
('n8658803', 'Naga', 'Gamoga', 0),
('n8661090', 'Elise', 'Kjoendal', 0),
('n8661171', 'Svanhild', 'Egge', 0),
('n8669023', 'Bernd', 'Hartzer', 0),
('n8671036', 'Michael', 'Chandler', 0),
('n8671559', 'Peter', 'Woodfall', 0),
('n8674230', 'Fan Leon', 'Yang', 0),
('n8677506', 'Chun-Han', 'Chen', 0),
('n8677581', 'Seon-Haeng', 'Lee', 0),
('n8677883', '', 'Godhasara Pranav Bhupatbhai', 0),
('n8680159', 'Timothy', 'Corcoran', 0),
('n8681961', 'Tahani Ali M', 'Al Mubarak', 0),
('n8683981', 'Timothy', 'Heinz', 0),
('n8685321', 'Shanshan', 'Xian', 0),
('n8685576', 'Matthew', 'Taylor', 0),
('n8686998', 'Timothy', 'Mardon', 0),
('n8687081', 'Jordan', 'Macey', 0),
('n8687838', 'Brendan', 'Byrne', 0),
('n8689491', 'Samuel', 'Hillier', 0),
('n8689750', 'Matthew', 'Brown', 0),
('n8697175', 'Yichen', 'Wang', 0),
('n8697353', 'Wenjie', 'Huang', 0),
('n8703043', 'Jack', 'Behne', 0),
('n8707138', 'Geoffrey', 'Luck', 0),
('n8721386', 'Saskia', 'Eggebrecht', 0),
('n8723885', 'Suvi Johanna', 'Maekinen', 0),
('n8730920', 'Suad Mohammed Said', 'Al Fana', 0),
('n8738602', 'Jordan', 'Stretton', 0),
('n8742600', 'Weiwei', 'Nong', 0),
('n8745234', 'Jingwen', 'Lin', 0),
('n8773360', 'Cheuk Hei', 'Hon', 0),
('n8777284', 'Dongmin', 'Lee', 0),
('n8782202', 'Nicolaas Gerardus', 'Nijland', 0),
('n8782261', 'Robrecht Johannes', 'Floris', 0),
('n8783497', 'Jeremy', 'Joly', 0),
('n8785589', 'Adam', 'Bain', 0),
('n8786119', 'Giel', 'Wijgergangs', 0),
('n8787719', 'Andre', 'Laugaland', 0),
('n8792763', 'Quang Phuc', 'Trinh', 0),
('n8811903', 'Lekha', 'Narra', 0),
('n8818835', 'Yuanying', 'Xu', 0),
('n8822859', 'Cheng-En', 'Wu', 0),
('n8829471', 'Mohamed', 'Younis', 0),
('n8842736', 'Murray', 'White', 0),
('n8892164', 'David', 'Cook', 0),
('n8909253', 'Steven', 'Stone', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(40) COLLATE utf8_bin NOT NULL,
  `salt` varchar(40) COLLATE utf8_bin NOT NULL,
  `realName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `email`, `password`, `salt`, `realName`) VALUES
(1, 'nobody@localhost', 'd186e8dac48a24d0115b568d0ab2c9e8b82e6adb', 'b03863f12c7673f51f626665f8f780742a052114', 'Nobody User'),
(2, 'tonyqvc@live.com', 'f581d3e82ceada1c111ab1a5c05e24fc196474c7', '3da541559918a808c2402bba5012f6c60b27661c', 'Tony Chau');

-- --------------------------------------------------------

--
-- Table structure for table `users_sessions`
--

CREATE TABLE IF NOT EXISTS `users_sessions` (
  `sessionId` varchar(128) COLLATE utf8_bin NOT NULL,
  `userId` mediumint(8) unsigned NOT NULL,
  `sessionKey` varchar(64) COLLATE utf8_bin NOT NULL,
  `sessionStart` bigint(20) unsigned NOT NULL,
  `expiry` bigint(20) unsigned NOT NULL,
  `userAgent` varchar(255) COLLATE utf8_bin NOT NULL,
  `ipAddress` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`sessionId`,`userId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users_sessions`
--

INSERT INTO `users_sessions` (`sessionId`, `userId`, `sessionKey`, `sessionStart`, `expiry`, `userAgent`, `ipAddress`) VALUES
('016c8e2c-09e9-4a29-b2b0-1b2559be7ea2', 2, '54771dae5520e423e5b080d07e3a898c3f098271', 1393853392, 1393874992, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36', '::1'),
('389381e0-e245-499b-be63-4b8a4e772148', 2, 'ce3f428a7abea69dcffc2e6cb76756c73786551a', 1392906421, 1392910021, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('4bbfcee0-0b03-4b4b-bc59-7659bf8fb2b6', 2, '4b05f61db05aa0a9607c168372d2e799994d1c7e', 1393206428, 1393228028, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36', '::1'),
('95ae493e-6cb3-4fea-ba63-8cf1bb787f0e', 2, '1f052c0c3c0cf073a40248fb403b8205e282b71e', 1337052234, 1337138634, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', '::1'),
('e14378af-3e8b-4ad0-a619-7ee6e907d70e', 2, 'b27612665458adbe88f7d17a9a6eef3c06928d4d', 1392906975, 1392928575, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('ebd336f6-8f38-46cd-a1f6-2c839e47a982', 2, '0695b91ba30c77e127ff753ca9dad58f0040d6f4', 1392906485, 1392903304, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('ec62fa14-6cb0-4e40-8533-cf4f417f0586', 2, '09034e2d13c78aecdde7f030d8e8c0cb2ad97d77', 1392906415, 1392910015, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36', '::1'),
('ed0741fb-dc79-406b-a89e-f66e08c3b078', 2, '1751e96993b78fb4f441559d5c90773781a0f66e', 1393904740, 1393926340, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `users_settings`
--

CREATE TABLE IF NOT EXISTS `users_settings` (
  `userId` mediumint(8) unsigned NOT NULL,
  `settingName` varchar(64) COLLATE utf8_bin NOT NULL,
  `settingValue` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`,`settingName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users_states`
--

CREATE TABLE IF NOT EXISTS `users_states` (
  `userId` mediumint(8) unsigned NOT NULL,
  `stateName` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userId`,`stateName`),
  KEY `stateName` (`stateName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users_states`
--

INSERT INTO `users_states` (`userId`, `stateName`) VALUES
(1, 'disableUserSystem'),
(2, 'specialUserAdmin');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_sessions`
--
ALTER TABLE `users_sessions`
  ADD CONSTRAINT `users_sessions_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_settings`
--
ALTER TABLE `users_settings`
  ADD CONSTRAINT `users_settings_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_states`
--
ALTER TABLE `users_states`
  ADD CONSTRAINT `users_states_ibfk_1` FOREIGN KEY (`stateName`) REFERENCES `states` (`stateName`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_states_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
