/**
 * Criteria 2 Table Object
 */
function Criteria2Obj() {
    "use strict";
    
    /**************************************************************/
    /* Class variables                                            */
    /**************************************************************/
    var COLUMN_MARKS = [1, 2, 2];
    
    /**************************************************************/
    /* Library Functions                                          */
    /**************************************************************/
    
    // Calculates the total for all sections.
    var calcTotal = function () {
        // get all sections
        var sections = $("table.criteria tr.section select.scoreBox");
        var totalMarks = 0;
        for (var i = 0; i < sections.length; i++) {
            totalMarks += $(sections[i]).val() * 1;
        }
        
        // shade the checkboxes
        var checkboxes = $("table.criteria input[type=checkbox]").each(function() {
            if (this.checked) {
                $(this).parent().addClass("checkedBox");
            }
            else {
                $(this).parent().removeClass("checkedBox");
            }
        });
        
        // place in header.
        $("span#currScaledScore").html(Math.round(totalMarks * 100) / 100);
        
        // place in percentage.
        $("span#percent").html(Math.round(totalMarks / (5 * sections.length) * 1000) / 10);
        
        // return total marks
        return totalMarks;
    };
    
    // Changes the status of the status box.
	var setStatus = function(msg) {
		$("#criteriaStatus").html(msg);
	};
    
    var repeatNTimes = function(n, c) {
        var str = "";
        for (var i = 0; i < n; i++)
            str += c;
        
        return str;
    };
    
    /**************************************************************/
    /* Attach Events Here                                         */
    /**************************************************************/
    
    // When checkboxes are checked.
    $("table.criteria input[type=checkbox]").change(function() {
        calcTotal();
    });
    
    // When closed is pressed.
    $("#closeWindow").click(function() {
		if (window.opener && !window.opener.closed) {
			window.opener.location.reload();
		}
		window.close();
	});
    
    // When Saved is pressed.
    $("#saveDraft").click(function() {
		var obj = {
			data: [],
			feedback: "",
            totalMarks: calcTotal()
		};

        // go through each section
		var sections = $("table.criteria tr.section");
		var feedback = $("table.criteria tr.feedback");
        for (var i = 0; i < sections.length; i++) {
            var sectObj = {
                ids: [],
                feedback: "",
                score: 0
            };
            
            // get checkbox status.
            var checkBoxes = $(sections[i]).find("input[type=checkbox]");
            for (var j = 0; j < checkBoxes.length; j++) {
                sectObj[checkBoxes[j].id] = checkBoxes[j].checked == true;
                sectObj.ids.push(checkBoxes[j].id);
            }
            
            // get select box
            sectObj.score = $(sections[i]).find("select.scoreBox").val();
            
            // get feedback
            sectObj.feedback = $(feedback[i]).find("textarea.sectionfeedback").val().trim();
            
            // add to global feedback
            var title = $(sections[i]).find("p.sectionTitle").text().trim();
            
            obj.feedback += title + "\n";
            obj.feedback += repeatNTimes(title.length, "-") + "\n";
            obj.feedback += sectObj.feedback;
            obj.feedback += "\n\n";
            
            obj.data.push(sectObj);
        }
		
        // POST IT
		$.post(webRoot + "submissions/save/" + submissionId, 
			{
				data:     JSON.stringify(obj),
				marks:    obj.totalMarks,
				feedback: obj.feedback
			})
			.done(function(data) {
				// done saving?
				if (data.error) {
					setStatus("Save failed: " + data.message);
				} else {
					setStatus("Save complete. Please commit when you are done.");
				}
			})
			.fail(function() {
				// failed to connect? or something.
				setStatus("Server could not be contacted.");
			});
	});

    // When "Set as marked" is pressed.
	$("#setMarked").click(function() {
		$.post(webRoot + "submissions/commit/" + submissionId)
			.done(function(data) {
				// done saving?
				if (data.error) {
					setStatus("Commit failed: " + data.message);
				} else {
					setStatus("Commit complete.");
				}
			})
			.fail(function() {
				// failed to connect? or something.
				setStatus("Server could not be contacted.");
			});
	});
    
    /**************************************************************/
    /* Onload Stuff occurs here                                   */
    /**************************************************************/
    
    // Interface changes
    $("table.criteria tr.section input[type=checkbox]").each(function() {
        this.checked = true;
    });
    
    $("table.criteria span.currMarks").each(function() {
        var select = document.createElement("select");
		var jSelect = $(select);

		jSelect.addClass("scoreBox");
        
        $(this).html("");
        
        // add the indiviual options from 0-7
		for (var i = 5; i >= 0; i -= 0.5) {
			var opt = document.createElement("option");
			var jOpt = $(opt);
            var txt = i;
            
            if (i - Math.floor(i) == 0) {
                txt = i + ".0";
            }
            
			jOpt.val(i);
			jOpt.text(i);
			jSelect.append(jOpt);
            
            jSelect.change(function() { calcTotal(); });
            jSelect.keyup (function() { calcTotal(); });

            $(this).append(jSelect);
		}
    });
    
    // Load previous if it exists.
	setStatus("Loading previous draft...");
	$.get(webRoot + "submissions/load/" + submissionId)
		.done(function(data) {
			// Lets see what we have here...
			if (data.error) {
				// aww.. no drafts.
				setStatus("No Drafts to be loaded. Blank Criteria loaded.");
			}
			else {
				var serialized = data.data;
				var obj = JSON.parse(serialized);
                
                // set each section
                var sections = $("table.criteria tr.section");
                var feedbacks = $("table.criteria tr.feedback");
				for (var i = 0; i < obj.data.length; i++) {
					var c = obj.data[i];

                    // set checkbox.
                    for (var j = 0; j < c.ids.length; j++) {
                        $("input#" + c.ids[j]).attr("checked", c[c.ids[j]]);
                    }
                    
                    // set feedback
                    $(feedbacks[i]).find("textarea.sectionfeedback").val(c.feedback);
                    
                    // set score
                    $(sections[i]).find("select.scoreBox").val(c.score);
				}
                
				setStatus("Latest draft loaded (Saved at: " + data.lastSaved + ")");
			}
            
            setTimeout(calcTotal, 100);
		});
}

$(Criteria2Obj);