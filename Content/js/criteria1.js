var marksCols = [
	[7],
	[6, 5],
	[4, 3],
	[2, 1, 0]
];

var CRITERIA_MAX_SCORE   = 7;
var ASSIGNMENT_MAX_SCORE = 25;

var CriteriaSheetObj = function() {
	// Some Helper functions
	var roundTo2 = function(n) {
		return Math.round(n * 100) / 100;
	};

	/**
	 * Takes in a select box, and will highlight the "grade"
	 * in the td in the same row.
	 */
	var highlightCriteriaCell = function(element) {
		var cols = $(element).parent().parent().children("td");
		var valSelected = $(element).val();
		var markIndex = 0;
		var markFound = false;

		for (markIndex = 0; markIndex < marksCols.length && !markFound; markIndex++) {
			for (var i = 0; i < marksCols[markIndex].length; i++) {
				if (marksCols[markIndex][i] == valSelected) {
					markFound = true;
					break;
				}
			}

			if (markFound)
				break;
		}

		var currIndex = -1;
		var selectedOne = false;

		for (var i = 0; i < cols.length; i++) {
			currIndex += cols[i].colSpan;
			if (currIndex >= markIndex && !selectedOne) {
				selectedOne = true;
				$(cols[i]).css("background-color", "#B8DE83");
			}
			else {
				$(cols[i]).css("background-color", "transparent");
			}
		}

		// update totals
		calcTotals();
	};

	/**
	 * Recalculates the score in each sub-total then finalises the total
	 */
	var calcTotals = function() {
		// calc each subtotal
		$("table.criteria").find("tr.criteria_subtotal").each(function() {
			calcSubTotal(this);
		});

		// find all subtotals that are NOT in an unchecked optional row.
		var accordians = $("div.accordian");
		var currScore  = 0;
		var totalScore = 0;
		for (var i = 0; i < accordians.length; i++) {
			var criteriaTable = $(accordians[i]).find("table.criteria tr.criteria_subtotal");
			if (criteriaTable.length <= 0) {
				// not a criteria accordian
				continue;
			}

			var optional = $(accordians[i]).find("input[name=optional]");
			var isGoodToGo = true;
			if (optional.length > 0) {
				console.log("Found optional criteria '" + $(criteriaTable[0]).data("section").name + "', checking");
				for (var j = 0; j < optional.length; j++) {
					isGoodToGo = isGoodToGo && optional[j].checked;
				}

				console.log("Include result: " + isGoodToGo);
			}

			if (isGoodToGo) {
				// add to total
				for (var j = 0; j < criteriaTable.length; j++) {
					currScore  += $(criteriaTable[j]).data("score").score;
					totalScore += $(criteriaTable[j]).data("score").max;
				}
			}
		}

		// place into actual marks placeholder
		var percent = currScore / totalScore;
		var scaledScore = roundTo2(percent * ASSIGNMENT_MAX_SCORE);
		$("span#currScaledScore").html(scaledScore);
		$("span#totalScaledScore").html(ASSIGNMENT_MAX_SCORE);
		$("span#percent").html(roundTo2(percent * 100));
	};

	/**
	 * Calculates the score in a sub-section only
	 */
	var calcSubTotal = function(tr_subtotal) {
		var subTotalNode = $(tr_subtotal);
		var subTotal = 0.0;
		var numCriteria = 0;
		var node = subTotalNode;

		var maxSubTotal = subTotalNode.find("span.subtotalTotal").text() * 1;

		while (node = node.prev(), node.hasClass("criteria")) {
			subTotal += node.find("select.scoreBox").val() / CRITERIA_MAX_SCORE;
			numCriteria++;
		}

		// total score is....!
		subTotal = Math.round(subTotal / numCriteria * maxSubTotal * 100) / 100;
		subTotalNode.find("span.subtotalVal").html(subTotal);

		// save into data-score
		subTotalNode.data("score", {
			score: subTotal,
			max: maxSubTotal
		});
	};

	var setStatus = function(msg) {
		$("#criteriaStatus").html(msg);
	};

	var str_repeat = function(str, n) {
		var s = "";
		for (var i = 0 ; i < n; i++) {
			s += str;
		}

		return s;
	};

	/**
	 * On Load Functions
	 */
	$("table.criteria th.scoreKeeping").each(function() {
		// add select elements
		var select = document.createElement("select");
		var jSelect = $(select);

		jSelect.addClass("scoreBox");

		// add the indiviual options from 0-7
		for (var i = 0; i <= CRITERIA_MAX_SCORE; i++) {
			var opt = document.createElement("option");
			var jOpt = $(opt);
			jOpt.val(i);
			jOpt.text(i);
			jSelect.append(jOpt);
		}

		jSelect.change(function() { highlightCriteriaCell(this); });
		jSelect.keyup (function() { highlightCriteriaCell(this); });
		
		$(this).append(jSelect);
	});

	$("tr.criteria td").click(function() {
		// get the row the td which was clicked on
		var row = $(this).parent();
		
		// set which one was selected.
		var cols = $(row).find("td");
		$(cols).removeClass("selected");
		$(this).addClass("selected");

		// iterate through each one until we know which one it is.
		var curr = 0;
		for (var i = 0; i < cols.length; i++) {
			if ($(cols[i]).hasClass("selected")) {
				console.log(marksCols[curr][0]);
				$(row).find("th.scoreKeeping .scoreBox").val(marksCols[curr][0]);
				$(row).find("th.scoreKeeping .scoreBox").change();
				break;
			}

			curr += cols[i].colSpan;
		}

		// done.
	});

	$("table.criteria").parent().parent().find("input[name=optional]").click(function() { calcTotals(); });

	$("#closeWindow").click(function() {
		if (window.opener && !window.opener.closed) {
			window.opener.location.reload();
		}
		window.close();
	});
	
	$("#saveDraft").click(function() {
		var obj = {
			data: [],
			feedback: ""
		};

		var sections = $("table.criteria");
		
		for (var j = 0; j < sections.length; j++) {
			var c = {
				marks:    [],
				feedback: "",
				optional: false,
				optionalValue: false
			};

			// check for optional tag too.
			var accordian = $(sections[j]).parent().parent();
			if ($(accordian).find("input[name=optional]").length > 0) {
				c.optional = true;
				c.optionalValue = $(accordian).find("input[name=optional]")[0].checked;
			}

			// scores
			var scores = $($(sections[j]).find("tr.criteria th.scoreKeeping .scoreBox"));
			for (var i = 0; i < scores.length; i++) {
				c.marks.push($(scores[i]).val());
			}

			// feedback
			if (c.optional && !c.optionalValue) {
				c.feedback = "This criteria was not included in marks calculation.";
			}
			else {
				c.feedback = $($(sections[j]).find("textarea.comments")).val();
			}

			// combined feedback
			var sectionName = $($(sections[j]).find("tr.criteria_subtotal")).data("section").name;
			obj.feedback += sectionName + "\n";
			obj.feedback += str_repeat("=", sectionName.length) + "\n";
			obj.feedback += c.feedback + "\n\n";

			obj.data.push(c);
		};
		
		// POST IT
		$.post(webRoot + "submissions/save/" + submissionId, 
			{
				data:     JSON.stringify(obj),
				marks:    $("#currScaledScore").text() * 1,
				feedback: obj.feedback
			})
			.done(function(data) {
				// done saving?
				if (data.error) {
					setStatus("Save failed: " + data.message);
				} else {
					setStatus("Save complete.");
				}
				console.log(data);

			})
			.fail(function() {
				// failed to connect? or something.
				setStatus("Server could not be contacted.");
			});
	});

	$("#setMarked").click(function() {
		$.post(webRoot + "submissions/commit/" + submissionId)
			.done(function(data) {
				// done saving?
				if (data.error) {
					setStatus("Commit failed: " + data.message);
				} else {
					setStatus("Commit complete.");
				}
				console.log(data);
			})
			.fail(function() {
				// failed to connect? or something.
				setStatus("Server could not be contacted.");
			});
	});

	$(".expandAll").click(function() {
		$("div.accordian").each(function() {
			if (!$(this).find("div.accordian-content").hasClass("accordian-open"))
				$(this).find("div.accordian-header").click();
		});
	});

	$(".collapseAll").click(function() {
		$("div.accordian").each(function() {
			if ($(this).find("div.accordian-content").hasClass("accordian-open"))
				$(this).find("div.accordian-header").click();
		});
	});

	// load JSON from previous drafts
	setStatus("Loading previous draft...");
	$.get(webRoot + "submissions/load/" + submissionId)
		.done(function(data) {
			// Lets see what we have here...
			if (data.error) {
				// aww.. no drafts.
				setStatus("No Drafts to be loaded. Blank Criteria loaded.");
			}
			else {
				var serialized = data.data;
				var obj = JSON.parse(serialized);
				var criteria = $("table.criteria");

				for (var i = 0; i < obj.data.length; i++) {
					var c = obj.data[i];
					var selects = $(criteria[i]).find("tr.criteria .scoreBox");

					// marks
					for (var j = 0; j < c.marks.length; j++) {
						$(selects[j]).val(c.marks[j]);
					}
					$(selects).change();

					// criteria
					$(criteria[i]).find("textarea.comments").val(c.feedback);

					// optional check
					if (c.optional)
						$(criteria[i]).parent().parent().find("input[name=optional]")[0].checked = c.optionalValue;
				}

				setStatus("Latest draft loaded (Saved at: " + data.lastSaved + ")");
                setTimeout(calcTotals, 10);
			}
		});
};

$(CriteriaSheetObj);
