/* Submission Source Code Viewer
 * Javascript File!
 */
var entityMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;'
};

function escapeHtml(string) {
	return String(string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
	});
}

var currFile = "";

function setSource(source, path) {
    // source code
    $("#sourceCode").html(escapeHtml(source));
    $("#sourceCode").removeClass("prettyprinted");
    prettyPrint();

    // link
    $("#openInBrowser").html(
        '<a href="' + encodeURI(path) + '">Open</a>');

    // scroll to code
    // quite annoying
    //location.hash = "#openInBrowser";
}

$(function() {
	$("table.subFilesTable a.viewSource").click(function() {
        // get the row.
        var row = $($(this).parent().parent());
        if (row.data('searchResult') === undefined || row.data('searchResult') === null) {
            // save the file to our global
            currFile = $(this).parent().children("a.url").attr("href");

            // Ajax and read that damn file.
            $.post(
                webRoot + "submissions/read/", 
                { filePath: $(this).parent().parent().children("td.pathInfo").text() }
                )
                .done(function(data) {
                    setSource(data.fileContents, currFile);
                });
        }
        else {
            var source = "";
            var data = row.data('searchResult');
            
            for (var i = 0; i < data.matches.length; i++) {
                source += 
                    "Line " + (data.matches[i][0] + 1) + ": " + 
                    data.matches[i][1] + "\n";
            }
            
            // get url
            var url = row.find("a.url").attr("href");
            
            // view it!
            setSource(source, url);
        }

		// Suppress normal a click event
		return false;
	});
    
	$("td.sourceCodeReadable a").click(function() {
		return false;
	});
});
