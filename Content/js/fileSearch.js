/*
 * File Search JavaScript
 */

$(function() {
    $("form#searchFile").submit(function() {
        // get search term.
        var term = $("form#searchFile input[name=searchTerm]").val();

        // get all files
        var rows = $("table.subFilesTable tr.hasFile");
        for (var i = 0; i < rows.length; i++) {
            var path = $(rows[i]).find("span.path").html();
            
            // check if a valid type to check first.
            if ($(rows[i]).find("a.viewSource").length > 0) {
                $.post(
                    webRoot + "submissions/searchTerm/", 
                    { 
                        filePath: path,
                        id: i,
                        term: term
                    }
                    )
                    .done(function(data) {
                        console.log(data);
                        var rows = $("table.subFilesTable tr.hasFile");
                        $(rows[data.id]).data("searchResult", data);
                        if (data.matches.length == 0) {
                            $(rows[data.id]).addClass('hidden');
                        }
                        else {
                            $(rows[data.id]).removeClass('hidden');
                        }
                        
                        $(rows[data.id]).find(".searchResults").html(
                            data.matches.length + " Matches Found"
                        );
                        $(rows[data.id]).find(".searchResults").removeClass("hidden");
                    });
            }
            else {
                $(rows[i]).addClass('hidden');
                $(rows[i]).find(".searchResults").html("");
                $(rows[i]).find(".searchResults").addClass("hidden");
            }
        }

        return false;
    });

    $("form#searchFile input[type=reset]").click(function() {
        // restore all rows to normal
        var rows = $("table.subFilesTable tr.hasFile");
        rows.removeClass('hidden');

        // clear all search data
        rows.data('searchResult', null);
        rows.find(".searchResults").html("");
        rows.find(".searchResults").addClass("hidden");
    }); 
});