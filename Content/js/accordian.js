$(function() {
	var CHARACTERS = ['+', '&mdash;'];
	var CHAR_STATE = 0;
	
	var OPEN_STATE = "accordian-open";

	$("div.accordian div.accordian-header").click(function() {
		var accordian = $(this).parent();
		var contents = accordian.children("div.accordian-content").first();
		
		if (!accordian.data("locked")) {
			// "lock" this accordian so it cannot be touched right now until finished.
			accordian.data("locked", true);
		
			// determine if closed or open
			if (contents.hasClass(OPEN_STATE)) {
				// Close it.
				contents.slideUp();
				contents.removeClass(OPEN_STATE);
			}
			else {
				// Open it!
				contents.slideDown();
				contents.addClass(OPEN_STATE);
			}
		
			// Swap state char
			accordian.find("div.accordian-state").html(CHARACTERS[contents.hasClass(OPEN_STATE) ? 1 : 0]);
			
			// "unlock"
			accordian.data("locked", false);
		}
	});
});
