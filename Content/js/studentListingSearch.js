$(function() {
	$('#studentListingSearch').keyup(function() {
		// split their search into terms.
		var terms = this.value.split(/\s+/);
		var termRegExp = [];
		for (var i = 0; i < terms.length; i++)
			termRegExp.push(new RegExp(terms[i], "i"));

		var rows = $('#studentList tr');
		for (var i = 0; i < rows.length; i++) {
			var idField   = $(rows[i]).children(".studentId");
			var nameField = $(rows[i]).children(".studentName");

			if (idField.length <= 0 || nameField <= 0)
				continue;

			var matches = false;
			var exists = true;
			for (var j = 0; j < termRegExp.length && exists; j++) {
				exists = exists && termRegExp[j].test(idField.text());
			}

			matches = exists;
			exists = true;
			for (var j = 0; j < termRegExp.length && exists; j++) {
				exists = exists && termRegExp[j].test(nameField.text().toLowerCase());
			}

			matches = exists || matches;

			if (!matches) {
				$(rows[i]).css('display', 'none');
			}
			else {
				$(rows[i]).css('display', 'table-row');
			}
		}
	});
});