var criteriaWindow;

function refreshPage() {
	if (criteriaWindow.closed) {
		location.reload();
	}
	else {
		setTimeout(refreshPage, 1000);
	}
}

$(function() {
	$("#showMarkingCriteria").click(function() {
		criteriaWindow = window.open(webRoot + "submissions/mark/" + submissionId);
		$(".criteriaWaiting").show();
	});
});
