// Date and Time Functions
function toRelativeDate(base_ts, delta_ts) {

	var secs_delta = Math.floor((delta_ts - base_ts) / 1000);
	var output_str = "";
	var prepend = "";
	var postpend = "";

	if (secs_delta < 0) {
		prepend = "About ";
		postpend = " ago.";
		secs_delta *= -1;
	}
	else {
		prepend = "In about ";
		postpend = ".";
	}

	/*
	console.log("Base TS: " + base_ts);
	console.log("Delta: " + delta_ts);
	console.log("Diff is " + secs_delta);
	*/

	var months = Math.floor(secs_delta / (60 * 60 * 24 * 30));
	secs_delta %= months * 60 * 60 * 24;
	var days = Math.floor(secs_delta / (60 * 60 * 24));
	secs_delta %= days * 60 * 60 * 24;
	var hours = Math.floor(secs_delta / (60 * 60));
	secs_delta %= hours * 60 * 60;
	var mins = Math.floor(secs_delta / 60);
	secs_delta %= mins * 60;
	var secs = secs_delta;

	/*
	console.log("Months " + months);
	console.log("days " + days);
	console.log("hours " + hours);
	console.log("mins " + mins);
	console.log("secs " + secs);
	*/

	if (months > 0 || days > 0) {
		if (months > 1) {
			output_str = "" + months + " months";
		}
		else if (months > 0) {
			output_str = "" + months + " month";
		}

		if (days > 0) {
			if (output_str.length > 0) {
				output_str += ", ";
			}

			if (days > 1) {
				output_str += "" + days + " days";
			}
			else {
				output_str += "1 day";
			}
		}
	}
	else {
		if (hours > 1) {
			output_str = "" + hours + " hours";
		}
		else if (hours > 0) {
			output_str = "1 hour";
		}

		if (secs > 0) {
			if (output_str.length > 0) {
				output_str += ", ";
			}
			
			if (secs > 1) {
				output_str += "" + secs + " seconds";
			}
			else if (secs > 0) {
				output_str += "1 second"; 
			}
		}
	}

	return prepend + output_str + postpend;
}