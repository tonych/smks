/***************************
 * Assignment Widget
 */
$(function() {
	// Load data
	$.getJSON(webRoot + "rest/assignments", function(data) {
		// if no data...
		if (data.count <= 0) {
			// provide a nice error message.
			$("#assignmentWidget").html('There is no assessment! ' + 
				'<a href="'+ webRoot +'import">Import or Create</a> some.');
		}
	});
});